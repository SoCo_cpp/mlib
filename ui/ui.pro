
!defined(MLIB_SRC,var):error("MLIB ui (mlib/ui/ui.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/ui

message("MLIB including ui...")

QT *= core gui
CONFIG *= qt

SOURCES +=  $$MSRC/mouse_tracking/mousebuttonstatus.cpp \
            $$MSRC/mouse_tracking/mousetracker.cpp


HEADERS +=  $$MSRC/mouse_tracking/mousebuttonstatus.h \
            $$MSRC/mouse_tracking/mousetracker.h


