#ifndef MOUSETRACKER_H
#define MOUSETRACKER_H

#include <QObject>
#include "ui/mouse_tracking/mousebuttonstatus.h"

class MouseTracker : public QObject
{
    Q_OBJECT
private:
    const static int cDefaultClickDelayMax = 500; // ms
public:
    explicit MouseTracker(QObject* parent = 0);

    void mouseMove(QMouseEvent* e);
    void mousePress(QMouseEvent* e);
    void mouseRelease(QMouseEvent* e);
    void mouseDblClick(QMouseEvent* e);
    void mouseWheel(QWheelEvent* e);

    bool updateStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys, const QPoint& pos, const QPoint& wheelPos);
    const MouseButtonStatus& buttonStatus() const;

protected:
    unsigned long _clickDelayMax;
    MouseButtonStatus _status;

signals:
    void mouseMove(const MouseButtonStatus* s);
    void mouseDrageBegin(const MouseButtonStatus* s);
    void mouseDrageMove(const MouseButtonStatus* s);
    void mouseDrageEnd(const MouseButtonStatus* s);
    void mouseClicked(const MouseButtonStatus* s);
    void mouseDblClicked(const MouseButtonStatus* s);
    void mouseWheel(const MouseButtonStatus* s);

public slots:
};

#endif // MOUSETRACKER_H
