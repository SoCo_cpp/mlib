#ifndef MOUSEBUTTONSTATUS_H
#define MOUSEBUTTONSTATUS_H

#include <QObject>
#include <QTime>
#include <QPoint>
#include <QMouseEvent>


class MouseButtonStatus : public QObject
{
    Q_OBJECT
public:
    MouseButtonStatus(QObject* parent = 0);
    MouseButtonStatus(const MouseButtonStatus& cpy);
    MouseButtonStatus& operator =(const MouseButtonStatus& rhs);
    void copy(const MouseButtonStatus& cpy);

    void clear();

    void setStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys);
    bool isStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys) const;
    void setPos(const QPoint& pos, const QPoint& wheelPos);
    void updatePos(const QPoint& pos); // done durring a drag
    void updateWheelPos(const QPoint& pos);

    const QPoint& curPos() const;
    const QPoint& prevPos() const;
    QPoint posChange() const;
    int posChangeX() const;
    int posChangeY() const;

    const QPoint& curWheelPos() const;
    const QPoint& prevWheelPos() const;

    bool isDown() const;
    bool hasMoved() const;
    Qt::MouseButtons buttons() const;
    Qt::KeyboardModifiers keys() const;

    unsigned int statusAge() const; // ms

protected:
    Qt::MouseButtons _buttons;
    Qt::KeyboardModifiers _keys;
    QPoint _curPos;
    QPoint _prevPos;
    QPoint _curWheelPos;
    QPoint _prevWheelPos;
    QTime _statusTime;

signals:

public slots:
};

#endif // MOUSEBUTTONSTATUS_H
