#include "ui/mouse_tracking/mousebuttonstatus.h"

MouseButtonStatus::MouseButtonStatus(QObject* parent /*= 0*/) :
    QObject(parent)
{

}

MouseButtonStatus::MouseButtonStatus(const MouseButtonStatus& cpy) :
    QObject(0)
{
    copy(cpy);
}

MouseButtonStatus& MouseButtonStatus::operator =(const MouseButtonStatus& rhs)
{
    copy(rhs);
    return *this;
}

void MouseButtonStatus::copy(const MouseButtonStatus& cpy)
{
    _buttons        = cpy._buttons;
    _keys           = cpy._keys;
    _curPos         = cpy._curPos;
    _prevPos        = cpy._prevPos;
    _curWheelPos    = cpy._curWheelPos;
    _prevWheelPos   = cpy._prevWheelPos;
    _statusTime     = cpy._statusTime;
}

void MouseButtonStatus::clear()
{
    _buttons        = Qt::NoButton;
    _keys           = Qt::NoModifier;
    _curPos         = QPoint(0,0);
    _prevPos        = QPoint(0,0);
    _curWheelPos    = QPoint(0,0);
    _prevWheelPos   = QPoint(0,0);
    _statusTime.restart();
}

void MouseButtonStatus::setStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys)
{
    _buttons = buttons;
    _keys = keys;
    _statusTime.restart();
}

bool MouseButtonStatus::isStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys) const
{
    return (_buttons == buttons && _keys == keys); // these compare as integers
}

void MouseButtonStatus::setPos(const QPoint& pos, const QPoint& wheelPos)
{
    _prevPos = _curPos = pos;
    _prevWheelPos = _curWheelPos = wheelPos;
}

void MouseButtonStatus::updatePos(const QPoint& pos)
{
    if (_curPos != pos)
    {
        _prevPos = _curPos;
        _curPos = pos;
    }
}

void MouseButtonStatus::updateWheelPos(const QPoint& pos)
{
    if (_curWheelPos != pos)
    {
        _prevWheelPos = _curWheelPos;
        _curWheelPos = pos;
    }
}

const QPoint& MouseButtonStatus::curPos() const
{
    return _curPos;
}

const QPoint& MouseButtonStatus::prevPos() const
{
    return _prevPos;
}

QPoint MouseButtonStatus::posChange() const
{
    return _curPos - _prevPos;
}

int MouseButtonStatus::posChangeX() const
{
    return _curPos.x() - _prevPos.x();
}

int MouseButtonStatus::posChangeY() const
{
    return _curPos.y() - _prevPos.y();
}

const QPoint& MouseButtonStatus::curWheelPos() const
{
    return _curWheelPos;
}

const QPoint& MouseButtonStatus::prevWheelPos() const
{
    return _prevWheelPos;
}

bool MouseButtonStatus::isDown() const
{
    return (_buttons != Qt::NoButton || _keys != Qt::NoModifier);
}

bool MouseButtonStatus::hasMoved() const
{
    return (_curPos.x() != _prevPos.x() || _curPos.y() != _prevPos.y());
}

Qt::MouseButtons MouseButtonStatus::buttons() const
{
    return _buttons;
}

Qt::KeyboardModifiers MouseButtonStatus::keys() const
{
    return _keys;
}

unsigned int MouseButtonStatus::statusAge() const
{
    return _statusTime.elapsed();
}
