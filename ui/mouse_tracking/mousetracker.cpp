#include "ui/mouse_tracking/mousetracker.h"
#include "logging/debuglogger.h"

MouseTracker::MouseTracker(QObject* parent /*= 0*/) :
    QObject(parent),
    _clickDelayMax(cDefaultClickDelayMax)
{

}

void MouseTracker::mouseMove(QMouseEvent* e)
{
    if (!updateStatus(e->buttons(), e->modifiers(), e->pos(), QPoint(0, 0)))
    {
        if (_status.isDown())
            emit mouseDrageMove(&_status);
        else
            emit mouseMove(&_status);
    }
}

void MouseTracker::mousePress(QMouseEvent* e)
{
    updateStatus(e->buttons(), e->modifiers(), e->pos(), QPoint(0, 0));
}

void MouseTracker::mouseRelease(QMouseEvent* e)
{
    DebugLog(dlDebug, "MouseTracker::mousePress buttons %lu", (unsigned long)e->buttons());
    updateStatus(e->buttons(), e->modifiers(), e->pos(), QPoint(0, 0));
}

void MouseTracker::mouseDblClick(QMouseEvent* e)
{
    updateStatus(e->buttons(), e->modifiers(), e->pos(), QPoint(0, 0));
    emit mouseDblClicked(&_status);
    _status.clear();
}

void MouseTracker::mouseWheel(QWheelEvent* e)
{
    QPoint wheelPos;
    if (e->orientation() == Qt::Horizontal)
        wheelPos.setX(e->delta());
    else
        wheelPos.setY(e->delta());
    if (!updateStatus(e->buttons(), e->modifiers(), e->pos(), wheelPos))
        emit mouseWheel(&_status);
}

bool MouseTracker::updateStatus(Qt::MouseButtons buttons, Qt::KeyboardModifiers keys, const QPoint& pos, const QPoint& wheelPos)
{
    _status.updatePos(pos);
    _status.updateWheelPos(wheelPos);
    if (!_status.isStatus(buttons, keys))
    {
        //DebugLog(dlDebug, "MouseTracker::updateStatus not match, isDown %c, hasMoved %c, age %lu", BoolYN(_status.isDown()), BoolYN(_status.hasMoved()), _status.statusAge());
        if (!_status.isDown())
        {
            if (_status.hasMoved())
                emit mouseMove(&_status);
        }
        else
        {
            if (_status.statusAge() <= _clickDelayMax)
                emit mouseClicked(&_status);
            else
                emit mouseDrageEnd(&_status);
        }
        _status.setStatus(buttons, keys);
        _status.setPos(pos, wheelPos);
        return true;
    }
    return false;
}

const MouseButtonStatus& MouseTracker::buttonStatus() const
{
    return _status;
}
