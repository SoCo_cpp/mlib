#include "multimedia/waveformpainter.h"

#include <QByteArray>
#include <QBuffer>
#include <QPixmap>
#include <QAudioDecoder>
#include <QAudioBuffer>
#include <QtMath>
#include <QPainter>
#include <QPen>
#include "logging/debuglogger.h"

WaveformPainter::WaveformPainter(QObject* parent) :
	QObject(parent),
	_state(stNone),
	_needsDecoded(false),
	_needsPainted(false),
	_paintWhenDecoded(false),
	_start(0),
	_length(0),
	_colorFG(Qt::white),
	_colorBG(Qt::black),
	_pixmap(0),
	_pcmAudioData(0),
	_decodedAudioData(0),
	_decodeBuffer(0),
	_decoder(0),
	_painter(0)
{
	_painter = new QPainter();
	_audioFormat.setByteOrder(QAudioFormat::LittleEndian);
	_audioFormat.setCodec("audio/pcm");
	_audioFormat.setSampleSize(16);
	_audioFormat.setSampleType(QAudioFormat::SignedInt);
	_audioFormat.setChannelCount(1);
	_audioFormat.setSampleRate(8000);

}

WaveformPainter::~WaveformPainter()
{
	clear();
	if (_painter)
		delete _painter;
}

/*static*/
qint64 WaveformPainter::audioDuration(const QAudioFormat &format, qint64 bytes)
{
	return (bytes * 1000) / (format.sampleRate() * format.channelCount() * (format.sampleSize() / 8));
}

/*static*/
qint64 WaveformPainter::audioLength(const QAudioFormat &format, qint64 timeMs)
{
	qint64 result = (format.sampleRate() * format.channelCount() * (format.sampleSize() / 8));
	result *= (timeMs / 1000);
	result -= result % (format.channelCount() * format.sampleSize());
	return result;
}

void WaveformPainter::clear()
{
	//DebugLog(dlDebug, "WaveformPainter::clear clearing");
	if (isBusy())
		stop();
	_needsDecoded = false;
	_needsPainted = false;
	_paintWhenDecoded = false;

	cleanupDecoding();
	_pcmAudioData = 0;
	_pixmap = 0;
	setState(stNone);
}

bool WaveformPainter::isBusy()
{
	return (_state != stNone);
}

void WaveformPainter::setColors(const QColor& colFG, const QColor& colBG)
{
	_colorFG = colFG;
	_colorBG = colBG;
}

void WaveformPainter::setStart(quint64 startMs)
{
	_start = startMs;
}

void WaveformPainter::setLength(quint64 lengthMs)
{
	_length = lengthMs;
}

void WaveformPainter::setRange(quint64 startMs, quint64 lengthMs)
{
	_start = startMs;
	_length = lengthMs;
}

const QAudioFormat& WaveformPainter::audioFormat() const
{
	return _audioFormat;
}

void WaveformPainter::setState(State st)
{
	_state = st;
	emit stateChanged();
}

WaveformPainter::State WaveformPainter::state() const
{
	return _state;
}

void WaveformPainter::setPixmap(QPixmap* ppixmap)
{
	_needsPainted = true;
	_pixmap = ppixmap;
}

QPixmap* WaveformPainter::pixmap() const
{
	return _pixmap;
}

void WaveformPainter::setAudioData(QByteArray* pcmAudioData)
{
	_pcmAudioData = pcmAudioData;
}

QByteArray* WaveformPainter::audioData() const
{
	return _pcmAudioData;
}
bool WaveformPainter::decodeAudioFile(const QString& fileName, bool paintWhenFinished /*= true*/)
{
	if (isBusy())
	{
		DebugLog(dlWarn, "WaveformPainter::decodeAudioData still busy");
		return false;
	}
	//DebugLog(dlDebug, "WaveformPainter::decodeAudioData decoding file: %s", qPrintable(fileName));
	setState(stDecoding);
	_needsDecoded = true;
	_needsPainted = true;
	_paintWhenDecoded = paintWhenFinished;
	_pcmAudioData = 0;
	if (!_decoder)
	{
		_decoder = new QAudioDecoder(this);
		_decoder->setAudioFormat(_audioFormat);
		connect(_decoder, SIGNAL(bufferReady()), this, SLOT(decoderBufferReady()));
		connect(_decoder, SIGNAL(finished()), this, SLOT(decoderFinished()));
		connect(_decoder, SIGNAL(error(QAudioDecoder::Error)), this, SLOT(decoderError()));
	}
	if (!_decodedAudioData)
		_decodedAudioData = new QByteArray();
	else
		_decodedAudioData->clear();
	_decoder->setAudioFormat(_audioFormat);
	_decoder->setSourceFilename(fileName);
	_decoder->start();
	return true;
}

bool WaveformPainter::decodeAudioData(QByteArray* baAudioData, bool paintWhenFinished /*= true*/)
{
	if (isBusy())
	{
		DebugLog(dlWarn, "WaveformPainter::decodeAudioData still busy");
		return false;
	}
	//DebugLog(dlDebug, "WaveformPainter::decodeAudioData decoding data");
	setState(stDecoding);
	_needsDecoded = true;
	_needsPainted = true;
	_paintWhenDecoded = paintWhenFinished;
	_pcmAudioData = 0;
	if (!_decoder)
	{
		_decoder = new QAudioDecoder(this);
		_decoder->setAudioFormat(_audioFormat);
		connect(_decoder, SIGNAL(bufferReady()), this, SLOT(decoderBufferReady()));
		connect(_decoder, SIGNAL(finished()), this, SLOT(decoderFinished()));
		connect(_decoder, SIGNAL(error(QAudioDecoder::Error)), this, SLOT(decoderError()));
	}
	if (!_decodedAudioData)
		_decodedAudioData = new QByteArray();
	else
		_decodedAudioData->clear();
	if (!_decodeBuffer)
		_decodeBuffer = new QBuffer(this);
	_decodeBuffer->close();
	_decodeBuffer->setBuffer(baAudioData);
	_decodeBuffer->open(QIODevice::ReadOnly);
	_decoder->setAudioFormat(_audioFormat);
	_decoder->setSourceDevice(_decodeBuffer);
	_decoder->start();
	return true;
}

QByteArray* WaveformPainter::decodedAudioData() const
{
	return _decodedAudioData;
}

void WaveformPainter::cleanupDecoding() // releases temprorary memory and buffers for decoding
{
	if (isBusy())
		stop();
	if (_decoder)
	{
		_decoder->deleteLater();
		_decoder = 0;
	}
	if (_decodeBuffer)
	{
		_decodeBuffer->close();
		_decodeBuffer->deleteLater();
		_decodeBuffer = 0;
	}
	if (_decodedAudioData)
	{
		delete _decodedAudioData;
		_decodedAudioData = 0;
	}
	_needsDecoded = true;
}

bool WaveformPainter::paint(bool ignoreBusy /*= false*/)
{
	if (!ignoreBusy && isBusy())
	{
		DebugLog(dlWarn, "WaveformPainter::paint still busy");
		return false;
	}
	if (!_pixmap || (!_decodedAudioData && !_pcmAudioData) )
	{
		DebugLog(dlWarn, "WaveformPainter::paint image and data not ready");
		return false;
	}
	if (!paint(_pixmap, (_pcmAudioData ? _pcmAudioData : _decodedAudioData) ))
	{
		DebugLog(dlWarn, "WaveformPainter::paint paint with set objects failed");
		return false;
	}
	return true;
}

bool WaveformPainter::paint(QPixmap* pixmap, QByteArray* pcmAudioData) // PCMS16LE format required
{
	if (!pixmap || !pcmAudioData)
	{
		DebugLog(dlWarn, "WaveformPainter::paint parameter check failed");
		return false;
	}
	if (_state != stPainting)
		setState(stPainting);

	const int channelBytes = _audioFormat.sampleSize() / 8;
	const int sampleBytes = _audioFormat.channelCount() * channelBytes;
	const int startOffsetBytes = (int)(_start == 0 ? 0 : audioLength(_audioFormat, _start));
	const int lengthBytes = (int)(_length == 0 ? 0 : audioLength(_audioFormat, _length));
	int dataBytes = pcmAudioData->size() - startOffsetBytes;
	if (dataBytes <= 0)
	{
		DebugLog(dlDebug, "WaveformPainter::paint start offset exceeds size or dataBytes is  empty. calc data bytes %d full data bytes: %d, start offset bytes: %d, start offset Ms: %d", dataBytes, pcmAudioData->size(), startOffsetBytes, (int)_start);
		if (!_painter->begin(pixmap))
		{
			DebugLog(dlWarn, "WaveformPainter::paint (start exceeds size) painter begin failed");
			return false;
		}
		_painter->fillRect(pixmap->rect(), _colorBG);
		if (!_painter->end())
		{
			DebugLog(dlWarn, "WaveformPainter::paint (start exceeds size) painter end failed");
			return false;
		}
		return true; // just a debug warning
	}
	if (lengthBytes != 0 && dataBytes > lengthBytes)
		dataBytes = lengthBytes;
	int numSamples = dataBytes / sampleBytes;
	qint16* ptr = reinterpret_cast<qint16*>(pcmAudioData->data() + startOffsetBytes);
	Q_ASSERT(ptr);

	if (!_painter->begin(pixmap))
	{
		DebugLog(dlWarn, "WaveformPainter::paint painter begin failed");
		return false;
	}
	_painter->setPen(_colorFG);
	_painter->fillRect(pixmap->rect(), _colorBG);

	QPointF rPoint(0.0 ,0.0);
	QLine line(QPoint(0, 0), QPoint(0, 0));
	for (int i = 0; i < numSamples;i++)
	{
		rPoint.setX( (((qreal)i) / numSamples) * pixmap->width() );
		rPoint.setY( (((((qreal)(*ptr)) / PCMS16MaxAmplitude) + 1.0) / 2.0) * pixmap->height() );
		line.setP2(rPoint.toPoint());
		_painter->drawLine(line);
		line.setP1(line.p2());
		ptr += _audioFormat.channelCount();
	}
	if (!_painter->end())
	{
		DebugLog(dlWarn, "WaveformPainter::paint painter end failed");
		return false;
	}
	_needsPainted = false;
	setState(stNone);
	emit paintingReady();
	return true;
}

int WaveformPainter::timeToPixels(quint64 timeMs, bool offsetFromStart /*= false*/, bool ignoreRange /*= false*/)
{
	if (!_pixmap || (!_decodedAudioData && !_pcmAudioData) )
	{
		DebugLog(dlWarn, "WaveformPainter::timeToPixels image and data not ready");
		return -1;
	}
	return timeToReal(timeMs, offsetFromStart, ignoreRange) * _pixmap->width();
}

qreal WaveformPainter::timeToReal(quint64 timeMs, bool offsetFromStart /*= false*/, bool ignoreRange /*= false*/)
{
	return timeToReal((_pcmAudioData ? _pcmAudioData : _decodedAudioData), timeMs, offsetFromStart, ignoreRange);
}

qreal WaveformPainter::timeToReal(QByteArray* pcmAudioData, quint64 timeMs, bool offsetFromStart /*= false*/, bool ignoreRange /*= false*/)
{
	if (!pcmAudioData)
	{
		DebugLog(dlWarn, "WaveformPainter::timeToReal data not ready");
		return -1.0;
	}
	const quint64 lengthBytes = pcmAudioData->size();
	quint64 lengthMs = audioDuration(_audioFormat, lengthBytes);
	if (!ignoreRange)
	{
		lengthMs -= _start;
		if (_length && _length < lengthMs)
			lengthMs = _length;
		if (lengthMs <= 0)
			return 0.0; // start past end
		if (offsetFromStart)
			timeMs -= _start;
	}
	return (((qreal)timeMs) / lengthMs);
}

void WaveformPainter::start()
{
	//DebugLog(dlDebug, "WaveformPainter::start trying to start");
	if (!paint())
	{
		DebugLog(dlWarn, "WaveformPainter::start paint failed");
		emit failure();
	}
}

void WaveformPainter::stop()
{
	//DebugLog(dlDebug, "WaveformPainter::start stop to stop");
	_paintWhenDecoded = false;
	if (_decoder)
		_decoder->stop();
	if (_decodeBuffer)
		_decodeBuffer->close();
	if (isBusy())
	{
		setState(stCanceled);
		emit canceled();
	}
}

void WaveformPainter::decoderBufferReady()
{
	//DebugLog(dlDebug, "WaveformPainter::decoderBufferReady more buffer");
	if (_needsDecoded && _state == stDecoding &&  _decoder && _decodedAudioData)
	{
		QAudioBuffer audioBuffer = _decoder->read();
		if (!audioBuffer.isValid())
		{
			DebugLog(dlWarn, "WaveformPainter::decoderBufferReady audioBuffer isinvalid");
			return;
		}
		_decodedAudioData->append(audioBuffer.constData<char>(), audioBuffer.byteCount());
		//DebugLog(dlDebug, "WaveformPainter::decoderBufferReady appended %d bytes, total %d", audioBuffer.byteCount(), _decodedAudioData->size());
	}
}

void WaveformPainter::decoderFinished()
{
	//DebugLog(dlDebug, "WaveformPainter::decoderFinished finished error string: %s", (_decoder ? qPrintable(_decoder->errorString()) : "<decoder null>"));
	if (_decodeBuffer)
		_decodeBuffer->close();
	if (_needsDecoded && _state == stDecoding && _decodedAudioData)
	{
		_needsDecoded = false;
		if (!_paintWhenDecoded)
		{
			setState(stNone); // done
			emit decodedDataReady();
		}
		else
		{
			setState(stPainting);
			emit decodedDataReady();
			if (!paint(true/*ignoreBusy*/))
			{
				DebugLog(dlWarn, "WaveformPainter::decoderFinished paint (whenDoneDecoding) failed");
				emit failure();
				return;
			}
		}
	}
}

void WaveformPainter::decoderError()
{
	if (_decoder)
	{
		DebugLog(dlWarn, "WaveformPainter::decoderError decode failed with error: %s", qPrintable(_decoder->errorString()));
		emit failure();
	}

}
