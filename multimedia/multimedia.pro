
!defined(MLIB_SRC,var):error("MLIB multimedia (mlib/multimedia/multimedia.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/multimedia

message("MLIB including multimedia...")

QT *= multimedia

SOURCES +=  $$MSRC/waveformpainter.cpp

HEADERS +=  $$MSRC/waveformpainter.h
