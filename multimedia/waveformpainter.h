#ifndef WAVEFORMPAINTER_H
#define WAVEFORMPAINTER_H

#include <QObject>
#include <QAudioFormat>
#include <QColor>

// PCMS16LE format required, other formats must be decoded

class QAudioDecoder; // predeclared
class QPixmap; // predeclared
class QByteArray; // predeclared
class QBuffer; // predeclared
class QPainter; // predeclared

class WaveformPainter : public QObject
{
	Q_OBJECT
public:
	static const quint16 PCMS16MaxAmplitude = 32768;
	static qint64 audioDuration(const QAudioFormat &format, qint64 bytes); // returns Ms
	static qint64 audioLength(const QAudioFormat &format, qint64 timeMs);


	typedef enum {stNone = 0, stPainting, stDecoding, stCanceled} State;

	WaveformPainter(QObject* parent = 0);
	~WaveformPainter();

	void clear();
	bool isBusy();

	void setColors(const QColor& colFG, const QColor& colBG);

	void setStart(quint64 startMs);
	void setLength(quint64 lengthMs);
	void setRange(quint64 startMs, quint64 lengthMs);

	const QAudioFormat& audioFormat() const;

	WaveformPainter::State state() const;

	void setPixmap(QPixmap* ppixmap);
	QPixmap* pixmap() const;

	void setAudioData(QByteArray* pcmAudioData); // PCMS16LE format required
	QByteArray* audioData() const;

	bool decodeAudioFile(const QString& fileName, bool paintWhenFinished = true);
	bool decodeAudioData(QByteArray* baAudioData, bool paintWhenFinished = true);
	QByteArray* decodedAudioData() const;
	void cleanupDecoding(); // releases temprorary memory and buffers for decoding

	bool paint(bool ignoreBusy = false);
	bool paint(QPixmap* pixmap, QByteArray* pcmAudioData); // PCMS16LE format required

	int timeToPixels(quint64 timeMs, bool offsetFromStart = false, bool ignoreRange = false); // -1 on failure
	qreal timeToReal(quint64 timeMs, bool offsetFromStart = false, bool ignoreRange = false); // -1 on failure
	qreal timeToReal(QByteArray* pcmAudioData, quint64 timeMs, bool offsetFromStart = false, bool ignoreRange = false); // -1 on failure

protected:
	State _state;
	bool _needsDecoded;
	bool _needsPainted;
	bool _paintWhenDecoded;

	quint64 _start;
	quint64 _length;

	QColor _colorFG;
	QColor _colorBG;

	QAudioFormat _audioFormat;
	QPixmap* _pixmap;
	QByteArray* _pcmAudioData;
	QByteArray* _decodedAudioData;
	QBuffer* _decodeBuffer;
	QAudioDecoder* _decoder;
	QPainter* _painter;

	void setState(State st);
signals:
	void stateChanged();
	void decodedDataReady();
	void paintingReady();
	void failure();
	void canceled();

protected slots:
	void decoderBufferReady();
	void decoderFinished();
	void decoderError();

public slots:
	void start();
	void stop();

};

#endif // WAVEFORMPAINTER_H
