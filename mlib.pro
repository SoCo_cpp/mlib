############################################
# MLIB_CONFIG :
#  - file
#  - logging
#  - opengl
#  - glgeometry
#  - glview
#  - comm
#  - irc
#  - map
#  - multimedia
#  - unixsignals
#  - threads
#  - ui
############################################

!defined(MLIB_SRC,var):error("MLIB (mlib/mlib.pro) - MLIB_SRC not defined.")
!defined(MLIB_CONFIG,var):error("MLIB (mlib/mlib.pro) - MLIB_CONFIG not defined.")

message("Including MLIB, souce path: $$MLIB_SRC")

QT *= core
CONFIG *= qt

INCLUDEPATH += $$MLIB_SRC

contains(MLIB_CONFIG, file):        include(file/file.pro)
contains(MLIB_CONFIG, logging):     include(logging/logging.pro)
contains(MLIB_CONFIG, map):         include(map/map.pro)
contains(MLIB_CONFIG, comm){
    include(comm/comm.pro)
    contains(MLIB_CONFIG, irc):     include(comm/irc/irc.pro)
}
contains(MLIB_CONFIG, multimedia):  include(multimedia/multimedia.pro)
contains(MLIB_CONFIG, opengl){
    include(opengl/opengl.pro)
    contains(MLIB_CONFIG, glgeometry):  include(opengl/geometry/geometry.pro)
    contains(MLIB_CONFIG, glview):      include(opengl/view/view.pro)
}

contains(MLIB_CONFIG, unixsignals):     include(os/unixsignals.pro)

contains(MLIB_CONFIG, threads):     include(threads/threads.pro)
contains(MLIB_CONFIG, ui):          include(ui/ui.pro)
