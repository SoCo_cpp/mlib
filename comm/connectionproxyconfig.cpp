#include "connectionproxyconfig.h"

ConnectionProxyConfig::ConnectionProxyConfig(QObject* parent /*= 0*/) :
	QObject(parent),
	_enabled(false),
	_type(QNetworkProxy::NoProxy),
	_hostName(""),
	_hostPort(0),
	_promptCredentials(false),
	_user(""),
	_password("")
{
}

ConnectionProxyConfig::ConnectionProxyConfig(const ConnectionProxyConfig& cpy) :
	QObject(cpy.parent())
{
	copy(cpy);
}

ConnectionProxyConfig& ConnectionProxyConfig::operator=(const ConnectionProxyConfig& rhs)
{
	copy(rhs);
	return *this;
}

void ConnectionProxyConfig::copy(const ConnectionProxyConfig& cpy)
{
	_enabled			= cpy._enabled;
	_type				= cpy._type;
	_hostName			= cpy._hostName;
	_hostPort			= cpy._hostPort;
	_promptCredentials	= cpy._promptCredentials;
	_user				= cpy._user;
	_password			= cpy._password;
	emit changed();
}

void ConnectionProxyConfig::clear()
{
	_enabled = false;
	_type = QNetworkProxy::NoProxy;
	_hostName = "";
	_hostPort = 0;
	_promptCredentials = false;
	_user = "";
	_password = "";
	emit changed();
}

bool ConnectionProxyConfig::isEnabled(bool requireTypeEnabled /*= true*/) const
{
	return (_enabled && (!requireTypeEnabled || _type != QNetworkProxy::NoProxy));
}

void ConnectionProxyConfig::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
	emit changed();
}

QNetworkProxy::ProxyType ConnectionProxyConfig::type() const
{
	return _type;
}

void ConnectionProxyConfig::setType(QNetworkProxy::ProxyType proxytype)
{
	_type = proxytype;
	emit changed();
}

const QString& ConnectionProxyConfig::hostName() const
{
	return _hostName;
}

void ConnectionProxyConfig::setHostName(const QString& host)
{
	_hostName = host;
	emit changed();
}

quint16 ConnectionProxyConfig::hostPort() const
{
	return _hostPort;
}

void ConnectionProxyConfig::setHostPort(quint16 port)
{
	_hostPort = port;
	emit changed();
}

void ConnectionProxyConfig::setPromptCredentials(bool enabled /*= true*/)
{
	_promptCredentials	= enabled;
	if (_promptCredentials)
	{
		_user		= "";
		_password	= "";
	}
	emit changed();
}

void ConnectionProxyConfig::setCredentials(const QString& user, const QString& password /*= QString()*/)
{
	_promptCredentials = false;
	_user		= user;
	_password	= password;
	emit changed();
}

void ConnectionProxyConfig::setUser(const QString& sUser)
{
	_user = sUser;
	emit changed();
}

void ConnectionProxyConfig::setPassword(const QString& sPassword)
{
	_password= sPassword;
	emit changed();
}

void ConnectionProxyConfig::clearCredentials(bool skipChangedSignal /*= false*/)
{
	_promptCredentials	= false;
	_user				= "";
	_password			= "";
	if (!skipChangedSignal)
		emit changed();
}

bool ConnectionProxyConfig::promptCredentials() const
{
	return _promptCredentials;
}

bool ConnectionProxyConfig::hasCredentials(bool requirePassword /*= false*/) const
{
	if (_user.isEmpty())
		return false;
	if (requirePassword && _password.isEmpty())
		return false;
	return true;
}

const QString& ConnectionProxyConfig::user() const
{
	return _user;
}

const QString& ConnectionProxyConfig::password() const
{
	return _password;
}

bool ConnectionProxyConfig::isValid(bool requireEnabled /*= false*/) const
{
	if (requireEnabled && !isEnabled())
		return false;
	if (isEnabled())
	{
		if (_hostName.isEmpty())
			return false;
		if (_hostPort == 0)
			return false;
	}
	return true;
}

const QNetworkProxy& ConnectionProxyConfig::proxy()
{
	_proxy = QNetworkProxy();
	if (!isValid(true/*requireEnabled*/))
    {
        qDebug("ConnectionProxyConfig::proxy isValid failed");
		_proxy.setType(QNetworkProxy::NoProxy);
    }
	else
	{
        //qDebug("ConnectionProxyConfig::proxy prompt %c, type %d, usr '%s', pass '%s'", (_promptCredentials?'T':'F'), (int)_type, qPrintable(_user), qPrintable(_password));
        _proxy.setType(_type);
		_proxy.setHostName(_hostName);
		_proxy.setPort(_hostPort);
        if (!_promptCredentials && !_user.isEmpty() && !_password.isEmpty())
		{
            //qDebug("ConnectionProxyConfig::proxy created with credentials");
            _proxy.setUser(_user);
			_proxy.setPassword(_password);
		}
        //else qDebug("ConnectionProxyConfig::proxy no credentials");
	}
	return _proxy;
}
