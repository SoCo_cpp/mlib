#include <QTimer>
#include <QDebug>
#include "compositeconnection.h"

CompositeConnection::CompositeConnection(QObject* parent /*= 0*/) :
	QObject(parent),
	_status(csNone),
	_wanted(cwNone),
	_persistent(false),
	_config()
{
    qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError");
    connect(&_config, SIGNAL(changed()), this, SIGNAL(configChanged()));

	connect(&_serialPort, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(onSerialError(QSerialPort::SerialPortError)));
    connect(&_serialPort, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

	connect(&_tcpSocket, SIGNAL(connected()), this, SLOT(onSocketConnected()));
	connect(&_tcpSocket, SIGNAL(disconnected()), this, SLOT(onSocketDisconnected()));
	connect(&_tcpSocket, SIGNAL(encrypted()), this, SLOT(onSocketEncrypted()));
	connect(&_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onSocketError(QAbstractSocket::SocketError)));
	//connect(&_tcpSocket, SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)), this, SLOT(onProxyAuthRequired(const QNetworkProxy & proxy, QAuthenticator * authenticator)));
    connect(&_tcpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

	connect(&_udpSocket, SIGNAL(connected()), this, SLOT(onSocketConnected()));
	connect(&_udpSocket, SIGNAL(disconnected()), this, SLOT(onSocketDisconnected()));
	connect(&_udpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onSocketError(QAbstractSocket::SocketError)));
    //connect(&_udpSocket, SIGNAL(proxyAuthenticationRequired(QNetworkProxy,QAuthenticator*)), this, SLOT(onProxyAuthRequired(const QNetworkProxy & proxy, QAuthenticator * authenticator)));
    connect(&_udpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

	connect(&_tcpServer, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(onSocketError(QAbstractSocket::SocketError)));
    connect(&_tcpServer, SIGNAL(newConnection()), this, SLOT(onNewServerConnection()));

    connect(this, SIGNAL(doConnectHost()), this, SLOT(onConnectHost()));
    connect(this, SIGNAL(doDisconnectHost()), this, SLOT(onDisconnectHost()));
    connect(this, SIGNAL(doReconnectHost()), this, SLOT(onReconnectHost()));

}

QString CompositeConnection::lastError() const
{
  return _lastError;
}

CompositeConnection::ConnectionStatus CompositeConnection::status() const
{
	return _status;
}

QString CompositeConnection::statusString() const
{
  switch(_status)
  {
    case csNone:
       return "None";
    case csDisconnected:
      return "Disconnected";
    case csConnecting:
      return "Connecting";
    case csConnected:
      return "Connected";
    case csTimedOut:
      return "Timed Out";
    case csFailed:
      return "Failed";
    default:
      return "Unknown";
  } // switch(_status)
}

const CompositeConnectionConfig& CompositeConnection::config() const
{
  return _config;
}

CompositeConnectionConfig& CompositeConnection::config()
{
  return _config;
}

/*protected*/
void CompositeConnection::setStatus(ConnectionStatus status)
{
    //qDebug("setStatus Connection status %d, last status %d last error: %s", (int)status, (int) _status, qPrintable(_lastError));
	ConnectionStatus lastStatus = _status;
	_status = status;
	emit statusChanged();
	if (_status == csConnected && lastStatus != csConnected)
	{
		emit connected();
		if (_persistent && _wanted == cwDisconnect)
			QTimer::singleShot(20, this, SLOT(onPersistenCheck()));
	}
	else if (lastStatus == csConnected && _status != csConnected)
	{
		emit disconnected();
		if (_persistent && _wanted == cwConnect)
			QTimer::singleShot(20, this, SLOT(onPersistenCheck()));
	}
}

bool CompositeConnection::persistent() const
{
	return _persistent;
}

void CompositeConnection::setPersistent(bool enabled /*= true*/)
{
	_persistent = enabled;
}

bool CompositeConnection::isConnected() const
{
	return _status == csConnected;
}

bool CompositeConnection::isDisconnected(bool persistentRequiresFullDisconnect /*= true*/)
{
    //qDebug("isDisconnected Connection status %d", (int) _status);
    switch (_status)
	{
		case csNone:
		case csDisconnected:
			return true;
		case csTimedOut:
		case csFailed:
			if (!_persistent || !persistentRequiresFullDisconnect)
				return true;
		default:
			return false;
	} // switch
	return false;
}

bool CompositeConnection::isConnectFailed()
{
	return (_status == csTimedOut || _status == csFailed);
}

bool CompositeConnection::canIODevice() const
{
	if (!_config.isValid())
		return false;
	return _config.type() != CompositeConnectionConfig::ctTcpServer;
}

QIODevice* CompositeConnection::asIODevice()
{
	if (!_config.isValid())
		return 0;
	switch (_config.type())
	{
		case CompositeConnectionConfig::ctSerial:
			return dynamic_cast<QIODevice*>(&_serialPort);
		case CompositeConnectionConfig::ctTcp:
			return dynamic_cast<QIODevice*>(&_tcpSocket);
		case CompositeConnectionConfig::ctUdp:
			return dynamic_cast<QIODevice*>(&_udpSocket);
		case CompositeConnectionConfig::ctTcpServer:
			return 0; // not supported; not derived from QIODevice
		default:
			return 0;
	} // switch
}

QSerialPort& CompositeConnection::serialPort()
{
	return _serialPort;
}

const QSerialPort& CompositeConnection::serialPort() const
{
	return _serialPort;
}

QSslSocket& CompositeConnection::tcpSocket()
{
	return _tcpSocket;
}

const QSslSocket& CompositeConnection::tcpSocket() const
{
	return _tcpSocket;
}

QTcpServer& CompositeConnection::tcpServer()
{
	return _tcpServer;
}

const QTcpServer& CompositeConnection::tcpServer() const
{
	return _tcpServer;
}

QUdpSocket& CompositeConnection::udpSocket()
{
	return _udpSocket;
}

const QUdpSocket& CompositeConnection::udpSocket() const
{
	return _udpSocket;
}

void CompositeConnection::wantConnect()
{
	_wanted = cwConnect;
	setPersistent();
	QTimer::singleShot(20, this, SLOT(onPersistenCheck())); // call this slot again in delay
}

void CompositeConnection::wantDisconnect()
{
	_wanted = cwDisconnect;
	QTimer::singleShot(20, this, SLOT(onPersistenCheck())); // call this slot again in delay
}


void CompositeConnection::onConnectHost()
{
    if (!connectHost())
        qDebug("CompositeConnection::onConnectHost connectHost failed");
}

void CompositeConnection::onDisconnectHost()
{
    if (!disconnectHost())
        qDebug("CompositeConnection::onDisconnectHost disconnectHost failed");
}

void CompositeConnection::onReconnectHost()
{
    if (!reconnectHost())
        qDebug("CompositeConnection::onReconnectHost reconnectHost failed");
}

bool CompositeConnection::connectHost(bool failIfConnecting /*= true*/, bool wantConnect /*= true*/)
{
    //qDebug("connectHost type %d host %s:%d (ssl %c) | serialPort %s", (int)_config.type(), qPrintable(_config.hostName()), _config.hostPort(), (_config.useSsl() ? 'Y' : 'N'), qPrintable(_config.serialPortName()));
    if (!_config.isValid())
	{
		_lastError = "config not valid";
		return false;
	}
	if (!isDisconnected() && failIfConnecting)
	{
		_lastError = "connecting already";
		return false;
	}
	switch (_config.type())
	{
		case CompositeConnectionConfig::ctSerial:
			// these setters only fail if setting while already open and it fails
			_serialPort.setPortName(_config.serialPortName());
			setStatus(csConnecting);
			if (!_serialPort.open(QIODevice::ReadWrite))
			{
				_lastError = QString("open serialPort failed. portName: %1, baud: %2").arg(_config.serialPortName()).arg(_config.serialBaud());
				setStatus(csFailed);
				return false;
			}
            _serialPort.setBaudRate(_config.serialBaud());
            _serialPort.setFlowControl(_config.serialFlowControl());
            _serialPort.setStopBits(_config.serialStopBits());
            setStatus(csConnected);
			break;
		case CompositeConnectionConfig::ctTcp:
			_tcpSocket.setProxy(_config.proxyConfig().proxy());
			setStatus(csConnecting);
			if (_config.useSsl())
				_tcpSocket.connectToHostEncrypted(_config.hostName(), _config.hostPort());
			else
				_tcpSocket.connectToHost(_config.hostName(), _config.hostPort());
			// wait for encrypted/connected signal
			break;
		case CompositeConnectionConfig::ctUdp:
			_udpSocket.setProxy(_config.proxyConfig().proxy());
			setStatus(csConnecting);
			if (_config.isServer())
				if (!_udpSocket.bind(_config.hostAddress(), _config.hostPort()))
				{
					_lastError = QString("bind Udp failed. hostAddress: %1, port: %2").arg(_config.hostAddress().toString()).arg(_config.hostPort());
					setStatus(csFailed);
					return false;
				}
			// Udp is connectionless except for binding to the listening port
			setStatus(csConnected);
			break;
		case CompositeConnectionConfig::ctTcpServer:
			setStatus(csConnecting);
			if (!_tcpServer.listen(_config.hostAddress(), _config.hostPort()))
			{
				_lastError = QString("TcpServer listen failed. hostAddress: %1, port: %2").arg(_config.hostAddress().toString()).arg(_config.hostPort());
				setStatus(csFailed);
				return false;
			}
			setStatus(csConnected);
			break;
		default:
			_lastError = QString("CompositeConnection::connectHost unsupported type: %1").arg((int)_config.type());
			return false;
	} // switch
	if (wantConnect)
	  _wanted = cwConnect;
	if (_persistent)
	  QTimer::singleShot(20, this, SLOT(onPersistenCheck())); // call this slot again in delay
	return true;
}

bool CompositeConnection::disconnectHost(bool failIfDisconnected /*= true*/, bool wantDisconnect /*= false*/)
{
	if (failIfDisconnected && isDisconnected())
	{
		_lastError = "already disconnected";
		return false;
	}
	switch (_config.type())
	{
		case CompositeConnectionConfig::ctSerial:
			_serialPort.close();
			setStatus(csDisconnected);
			break;
		case CompositeConnectionConfig::ctTcp:
			_tcpSocket.disconnectFromHost();
			// wait for disconnect signal
			break;
		case CompositeConnectionConfig::ctUdp:
			_udpSocket.close();
			// wait for disconnect signal
			break;
		case CompositeConnectionConfig::ctTcpServer:
			_tcpServer.close();
			setStatus(csDisconnected);
			break;
		default:
			_lastError = QString("Host unsupported type: %1").arg((int)_config.type());
			return false;
	} // switch
	if (wantDisconnect)
	  _wanted = cwDisconnect;
	if (_persistent)
	  QTimer::singleShot(20, this, SLOT(onPersistenCheck())); // call this slot again in delay
	return true;
}

bool CompositeConnection::reconnectHost(bool failIfConnected /*= true*/)
{
	if (failIfConnected && isConnected())
	{
		_lastError =  "already connected";
		return false;
	}
	if (!disconnectHost(false/*failIfDisconnected*/))
	{
		_lastError = "disconnectHost failed";
		return false;
	}
	if (!connectHost())
	{
		_lastError = "connectHost failed";
		return false;
	}
	return true;
}

bool CompositeConnection::canRead()
{
	QIODevice* io = asIODevice();
	if (!io)
		return false;
	return io->atEnd();
}

qint64 CompositeConnection::readSize()
{
	QIODevice* io = asIODevice();
	if (!io)
		return 0;
	return io->bytesAvailable();
}

bool CompositeConnection::canReadLine()
{
	QIODevice* io = asIODevice();
	if (!io)
		return false;
	return io->canReadLine();
}

QByteArray CompositeConnection::readAll()
{
	QIODevice* io = asIODevice();
	if (!io)
		return QByteArray();
	return io->readAll();
}

QByteArray CompositeConnection::readLine(qint64 maxSize /*= 0*/) // empty on error
{
	QIODevice* io = asIODevice();
    if (!io)
    {
        qDebug("CompositeConnection::readLine asIODevice failed ");
        return QByteArray();
     }
    /*
    // Debug replacement for return io->readLine(maxSize);
    char data[2056];
    qint64 r = io->readLine(data, 2056);
    qDebug("CompositeConnection::readLine r: %ld ", (long)r);
    if (r <= 0)
        return QByteArray();
    return QByteArray(data, r);
    */
    return io->readLine(maxSize);
}

qint64 CompositeConnection::read(char* data, qint64 maxSize) // -1 error
{
	QIODevice* io = asIODevice();
	if (!io)
		return -1;
	return io->read(data, maxSize);
}

bool CompositeConnection::getChar(char* pc)
{
	QIODevice* io = asIODevice();
	if (!io)
		return false;
	return io->getChar(pc);
}

bool CompositeConnection::write(const QString& string)
{
	qint64 r = write(string.toLocal8Bit());
	return (r != -1);
}

qint64 CompositeConnection::write(const char* data, qint64 maxSize) // -1 error
{
	QIODevice* io = asIODevice();
	if (!io)
		return -1;
	return io->write(data, maxSize);
}

qint64 CompositeConnection::write(const QByteArray& byteArray) // -1 error
{
	QIODevice* io = asIODevice();
	if (!io)
		return -1;
	return io->write(byteArray);
}

bool CompositeConnection::putChar(char c)
{
	QIODevice* io = asIODevice();
	if (!io)
		return false;
	return io->putChar(c);
}

void CompositeConnection::onPersistenCheck()
{
	int delay = 200;
	switch (_wanted)
	{
		case cwNone:
			return; // we are done, return without reenabling timer
		case cwDisconnect:
            //qDebug("onPersistenCheck want Disconnect status %d....", (int) _status);
            if (isDisconnected()) // full disconnect only
				return; // we are done, return without reenabling timer
			else if (isConnected())
			{
                //qDebug("onPersistenCheck want Disconnect status %d....disconnectHost", (int) _status);
                if (!disconnectHost())
				{
					delay = 1500;
					_lastError = "disconnectHost failed";
				}
			}
			break;
		case cwConnect:
            //qDebug("onPersistenCheck want Connect status %d....", (int) _status);
			if (isConnected())
				return; // we are done, return without reenabling timer
            else if (isDisconnected(false/*persistentRequiresFullDisconnect*/))
            {
                if (isConnectFailed())
                {
                    if (!reconnectHost(false/*failIfConnecting*/))
                    {
                        delay = 1500;
                        _lastError = "reconnectHost failed";
                    }
                }
                else
                {
                    //qDebug("onPersistenCheck want Connect status %d....connectHost", (int) _status);
                    if (!connectHost(false/*failIfConnecting*/))
                    {
                        delay = 1500;
                        _lastError = "connectHost failed";
                    }
               }
			}
			break;
		default:
			_lastError = QString("unsupported wanted state: %1").arg((int)_wanted);
			return; // don't know what to do, return without reenabling timer
	} // switch
	QTimer::singleShot(delay, this, SLOT(onPersistenCheck())); // call this slot again in delay
}

void CompositeConnection::onSerialError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::NoError)
        return; // ignore NoError emitted on connection
    _lastError = QString("SerialPort error: %1").arg((int)error);
    emit connError(_lastError);
    setStatus(csFailed);
}

void CompositeConnection::onSocketConnected()
{
	if (!_config.useSsl())
		setStatus(csConnected);
	// SSL must wait for the encrypted signal to be fully connected
}

void CompositeConnection::onSocketEncrypted()
{
	setStatus(csConnected);
}

void CompositeConnection::onSocketDisconnected()
{
	setStatus(csDisconnected);
}

void CompositeConnection::onSocketError(QAbstractSocket::SocketError socketError) // _socket.errorString()
{
    QAbstractSocket* socket = dynamic_cast<QAbstractSocket*>(sender());
	if (socket)
        _lastError = QString("Socket error: %1").arg(socket->errorString());
    else
        _lastError = QString("Socket error: %1").arg((int)socketError);
    qDebug("onSocketError status was %d, error %s", (int) _status, qPrintable(_lastError));
    emit connError(_lastError);
    setStatus(csFailed);
}

void CompositeConnection::onSocketPeerVerifyError(const QSslError& sslError) // _socket.ignoreSslErrors(); or sslError.errorString()
{
	if (_config.ignoreInvalidSsl())
		_tcpSocket.ignoreSslErrors();
	else
	{
        _lastError = QString("Socket Peer Verfiy error: %1").arg(sslError.errorString());
        disconnectHost();
        emit connError(_lastError);
        setStatus(csFailed);
	}
}

void CompositeConnection::onProxyAuthRequired(const QNetworkProxy &, QAuthenticator* )
{
	_lastError = "proxy requires authentication, but prompt for auth is not yet supported";
    emit connError(_lastError);
    //Note: It is not possible to use a QueuedConnection to connect to this signal, as the connection will fail if the authenticator has not been filled in with new information when the signal returns.
}

void CompositeConnection::onNewServerConnection()
{
    QTcpSocket * socket = _tcpServer.nextPendingConnection();

    //qDebug("CompositeConnection::onNewServerConnection is null: %c", (socket ? 'N' : 'Y'));
    if (socket)
        connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void CompositeConnection::onReadyRead()
{
    QIODevice* io = 0;
    if (!_config.isValid())
    {
        qDebug("CompositeConnection::onReadyRead config not valid");
        return;
    }
    switch (_config.type())
    {
        case CompositeConnectionConfig::ctSerial:
        case CompositeConnectionConfig::ctTcp:
        case CompositeConnectionConfig::ctUdp:
        case CompositeConnectionConfig::ctTcpServer: // will be from client QTcpSocket, not _tcpServer
            io = qobject_cast<QIODevice*>(sender());
            break;
        default:
            qDebug("CompositeConnection::onReadyRead invalid type: %d", (int)_config.type());
            return ;
    } // switch
    if (!io)
    {
        qDebug("CompositeConnection::onReadyRead cast unsuccessful");
        return;
    }
    emit readyRead(io);
}
