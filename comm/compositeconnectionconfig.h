#ifndef COMPOSITECONNECTIONCONFIG_H
#define COMPOSITECONNECTIONCONFIG_H

#include <QObject>
#include <QHostAddress>
#include <QSerialPort>
#include "connectionproxyconfig.h"

class CompositeConnectionConfig : public QObject
{
	Q_OBJECT
public:
	typedef enum {ctNone = 0, ctSerial, ctTcp, ctTcpServer, ctUdp} ConnectionType;

	CompositeConnectionConfig(QObject* parent = 0);
	CompositeConnectionConfig(const CompositeConnectionConfig& cpy);
	CompositeConnectionConfig& operator=(const CompositeConnectionConfig& rhs);
	void copy(const CompositeConnectionConfig& cpy);
	void clear();

	void setType(ConnectionType conType);
	ConnectionType type() const;

	bool isSerial() const;
	bool isTcp(bool includeTcpServer = true) const;
	bool isUdp() const;

	bool usesProxy() const;
	const ConnectionProxyConfig& proxyConfig() const;
	ConnectionProxyConfig& proxyConfig();

	void setSerialPortName(const QString& name);
	const QString& serialPortName() const;
	void setSerialBaud(qint32 baud);
	qint32 serialBaud() const;
	void setSerialFlowControl(QSerialPort::FlowControl flowControl);
	QSerialPort::FlowControl serialFlowControl() const;
	void setSerialStopBits(QSerialPort::StopBits bits);
	QSerialPort::StopBits serialStopBits() const;

	void setSerial(const QString& portName, qint32 baud = 115200, QSerialPort::FlowControl flowControl = QSerialPort::NoFlowControl, QSerialPort::StopBits stopBits = QSerialPort::OneStop);
	void setTcp(const QString& hostName, quint16 port);
	void setTcp(const QHostAddress& address, quint16 port);
	void setTcpServer(const QString& hostName, quint16 port);
	void setTcpServer(const QHostAddress& address, quint16 port);
	void setTcpSsl(const QString& hostName, quint16 port, bool ignoreInvalidSsl = false);
	void setTcpSsl(const QHostAddress& address, quint16 port, bool ignoreInvalidSsl = false);
	void setUdp(quint16 port, const QString& hostName);
	void setUdp(quint16 port, const QHostAddress& address = QHostAddress(QHostAddress::Any));
	void setUdpWriteOnly(quint16 port);

	void setServer(bool bServer);
	bool isServer() const;
	void setHostAddress(const QHostAddress& addr);
	const QHostAddress& hostAddress() const;
	void setHostName(const QString& name);
	QString hostName() const;
	void setHostPort(quint16 port);
	quint16 hostPort() const;
	void setUseSsl(bool enabled = true);
	bool useSsl() const;
	void setIgnoreInvalidSsl(bool enabled = true);
	bool ignoreInvalidSsl() const;

	bool isValid() const;

protected:
	ConnectionType _type;
	ConnectionProxyConfig _proxyConfig;

	bool _server;
	QHostAddress _hostAddress;
	QString _hostName;
	quint16 _hostPort;
	bool _useSsl;
	bool _ignoreInvalidSsl;

	QString _serialPortName;
	qint32 _serialBaud;
	QSerialPort::FlowControl _serialFlowControl;
	QSerialPort::StopBits _serialStopBits;

signals:
	void changed();

public slots:

};

#endif // COMPOSITECONNECTIONCONFIG_H
