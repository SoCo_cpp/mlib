#ifndef CONNECTIONPROXYCONFIG_H
#define CONNECTIONPROXYCONFIG_H

#include <QObject>
#include <QNetworkProxy>

class ConnectionProxyConfig : public QObject
{
	Q_OBJECT
public:
	ConnectionProxyConfig(QObject* parent = 0);
	ConnectionProxyConfig(const ConnectionProxyConfig& cpy);
	ConnectionProxyConfig& operator=(const ConnectionProxyConfig& rhs);
	void copy(const ConnectionProxyConfig& cpy);
	void clear();

	bool isEnabled(bool requireTypeEnabled = true) const;
	void setEnabled(bool enabled = true);

	QNetworkProxy::ProxyType type() const;
	void setType(QNetworkProxy::ProxyType proxytype);

	const QString& hostName() const;
	void setHostName(const QString& host);

	quint16 hostPort() const;
	void setHostPort(quint16 port);

	const QString& user() const;
	const QString& password() const;
	void setPromptCredentials(bool enabled = true);
	void setCredentials(const QString& user, const QString& password = QString());
	void setUser(const QString& sUser);
	void setPassword(const QString& sPassword);
	void clearCredentials(bool skipChangedSignal = false);
	bool promptCredentials() const;
	bool hasCredentials(bool requirePassword = false) const;

	bool isValid(bool requireEnabled = false) const;
	const QNetworkProxy& proxy();

protected:
	bool _enabled;
	QNetworkProxy::ProxyType _type;
	QString _hostName;
	quint16 _hostPort;
	bool _promptCredentials;
	QString _user;
	QString _password;
	QNetworkProxy _proxy;

signals:
	void changed();

public slots:

};

#endif // CONNECTIONPROXYCONFIG_H
