#ifndef COMPOSITECONNECTION_H
#define COMPOSITECONNECTION_H

#include <QObject>
#include <QSerialPort>
#include <QSslSocket>
#include <QTcpServer>
#include <QUdpSocket>
#include "compositeconnectionconfig.h"

class CompositeConnection : public QObject
{
	Q_OBJECT
public:
	typedef enum {csNone = 0, csDisconnected, csConnecting, csConnected, csTimedOut, csFailed} ConnectionStatus;
	typedef enum {cwNone = 0, cwDisconnect, cwConnect} ConnectionWanted; // for persistent connections

	CompositeConnection(QObject* parent = 0);

	QString lastError() const;

	ConnectionStatus status() const;
	QString statusString() const;

	const CompositeConnectionConfig& config() const;
	CompositeConnectionConfig& config();

	bool persistent() const;
	void setPersistent(bool enabled = true);

    void wantConnect();
    void wantDisconnect();

    bool connectHost(bool failIfConnecting = true, bool wantConnect = true); // standard connect
    bool disconnectHost(bool failIfDisconnected = true, bool wantDisconnect = true); // standard disconnect
    bool reconnectHost(bool failIfConnected = true); // standard reconnect

	bool isConnected() const;
	bool isDisconnected(bool persistentRequiresFullDisconnect = true);
	bool isConnectFailed();

	bool canIODevice() const;
	QIODevice* asIODevice(); // not availible for ctTcpServer

	QSerialPort& serialPort();
	const QSerialPort& serialPort() const;

	QSslSocket& tcpSocket();
	const QSslSocket& tcpSocket() const;

	QTcpServer& tcpServer();
	const QTcpServer& tcpServer() const;

	QUdpSocket& udpSocket();
	const QUdpSocket& udpSocket() const;

	bool canRead();
	qint64 readSize();
	bool canReadLine();

	QByteArray readAll();
	QByteArray readLine(qint64 maxSize = 0); // empty on error
	qint64 read(char* data, qint64 maxSize); // -1 error
	bool getChar(char* pc);

	bool write(const QString& string); // -1 error
	qint64 write(const char* data, qint64 maxSize); // -1 error
	qint64 write(const QByteArray& byteArray); // -1 error
	bool putChar(char c);

protected:
	ConnectionStatus _status;
	ConnectionWanted _wanted;
	bool _persistent;
	CompositeConnectionConfig _config;
	QSerialPort _serialPort;
	QSslSocket _tcpSocket;
	QTcpServer _tcpServer;
	QUdpSocket _udpSocket;
	QString _lastError;

	void setStatus(ConnectionStatus status);

signals:
	void configChanged();
	void connected();
	void disconnected();
	void statusChanged();
    void readyRead(QIODevice* io);
	void newServerConnection();

    void doConnectHost();
    void doDisconnectHost();
    void doReconnectHost();
    void connError(QString errorMessage);

public slots:

    void onConnectHost();
    void onDisconnectHost();
    void onReconnectHost();

    void onPersistenCheck();
	void onSerialError(QSerialPort::SerialPortError error);

    void onReadyRead();
	void onSocketConnected();
	void onSocketEncrypted();
	void onSocketDisconnected();
	void onSocketError(QAbstractSocket::SocketError socketError); // _socket.errorString()
	void onSocketPeerVerifyError(const QSslError& sslError); // _socket.ignoreSslErrors(); or sslError.errorString()
	void onProxyAuthRequired(const QNetworkProxy & proxy, QAuthenticator * authenticator);
    void onNewServerConnection();
};

#endif // COMPOSITECONNECTION_H
