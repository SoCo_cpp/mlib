
!defined(MLIB_SRC,var):error("MLIB comm (mlib/comm/comm.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/comm

message("MLIB including comm...")

QT *= network serialport

SOURCES +=  $$MSRC/compositeconnection.cpp \
            $$MSRC/compositeconnectionconfig.cpp \
            $$MSRC/connectionproxyconfig.cpp


HEADERS +=  $$MSRC/compositeconnection.h \
            $$MSRC/compositeconnectionconfig.h \
            $$MSRC/connectionproxyconfig.h
