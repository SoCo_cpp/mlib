#include "compositeconnectionconfig.h"

CompositeConnectionConfig::CompositeConnectionConfig(QObject* parent /*= 0*/) :
	QObject(parent),
	_type(ctNone),
	_proxyConfig(),
	_server(false),
	_hostAddress(),
	_hostName(""),
	_hostPort(0),
	_useSsl(false),
	_ignoreInvalidSsl(false),
	_serialPortName(""),
	_serialBaud(0),
	_serialFlowControl(QSerialPort::NoFlowControl),
	_serialStopBits(QSerialPort::OneStop)
{
	connect(&_proxyConfig, SIGNAL(changed()), this, SIGNAL(changed()));
}

CompositeConnectionConfig::CompositeConnectionConfig(const CompositeConnectionConfig& cpy) :
	QObject(cpy.parent())
{
	connect(&_proxyConfig, SIGNAL(changed()), this, SIGNAL(changed()));
	copy(cpy);
}

CompositeConnectionConfig& CompositeConnectionConfig::operator=(const CompositeConnectionConfig& rhs)
{
	copy(rhs);
	return *this;
}

void CompositeConnectionConfig::copy(const CompositeConnectionConfig& cpy)
{
	_type			= cpy._type;
	_proxyConfig		= cpy._proxyConfig;
	_server			= cpy._server;
	_hostAddress		= cpy._hostAddress;
	_hostName		= cpy._hostName;
	_hostPort		= cpy._hostPort;
	_useSsl			= cpy._useSsl;
	_ignoreInvalidSsl	= cpy._ignoreInvalidSsl;
	_serialPortName		= cpy._serialPortName;
	_serialBaud		= cpy._serialBaud;
	_serialFlowControl	= cpy._serialFlowControl;
	_serialStopBits		= cpy._serialStopBits;
	emit changed();
}

void CompositeConnectionConfig::clear()
{

	_type				= ctNone;
	_proxyConfig.clear();
	_hostAddress.clear();
	_hostName			= "";
	_server				= false;
	_hostPort			= 0;
	_useSsl				= false;
	_ignoreInvalidSsl	= false;
	_serialPortName		= "";
	_serialBaud			= 0;
	_serialFlowControl	= QSerialPort::NoFlowControl;
	_serialStopBits		= QSerialPort::OneStop;

	emit changed();
}

void CompositeConnectionConfig::setType(ConnectionType conType)
{
	_type = conType;
	emit changed();
}

CompositeConnectionConfig::ConnectionType CompositeConnectionConfig::type() const
{
	return _type;
}

bool CompositeConnectionConfig::isSerial() const
{
	return _type == ctSerial;
}

bool CompositeConnectionConfig::isTcp(bool includeTcpServer /*= true*/) const
{
	return (_type == ctTcp || (includeTcpServer && _type == ctTcpServer));
}

bool CompositeConnectionConfig::isUdp() const
{
	return _type == ctUdp;
}

bool CompositeConnectionConfig::usesProxy() const
{
	return _proxyConfig.isEnabled();
}

const ConnectionProxyConfig& CompositeConnectionConfig::proxyConfig() const
{
	return _proxyConfig;
}

ConnectionProxyConfig& CompositeConnectionConfig::proxyConfig()
{
	return _proxyConfig;
}

void CompositeConnectionConfig::setSerialPortName(const QString& name)
{
	_serialPortName = name;
	emit changed();
}

const QString& CompositeConnectionConfig::serialPortName() const
{
	return _serialPortName;
}

void CompositeConnectionConfig::setSerialBaud(qint32 baud)
{
	_serialBaud = baud;
	emit changed();
}

qint32 CompositeConnectionConfig::serialBaud() const
{
	return _serialBaud;
}

void CompositeConnectionConfig::setSerialFlowControl(QSerialPort::FlowControl flowControl)
{
	_serialFlowControl = flowControl;
	emit changed();
}

QSerialPort::FlowControl CompositeConnectionConfig::serialFlowControl() const
{
	return _serialFlowControl;
}

void CompositeConnectionConfig::setSerialStopBits(QSerialPort::StopBits bits)
{
	_serialStopBits = bits;
	emit changed();
}

QSerialPort::StopBits CompositeConnectionConfig::serialStopBits() const
{
  return _serialStopBits;
}

void CompositeConnectionConfig::setSerial(const QString& portName, qint32 baud /*= 115200*/, QSerialPort::FlowControl flowControl /*= QSerialPort::NoFlowControl*/, QSerialPort::StopBits stopBits /*= QSerialPort::OneStop*/)
{
	_type				= ctSerial;
	_serialPortName		= portName;
	_serialBaud			= baud;
	_serialFlowControl	= flowControl;
	_serialStopBits		= stopBits;

	emit changed();
}

void CompositeConnectionConfig::setTcp(const QString& hostName, quint16 port)
{
  setTcp(QHostAddress(hostName), port);
}

void CompositeConnectionConfig::setTcp(const QHostAddress& address, quint16 port)
{
  _type			= ctTcp;
  _server		= false;
  _useSsl		= false;
  _ignoreInvalidSsl	= false;
  _hostAddress		= address;
  _hostName             = address.toString();
  _hostPort		= port;

  emit changed();
}

void CompositeConnectionConfig::setTcpServer(const QString& hostName, quint16 port)
{
	_type			= ctTcpServer;
	_server			= true;
	_useSsl			= false;
	_ignoreInvalidSsl	= false;
	_hostAddress		= QHostAddress(hostName);
	_hostName		= hostName;
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setTcpServer(const QHostAddress& address, quint16 port)
{
	_type			= ctTcpServer;
	_server			= true;
	_useSsl			= false;
	_ignoreInvalidSsl	= false;
	_hostAddress		= address;
	_hostName		= address.toString();
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setTcpSsl(const QString& hostName, quint16 port, bool ignoreInvalidSsl /*= false*/)
{
	_type			= ctTcp;
	_server			= false;
	_useSsl			= true;
	_ignoreInvalidSsl	= ignoreInvalidSsl;
	_hostAddress		= QHostAddress(hostName);
	_hostName		= hostName;
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setTcpSsl(const QHostAddress& address, quint16 port, bool ignoreInvalidSsl /*= false*/)
{
	_type			= ctTcp;
	_server			= false;
	_useSsl			= true;
	_ignoreInvalidSsl	= ignoreInvalidSsl;
	_hostAddress		= address;
	_hostName		= address.toString();
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setUdp(quint16 port, const QString& hostName)
{
	_type			= ctUdp;
	_server			= true;
	_useSsl			= false;
	_ignoreInvalidSsl	= false;
	_hostAddress		= QHostAddress(hostName);
	_hostName		= hostName;
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setUdp(quint16 port, const QHostAddress& address /*= QHostAddress(QHostAddress::Any)*/)
{
	_type			= ctUdp;
	_server			= true;
	_useSsl			= false;
	_ignoreInvalidSsl	= false;
	_hostAddress		= address;
	_hostName		= address.toString();
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setUdpWriteOnly(quint16 port)
{
	_type			= ctUdp;
	_server			= false;
	_useSsl			= false;
	_ignoreInvalidSsl	= false;
	_hostAddress		= "";
	_hostName		= "";
	_hostPort		= port;

	emit changed();
}

void CompositeConnectionConfig::setServer(bool bServer)
{
	_server = bServer;
	emit changed();
}

bool CompositeConnectionConfig::isServer() const
{
	return _server;
}

void CompositeConnectionConfig::setHostAddress(const QHostAddress& addr)
{
	_hostAddress = addr;
	_hostName = addr.toString();
	emit changed();
}

const QHostAddress& CompositeConnectionConfig::hostAddress() const
{
	return _hostAddress;
}

void CompositeConnectionConfig::setHostName(const QString& name)
{
	_hostAddress = QHostAddress(name);
	_hostName = name;
	emit changed();
}

QString CompositeConnectionConfig::hostName() const
{
	return _hostName;
}

void CompositeConnectionConfig::setHostPort(quint16 port)
{
	_hostPort = port;
	emit changed();
}

quint16 CompositeConnectionConfig::hostPort() const
{
	return _hostPort;
}

void CompositeConnectionConfig::setUseSsl(bool enabled /*= true*/)
{
	_useSsl = enabled;
	emit changed();
}

bool CompositeConnectionConfig::useSsl() const
{
	return _useSsl;
}

void CompositeConnectionConfig::setIgnoreInvalidSsl(bool enabled /*= true*/)
{
	_ignoreInvalidSsl = enabled;
	emit changed();
}

bool CompositeConnectionConfig::ignoreInvalidSsl() const
{
	return _ignoreInvalidSsl;
}

bool CompositeConnectionConfig::isValid() const
{
	if (!_proxyConfig.isValid())
		return false;
	switch (_type)
	{
		case ctNone:
			return false; // no connection type set
		case ctSerial:
			if (_serialPortName.isEmpty())
				return false;
			if (_serialBaud < 1200)
				return false;
			break;
		case ctTcp:
		case ctTcpServer:
		case ctUdp:
			if (_server && _hostAddress.isNull())
				return false;
			if (!_server && (_hostPort == 0 || _hostName.isEmpty()))
				return false; // invalid port. Port zero is used as ANY for some server bindings.
			break;
		default:
			return false; // invaid connection type

	} // switch
	return true;
}
