#ifndef IRCHANDLER_H
#define IRCHANDLER_H

#include <QObject>
#include <QTimer>
#include <QDateTime>
#include <QThread>
#include <QMutex>
#include "comm/irc/ircpacket.h"

class IrcClient; // predeclare
class IrcHandler : public QObject
{
	Q_OBJECT
public:
    const static int cHandleInputTimerInterval = 10;	// msec
	const static qint64 cMaxReadLineCharacters = 512;	// IRC protocol specification

	explicit IrcHandler(QObject*, IrcClient& ircClient);
	~IrcHandler();

	void setRawLogEnabled(bool enabled = true);
	bool rawLogEnabled() const;

    QDateTime lastReceived() const;
    void initLastReceived();
    IrcPacket& packet();
	void sendLine(const QString& str);

protected:
	IrcClient& _ircClient;
	bool _rawLogEnabled; // enabled emitting receivedRawInput (costly deep copy)
	QTimer _handleInputTimer;
	IrcPacket _ircPacket;
	QThread _thread;
    QDateTime _lastReceived;
    QMutex _mxHandleInput;

	void handlePacket();
	void handleNumericCommandPacket();
	void handleTextCommandPacket();

signals:
	// Internal/Handler-to-Client Signals
	void setLoginStatus(int);
	void send(QString str);
	void sendNickUserLogin();
	void sendAuthenticateSASL();
	void sendJoin(QString sChannel);
	void sendPart(QString sChannel, QString sMessage);
	void sendPrivateMessage(QString sNickChannel, QString sMessage);
	void sendWhois(QString sNick);
    void sendPing(QString sMessage);
	void sendQuit(QString sMessage);
	// External Signals
	void receivedRawInput(QString sRawInput);
	void receivedPong(QString nick, QString user, QString target, QString message);
	void receivedNotice(QString nick, QString user, QString target, QString message);
	void receivedPrivmsg(QString nick, QString user, QString target, QString message);
	void receivedJoin(QString nick, QString user, QString channel);
	void receivedPart(QString nick, QString user, QString channel, QString message);
	void receivedQuit(QString nick, QString user, QString message);
	void receivedNick(QString nick, QString user,QString newNick);
	void receivedMode(QString nick, QString user, QString target, QString params);
	void receivedTopic(QString nick, QString user, QString target, QString message);
	void receivedKick(QString nick, QString user, QString channel, QString target, QString message);

	void receivedWhoisUser(QString nick, QString user, QString host, QString realName);
	void receivedWhoisServer(QString nick, QString server, QString serverInfo);
	void receivedWhoisOperator(QString nick, QString message);
	void receivedWhoisIdle(QString nick, QString idle, QString idleMessage);
	void receivedWhoisChannels(QString nick, QString message);
	void receivedWhoisAuth(QString nick, QString authNick, QString message);
	void receivedWhoisEnd(QString nick, QString message);

	void receivedNickInUse(QString nick, QString message);
	void receivedAuthFailed(QString nick, QString message);

public slots:
	void handleInputTimerTick();

};

#endif // IRCHANDLER_H
