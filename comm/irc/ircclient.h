#ifndef IRCCLIENT_H
#define IRCCLIENT_H

#include <QObject>
#include <QQueue>
#include <QTimer>
#include <QThread>
#include <QMutex>
#include <QDateTime>
#include "comm/compositeconnection.h"
#include "comm/irc/ircclientconfig.h"
#include "comm/irc/irchandler.h"

class IrcClient : public QObject
{
	Q_OBJECT
public:
	const static qint64 cMaxReadLineCharacters = 512;	// IRC protocol specification
    const static int cOutputTimerInterval           = 10;	// msec
	const static int cLoginTimerInterval			= 200;	// msec
    const static int cWatchdogTimerInterval         = 30000; // msec 30 sec
    const static int cWatchdogReceiveTimeoutSecs    = 180; // sec 3 min
    const static int cWatchdogReadyTimeoutSecs      = 420; // sec 7 min
    const static int cPingInterval                  = 40000;  // msec 40 sec
    const static int cPingAverageCountMax           = 10;

	typedef enum {cNone = 0, cDisconnected, cDisconnecting, cConnecting, cConnected, cLogin, cReady, cError} ClientStatus;
	typedef enum {lNone = 0,
				  lWaitFirstReply, lFirstReplyDone,
				  lPreAuth, lPreAuthWait, lPreAuthDone,
				  lMotdWait, lMotdDone,
				  lNickservAuth, lNickservAuthWait, lNickservAuthDone,
				  lAutoJoin,
				  lReady} LoginStatus;

	explicit IrcClient(QObject *parent = 0);
	~IrcClient();

	IrcHandler& handler();
	const IrcHandler& handler() const;

	void setClientStatus(ClientStatus cStatus);
	ClientStatus clientStatus() const;
	QString clientStatusString() const;

	//setLoginStatus is a slot
	LoginStatus loginStatus() const;
	QString loginStatusString() const;

	bool isReady() const;
	bool isConnected() const;
	bool isDisconnected() const;
	bool isPreAuthenticated() const;
	bool isLoggedIn() const;

    CompositeConnection& connection();

    bool enableWatchdog(int intervalSecs = 30);
    void disableWatchdog();
    QDateTime lastReady() const;

	void setConfig(const IrcClientConfig& cfg);
	IrcClientConfig& config();
	const IrcClientConfig& config() const;

    qint64 lastLag() const; // ms
    double averageLag() const; // ms

	bool connectClient(bool forceReconnect = false, bool failOnConnected = false);
	bool disconnectClient(const QString& sQuitMessage = "", bool force = false);

	void setRawLogEnabled(bool enabled = true);
	bool rawLogEnabled() const;

	void sendRaw(const QString& str);
	void sendLine(const QString& str);

	bool hasInputQueue(); // does its own lock
	QQueue<QString>& lockInputQueue();
	void unlockInputQueue();

protected:
	CompositeConnection _connection;
	IrcHandler _handler;
	ClientStatus _clientStatus;
	LoginStatus _loginStatus;
	bool _rawLogEnabled; // enabled emitting sendRawOutput (costly deep copy)
	IrcClientConfig _config;
    qint64 _lastLag; // ms
    double _averageLag; // ms
    unsigned int _averageLagCount;

    QThread _thread;

    QTimer* _outputTimer;
    QTimer* _loginTimer;
    QTimer* _pingTimer;
    QTimer* _watchdogTimer;
    QDateTime _lastReady;

	QQueue<QString> _outputQueue;
	QQueue<QString> _inputQueue;
    QMutex _outputQueueLock;
    QMutex _inputQueueLock;

signals:
	void clientStatusChanged();
	void loginStatusChanged();
	void connected();
	void disconnected();
	void sendRawOutput(QString sRawOutput);
    void watchdogError(QString sErrorMsg);
    void lagAverage(double msLagAverage);

public slots:
    void threadStarted();
	void configChanged();

	void outputTimerTick();
	void loginTimerTick();
    void pingTimerTick();
	void connectionStatusChanged();
    void connectionReadyRead(QIODevice* io);

    void watchdogTimerTick();
    void receivedPong(QString nick, QString user, QString target, QString message);

	void setLoginStatus(int iStatus);

	void send(QString str);
	void sendNickChange(QString newNick);
	void sendNickUserLogin();
	void sendAuthenticateSASL();
	void sendAutoJoins();
	void sendJoin(QString sChannel);
	void sendPart(QString sChannel, QString sMessage);
	void sendPrivateMessage(QString sNickChannel, QString sMessage);
	void sendWhois(QString sNick);
    void sendPing(QString sMessage);
    void sendQuit(QString sMessage);
	void sendNickGHost();
	void sendNickRelase();

};

#endif // IRCCLIENT_H
