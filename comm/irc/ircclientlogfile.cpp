#include "comm/irc/ircclientlogfile.h"
#include "logging/timestampedlog.h"
#include "logging/debuglogger.h"

IrcClientLogFile::IrcClientLogFile(QObject* parent /*= 0*/, const QString& sFileSuffix /*= ".txt"*/, const QString& sPathDelimiter /*= "/"*/) :
	QObject(parent),
	_logFile(new TimestampedLog(this)),
	_lastTime(),
	_baseLogPath(""),
	_archiveSubdir(""),
	_fileSuffix(sFileSuffix),
	_pathDelimiter(sPathDelimiter)
{
	_lastTime.restart();
}

void IrcClientLogFile::setBaseLogPath(const QString& basePath)
{
	_baseLogPath = basePath;
}

const QString& IrcClientLogFile::baseLogPath() const
{
	return _baseLogPath;
}

bool IrcClientLogFile::setLogName(const QString& logName)
{
	_logFile->setPathDelimiter(_pathDelimiter);
	_logFile->setLogName(logName);
	_logFile->setFileName(_baseLogPath + _pathDelimiter + _logFile->logName() + _fileSuffix);
	if (_logFile->archiveEnabled())
		_logFile->setArchiveDir(_archiveSubdir, true/*appendToFilePath*/);
	if (!_logFile->makePath())
	{
		DebugLog(dlWarn, "IrcClientLogFile::setLogName log file failed to make path. logName: %s, log path: %s, archive path: %s", qPrintable(_logFile->logName()), qPrintable(_logFile->filePath()), qPrintable(_logFile->archiveDir()));
		_logFile->setLogName("");
		return false;
	}
	touch();
	return true;
}

const QString& IrcClientLogFile::logName() const
{
	return _logFile->logName();
}

void IrcClientLogFile::setArchiving(qint64 size, const QString& fileNameFormat, const QString& archiveSubdir)
{
	_archiveSubdir = archiveSubdir;
	_logFile->setArchiving(size, fileNameFormat, archiveSubdir, true/*appendToFilePath*/);
}

TimestampedLog* IrcClientLogFile::logFile()
{
	return _logFile;
}

const TimestampedLog* IrcClientLogFile::logFile() const
{
	return _logFile;
}

void IrcClientLogFile::log(const char* strFmt, ...)
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	_logFile->log(sMsg);
	touch();
}

void IrcClientLogFile::log(const char* strFmt, va_list arglist)
{
	_logFile->log(QString().vsprintf(strFmt, arglist));
	touch();
}

void IrcClientLogFile::log(const QString& sMsg)
{
	_logFile->log(sMsg);
	touch();
}

int IrcClientLogFile::age() const
{
	return _lastTime.elapsed();
}

void IrcClientLogFile::touch()
{
	_lastTime.restart();
}

