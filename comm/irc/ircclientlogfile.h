#ifndef IRCCLIENTLOGFILE_H
#define IRCCLIENTLOGFILE_H

#include <QObject>
#include <QTime>

class TimestampedLog; // predeclared

class IrcClientLogFile : public QObject
{
	Q_OBJECT
public:
	explicit IrcClientLogFile(QObject* parent = 0, const QString& sFileSuffix = ".txt", const QString& sPathDelimiter = "/");

	void setBaseLogPath(const QString& basePath);
	const QString& baseLogPath() const;

	bool setLogName(const QString& logName); // saved in _log->logName()
	const QString& logName() const;

	void setArchiving(qint64 size, const QString& fileNameFormat, const QString& archiveSubdir);

	TimestampedLog* logFile();
	const TimestampedLog* logFile() const;

	void log(const char* strFmt, ...);
	void log(const char* strFmt, va_list arglist);
	void log(const QString& sMsg);

	int age() const;

	void touch();

protected:
	TimestampedLog* _logFile;
	QTime _lastTime;
	QString _baseLogPath;
	QString _archiveSubdir;
	QString _fileSuffix;
	QString _pathDelimiter;

signals:

public slots:

};

#endif // IRCCLIENTLOGFILE_H
