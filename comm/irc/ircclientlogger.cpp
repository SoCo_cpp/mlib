#include "comm/irc/ircclientlogger.h"
#include "comm/irc/irchandler.h"
#include "logging/debuglogger.h"
#include "comm/irc/ircclientlogfile.h"
#include <QDir>

IrcClientLogger::IrcClientLogger(IrcClient& ircclient, QObject* parent /*= 0*/) :
	QObject(parent),
	_irc(ircclient),
	_enabled(false),
	_logOptions(0),
	_baseLogPath(QDir::homePath()),
	_archiveSize(0),
	_archiveSubdir("archive"),
	_archiveFormat("_MM-dd-yy_hh-mm-ss")
{
	_logFilesCleanupTimer.setInterval(C_LogFile_Sweep_Interval_Ms); // don't start until we have one logFile
	connect(this, SIGNAL(logOptionsChanged()), this, SLOT(updateLogOptionBindings()));
	connect(&_logFilesCleanupTimer, SIGNAL(timeout()), this, SLOT(cleanupLogFilesTimerTick()));
	_logFilesCleanupTimer.start();
}

void IrcClientLogger::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
	updateLogOptionBindings();
}

bool IrcClientLogger::isEnabled() const
{
	return _enabled;
}

void IrcClientLogger::setBaseLogPath(const QString& sBaseLogPath)
{
	_baseLogPath = sBaseLogPath;
}

const QString& IrcClientLogger::baseLogPath() const
{
	return _baseLogPath;
}
void IrcClientLogger::setArchiving(qint64 size, const QString& archiveSubdir /*= QString("archive")*/, const QString& fileNameFormat /*= QString("_MM-dd-yy_hh-mm-ss")*/)
{
	_archiveSize = size;
	_archiveFormat = fileNameFormat;
	_archiveSubdir = archiveSubdir;
}

void IrcClientLogger::clearLogOptions()
{
	_logOptions = 0L;
	emit logOptionsChanged();
}

void IrcClientLogger::setAllLogOptions()
{
	_logOptions = ~Q_INT64_C(0);
	emit logOptionsChanged();
}

quint64 IrcClientLogger::rawLogOptionMask(LogOptions opt)
{
	return (Q_INT64_C(1) << ((quint64)opt));
}

void IrcClientLogger::setRawLogOptions(quint64 rawOpt)
{
	_logOptions = rawOpt;
	emit logOptionsChanged();
}

quint64 IrcClientLogger::rawLogOptions() const
{
	return _logOptions;
}

void IrcClientLogger::setLogOption(LogOptions opt)
{
	_logOptions |= rawLogOptionMask(opt);
	emit logOptionsChanged();
}

void IrcClientLogger::clearLogOption(LogOptions opt)
{
	_logOptions &= ~(rawLogOptionMask(opt));
	emit logOptionsChanged();
}

bool IrcClientLogger::hasLogOption(LogOptions opt)
{
	return ((_logOptions & rawLogOptionMask(opt)) != 0L);
}

IrcClientLogFile* IrcClientLogger::getLogFile(const QString& logName)
{
	_logFilesLocker.lockMany();
	for (int i = 0;i < _logFiles.size();i++)
		if (_logFiles[i]->logName() == logName)
			return _logFiles[i]; // leave many locked
	// not found, look for expired
	for (int i = 0;i < _logFiles.size();i++)
		if (_logFiles[i]->age() > C_LogFile_Expired_Time_Ms)
		{
			if (!_logFiles[i]->setLogName(logName))
			{
				DebugLog(dlWarn, "IrcClientLogger::getLogFile reused logFile [%d] setLogName failed: %s", i, qPrintable(logName));
				_logFilesLocker.unlockMany();
				return 0;
			}
			return _logFiles[i]; // leave many locked
		}
	_logFilesLocker.unlockMany();
	IrcClientLogFile* newLogFile = new IrcClientLogFile(this);
	newLogFile->setBaseLogPath(_baseLogPath);
	if (_archiveSize)
		newLogFile->setArchiving(_archiveSize, _archiveFormat, _archiveSubdir);
	if (!newLogFile->setLogName(logName))
	{
		DebugLog(dlWarn, "IrcClientLogger::getLogFile newLogFile setLogName failed: %s", qPrintable(logName));
		return 0;
	}
	_logFilesLocker.lockOne();
	_logFiles.append(newLogFile);
	_logFilesLocker.unlockOneAndLockMany();
	return newLogFile; // leave many locked
}

bool IrcClientLogger::log(const QString& logName, const char* strFmt, ...)
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	return log(logName, sMsg);
}

bool IrcClientLogger::log(const QString& logName, const QString& sMsg)
{
	IrcClientLogFile* logFile = getLogFile(logName);
	if (!logFile) // getLogFile leaves many locked, unless failure
	{
		DebugLog(dlWarn, "IrcClientLogger::log getLogFile failed. logName: %s, Message: %s", qPrintable(logName), qPrintable(sMsg));
		return false;
	}
	logFile->log(sMsg);
	_logFilesLocker.unlockMany();
	return true;
}

void IrcClientLogger::updateLogOptionBindings()
{
	disconnect(&_irc, 0, this, 0);
	disconnect(&_irc.handler(), 0, this, 0);
	if (!_enabled)
		return; // done
	if (hasLogOption(loConnectDisconnect))
	{
		connect(&_irc, SIGNAL(connected()), this, SLOT(clientConnected()));
		connect(&_irc, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
	}
	if (hasLogOption(loPrivMsg) || hasLogOption(loChannelMsg))
	{
		connect(&_irc.handler(), SIGNAL(receivedNotice(QString,QString,QString,QString)), this, SLOT(receivedNotice(QString,QString,QString,QString)));
		connect(&_irc.handler(), SIGNAL(receivedPrivmsg(QString,QString,QString,QString)), this, SLOT(receivedPrivmsg(QString,QString,QString,QString)));
	}
	if (hasLogOption(loChannelJoinPart) || hasLogOption(loSelfJoinPart))
	{
		connect(&_irc.handler(), SIGNAL(receivedJoin(QString,QString,QString)), this, SLOT(receivedJoin(QString,QString,QString)));
		connect(&_irc.handler(), SIGNAL(receivedPart(QString,QString,QString,QString)), this, SLOT(receivedPart(QString,QString,QString,QString)));
	}
	if (hasLogOption(loSelfQuit))
		connect(&_irc.handler(), SIGNAL(sendQuit(QString)), this, SLOT(sendQuit(QString)));
	if (hasLogOption(loSelfPrivMsg) || hasLogOption(loSelfChanMsg))
		connect(&_irc.handler(), SIGNAL(sendPrivateMessage(QString,QString)), this, SLOT(sendPrivateMessage(QString,QString)));
	if (hasLogOption(loChannelQuit) || hasLogOption(loSelfQuit))
		connect(&_irc.handler(), SIGNAL(receivedQuit(QString,QString,QString)), this, SLOT(receivedQuit(QString,QString,QString)));
	if (hasLogOption(loChannelNick) || hasLogOption(loSelfNick))
		connect(&_irc.handler(), SIGNAL(receivedNick(QString,QString,QString)), this, SLOT(receivedNick(QString,QString,QString)));
	if (hasLogOption(loChannelMode) || hasLogOption(loSlefMode))
		connect(&_irc.handler(), SIGNAL(receivedMode(QString,QString,QString,QString)), this, SLOT(receivedMode(QString,QString,QString,QString)));
	if (hasLogOption(loChannelKick) || hasLogOption(loSelfKick))
		connect(&_irc.handler(), SIGNAL(receivedKick(QString,QString,QString,QString,QString)), this, SLOT(receivedKick(QString,QString,QString,QString,QString)));
	if (hasLogOption(loChannelTopic))
		connect(&_irc.handler(), SIGNAL(receivedTopic(QString,QString,QString,QString)), this, SLOT(receivedTopic(QString,QString,QString,QString)));
}

void IrcClientLogger::cleanupLogFilesTimerTick()
{
	_logFilesLocker.lockOne();
	for (int i = 0;i < _logFiles.size();i++)
		if (_logFiles[i]->age() > C_LogFile_Release_Time_Ms)
		{
            //DebugLog(dlDebug, "IrcClientLogger::cleanupLogFilesTimerTick scrubbing [%d] %s, age: %d", i, qPrintable(_logFiles[i]->logName()), _logFiles[i]->age());
			_logFiles.takeAt(i)->deleteLater();
			i--;
		}
	_logFilesLocker.unlockOne();
}

void IrcClientLogger::clientConnected()
{
	if (!log(_irc.config().accountConfig().nick(), "<connected>"))
		DebugLog(dlWarn, "IrcClientLogger::clientConnected log failed");
}

void IrcClientLogger::clientDisconnected()
{
	if (!log(_irc.config().accountConfig().nick(), "<disconnected>"))
		DebugLog(dlWarn, "IrcClientLogger::clientDisconnected log failed");
}

void IrcClientLogger::sendPrivateMessage(QString sNickChannel, QString sMessage)
{
	if (!log(sNickChannel, "<" + _irc.config().accountConfig().nick() + "> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::sendPrivateMessage log failed");
}

void IrcClientLogger::sendQuit(QString sMessage)
{
	if (!log("main", "Quit <" + _irc.config().accountConfig().nick() + "> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::sendQuit logAll failed");
}

void IrcClientLogger::receivedNotice(QString nick, QString user, QString target, QString sMessage)
{
	Q_UNUSED(user);
	if (target.isEmpty() || target != _irc.config().accountConfig().nick())
	{
		if (target.isEmpty() || target == "*" || target == "***")
			target = "main";
		if (!nick.isEmpty())
			sMessage = "(NOTICE)<" + nick + ">" + sMessage;
		else
			sMessage = "(NOTICE) " + sMessage;
		if (!log(target , sMessage))
			DebugLog(dlWarn, "IrcClientLogger::receivedNotice log failed");
	}
	else
	{
		if (nick.isEmpty())
			nick = target;
		if (!log(nick , "(NOTICE) " + sMessage))
			DebugLog(dlWarn, "IrcClientLogger::receivedNotice log failed");
	}
}

void IrcClientLogger::receivedPrivmsg(QString nick, QString user, QString target, QString sMessage)
{
	Q_UNUSED(user);
	if (target != _irc.config().accountConfig().nick())
	{
		if (!log(target , "<" + nick + "> " + sMessage))
			DebugLog(dlWarn, "IrcClientLogger::receivedPrivmsg log failed");
	}
	else
	{
		if (!log(nick , sMessage))
			DebugLog(dlWarn, "IrcClientLogger::receivedPrivmsg log failed");
	}
}

void IrcClientLogger::receivedJoin(QString nick, QString user, QString channel)
{
	if (!log(channel , "Joined <" + nick + " (" + user + ")>"))
		DebugLog(dlWarn, "IrcClientLogger::receivedJoin log failed");
}

void IrcClientLogger::receivedPart(QString nick, QString user, QString channel, QString sMessage)
{
	if (!log(channel , "Parted <" + nick + " (" + user + ")> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::receivedPart log failed");
}

void IrcClientLogger::receivedQuit(QString nick, QString user, QString sMessage)
{
	if (!log("main", "Quit <" + nick + " (" + user + ")> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::receivedQuit logAll failed");
}

void IrcClientLogger::receivedNick(QString nick, QString user, QString newNick)
{
	if (!log("main", "Nick changed <" + nick + " (" + user + ")> -> <" + newNick + ">"))
		DebugLog(dlWarn, "IrcClientLogger::receivedNick log failed");
}

void IrcClientLogger::receivedMode(QString nick, QString user, QString target, QString params)
{
	// same even if self target
	if (nick.isEmpty())
		nick = "*";
	if (!log(target , "Mode <" + nick + " (" + user + ")> " + params))
		DebugLog(dlWarn, "IrcClientLogger::receivedMode log failed");
}

void IrcClientLogger::receivedTopic(QString nick, QString user, QString target, QString sMessage)
{
	if (!log(target , "Topic <" + nick + " (" + user + ")> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::receivedTopic log failed");
}

void IrcClientLogger::receivedKick(QString nick, QString user, QString channel, QString target, QString sMessage)
{
	// same even if self target
	if (!log(channel , "Kicked <" + target + "> by  <" + nick + " (" + user + ")> " + sMessage))
		DebugLog(dlWarn, "IrcClientLogger::receivedKick log failed");
}

