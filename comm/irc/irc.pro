
!defined(MLIB_SRC,var):error("MLIB irc (mlib/comm/irc/irc.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/comm/irc

message("MLIB including irc...")

SOURCES +=  $$MSRC/ircaccountconfig.cpp \
            $$MSRC/ircclientconfig.cpp \
            $$MSRC/ircclient.cpp \
            $$MSRC/irchandler.cpp \
            $$MSRC/ircpacket.cpp \ 
            $$MSRC/ircclientlogger.cpp \
            $$MSRC/ircclientlogfile.cpp \
            $$MSRC/ircclientrawlogger.cpp


HEADERS +=  $$MSRC/ircaccountconfig.h \
            $$MSRC/ircclientconfig.h \
            $$MSRC/ircclient.h \
            $$MSRC/irchandler.h \
            $$MSRC/ircpacket.h \ 
            $$MSRC/ircclientlogger.h \
            $$MSRC/ircclientlogfile.h \
            $$MSRC/ircclientrawlogger.h

