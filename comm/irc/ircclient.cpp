#include "comm/irc/ircclient.h"
#include "logging/debuglogger.h"
#include <QDateTime>

IrcClient::IrcClient(QObject *parent) :
	QObject(parent),
	_handler(0, *this),
	_clientStatus(cNone),
	_loginStatus(lNone),
    _rawLogEnabled(false),
    _lastLag(0),
    _averageLag(0.0),
    _averageLagCount(0),
    _outputTimer(0),
    _loginTimer(0),
    _pingTimer(0),
    _watchdogTimer(0)
{
    moveToThread(&_thread);
    connect(&_config, SIGNAL(changed()), this, SLOT(configChanged()));
	connect(&_connection, SIGNAL(statusChanged()), this, SLOT(connectionStatusChanged()));
    connect(&_connection, SIGNAL(readyRead(QIODevice*)), this, SLOT(connectionReadyRead(QIODevice*)));
    connect(&_thread, SIGNAL(started()), this, SLOT(threadStarted()));
    connect(&_handler, SIGNAL(receivedPong(QString,QString,QString,QString)), this, SLOT(receivedPong(QString,QString,QString,QString)));
    _thread.start();
}

IrcClient::~IrcClient()
{
    if (_outputTimer)
    {
        _outputTimer->stop();
        delete _outputTimer;
        _outputTimer = 0;
    }
    if (_loginTimer)
    {
        _loginTimer->stop();
        delete _loginTimer;
        _loginTimer = 0;
    }
    if (_pingTimer)
    {
        _pingTimer->stop();
        delete _pingTimer;
        _pingTimer = 0;

    }
    if (_watchdogTimer)
    {
        _watchdogTimer->stop();
        delete _watchdogTimer;
        _watchdogTimer = 0;
    }
    _thread.quit();
	_thread.wait();
}

void IrcClient::threadStarted()
{
    _outputTimer = new QTimer(this);
    _loginTimer = new QTimer(this);
    _pingTimer = new QTimer(this);
    _watchdogTimer = new QTimer(this);
    connect(_outputTimer, SIGNAL(timeout()), this, SLOT(outputTimerTick()));
    connect(_loginTimer, SIGNAL(timeout()), this, SLOT(loginTimerTick()));
    connect(_pingTimer, SIGNAL(timeout()), this, SLOT(pingTimerTick()));
    connect(_watchdogTimer, SIGNAL(timeout()), this, SLOT(watchdogTimerTick()));
    _pingTimer->start(cPingInterval);
    _outputTimer->start(cOutputTimerInterval);
}

IrcHandler& IrcClient::handler()
{
	return _handler;
}

const IrcHandler& IrcClient::handler() const
{
	return _handler;
}

void IrcClient::setClientStatus(ClientStatus cStatus)
{
	_clientStatus = cStatus;
	emit clientStatusChanged();
}

IrcClient::ClientStatus IrcClient::clientStatus() const
{
	return _clientStatus;
}

QString IrcClient::clientStatusString() const
{
	switch (_clientStatus)
	{
		case cNone:				return "None";
		case cDisconnected:		return "Disconnected";
		case cDisconnecting:	return "Disconnecting";
		case cConnecting:		return "Connecting";
		case cConnected:		return "Connected";
		case cLogin:			return "Login";
		case cReady:			return "Ready";
		case cError:			return "Error";
		default:				return QString("Unknown(%1)").arg(_clientStatus);
	} // switch
}

void IrcClient::setLoginStatus(int iStatus)
{
	_loginStatus = (LoginStatus)iStatus;
	emit loginStatusChanged();
}

IrcClient::LoginStatus IrcClient::loginStatus() const
{
	return _loginStatus;
}

QString IrcClient::loginStatusString() const
{
	switch (_loginStatus)
	{
		case lNone:				return "None";
		case lWaitFirstReply:	return "WaitFirstReply";
		case lFirstReplyDone:	return "FirstReplyDone";
		case lPreAuth:			return "PreAuth";
		case lPreAuthWait:		return "PreAuthWait";
		case lPreAuthDone:		return "PreAuthDone";
		case lMotdWait:			return "MotdWait";
		case lMotdDone:			return "MotdDone";
		case lNickservAuth:		return "NickservAuth";
		case lNickservAuthWait:	return "NickservAuthWait";
		case lNickservAuthDone:	return "NickservAuthDone";
		case lAutoJoin:			return "AutoJoin";
		case lReady:			return "Ready";
		default:				return QString("Unknown(%1)").arg(_loginStatus);
	} // switch
}

bool IrcClient::isReady() const
{
	return (_clientStatus == cReady);
}

bool IrcClient::isConnected() const
{
	switch (_clientStatus)
	{
		case cConnected:
		case cLogin:
		case cReady:
			return true;
		default:
			return false;
	} // switch (_clientStatus)
}

bool IrcClient::isDisconnected() const
{
	switch (_clientStatus)
	{
		case cNone:
		case cDisconnected:
		case cDisconnecting:
		case cError:
			return true;
		default:
			return false;
	} // switch (_clientStatus)
}

bool IrcClient::isPreAuthenticated() const
{
	switch (_loginStatus)
	{
		case lNone:
		case lWaitFirstReply:
		case lFirstReplyDone:
		case lPreAuth:
		case lPreAuthWait:
			return false;
		default:
			return true;
	}
}
bool IrcClient::isLoggedIn() const
{
	switch (_loginStatus)
	{
		case lNickservAuthDone:
		case lAutoJoin:
		case lReady:
			return true;
		default:
			return false;
	}
}

CompositeConnection& IrcClient::connection()
{
	return _connection;
}

void IrcClient::setConfig(const IrcClientConfig& cfg)
{
	_config = cfg;
}

IrcClientConfig& IrcClient::config()
{
	return _config;
}

const IrcClientConfig& IrcClient::config() const
{
	return _config;
}

qint64 IrcClient::lastLag() const
{
    return _lastLag;
}

double IrcClient::averageLag() const
{
    return _averageLag;
}

bool IrcClient::enableWatchdog(int intervalSecs /*= cWatchdogTimerInterval*/)
{
    if (!_watchdogTimer)
    {
        DebugLog(dlWarn, "IrcClient::enableWatchdog Watchdog Timer not valid.");
        return false;
    }
    _lastReady = QDateTime::currentDateTime();
    _watchdogTimer->setInterval(intervalSecs);
    QMetaObject::invokeMethod(_watchdogTimer, "start", Qt::QueuedConnection);
    return true;
}

void IrcClient::disableWatchdog()
{
    if (_watchdogTimer)
        _watchdogTimer->stop();
}

QDateTime IrcClient::lastReady() const
{
    return _lastReady;
}

bool IrcClient::connectClient(bool forceReconnect /*= false*/, bool failOnConnected /*= false*/)
{
    _lastLag = 0.0;
    _averageLag = 0.0;
    _averageLagCount = 0;
    if (!_config.accountConfig().isValid())
	{
		DebugLog(dlWarn, "IrcClient::connectClient account config isValid check failed");
		return false;
	}
	if (_connection.isConnected())
	{
		if (failOnConnected)
		{
			DebugLog(dlWarn, "IrcClient::connectClient already connected for failOnConnected");
			return false;
		}
		if (forceReconnect && !_connection.disconnectHost(false/*failIfDisconnected*/))
		{
			DebugLog(dlWarn, "IrcClient::connectClient connection failed to disconnectHost for forceReconnection");
			return false;
		}
	}
	_connection.config().copy(_config.serverConfig());
	if (!_connection.config().isValid())
	{
        DebugLog(dlWarn, "IrcClient::connectClient server config isValid check failed");
		return false;
	}
    _handler.initLastReceived();
    _lastReady = QDateTime::currentDateTime();
    if (!forceReconnect)
        _connection.wantConnect();
    else
    {
        if (!_connection.reconnectHost(false/*failIfConnecting*/))
        {
            DebugLog(dlWarn, "IrcClient::connectClient (forceReconnect) connection reconnectHost failed");
            return false;
        }
    }
	return true;
}

bool IrcClient::disconnectClient(const QString& sQuitMessage /*= ""*/, bool force /*= false*/)
{
    _lastLag = 0.0;
    _averageLag = 0.0;
    _averageLagCount = 0;
    if (_connection.isConnected())
		sendQuit(sQuitMessage); // ignored if not isRead()
	if (force)
		_connection.disconnectHost(false/*Fail if disconnected*/);
	else
		_connection.wantDisconnect();
	return true;
}

void IrcClient::setRawLogEnabled(bool enabled /*= true*/)
{
	_rawLogEnabled = enabled;
}

bool IrcClient::rawLogEnabled() const
{
	return _rawLogEnabled;
}

void IrcClient::sendLine(const QString& sMsg)
{
	sendRaw(sMsg + "\r\n");
}

void IrcClient::sendRaw(const QString& sMsg)
{
	if (_rawLogEnabled)
		emit sendRawOutput(sMsg);
    _outputQueue.enqueue(sMsg);
}

void IrcClient::outputTimerTick()
{
    if (_outputQueueLock.tryLock())
    {
        while (!_outputQueue.isEmpty())
            _connection.write(_outputQueue.dequeue());
        _outputQueueLock.unlock();
    }
}

void IrcClient::connectionReadyRead(QIODevice* io)
{
    Q_UNUSED(io);
	_inputQueueLock.lock();
	while (_connection.canReadLine())
		_inputQueue.enqueue(QString(_connection.readLine(cMaxReadLineCharacters)));
	_inputQueueLock.unlock();
}

void IrcClient::loginTimerTick()
{
    //DebugLog(dlDebug, "IrcClient::loginTimerTick status: %s", qPrintable(loginStatusString()));
	switch (_loginStatus)
	{
		case lNone:
			if (!isConnected())
                _loginTimer->stop();
			break;
		case lWaitFirstReply:
			// waiting until incremented by connectionReadyRead
			break;
		case lFirstReplyDone:
			DebugLog(dlDebug, "IrcClient::loginTimerTick beginning login process");
			setLoginStatus(lPreAuth);
			break;
		case lPreAuth:
			if (!_config.accountConfig().useSASL())
				setLoginStatus(lPreAuthDone);
			else
			{
				setLoginStatus(lPreAuthWait);
				sendLine("CAP LS");
				sendNickUserLogin();
			}
			break;
		case lPreAuthWait:
			break;
		case lPreAuthDone:
			setLoginStatus(lMotdWait);
			break;
		case lMotdWait:
			break;
		case lMotdDone:
			if (!_config.accountConfig().useSASL())
				setLoginStatus(lNickservAuthDone); // SASL skips Nickserv Authentication
			else
				setLoginStatus(lNickservAuth);
			break;
		case lNickservAuth:
			if (!_config.accountConfig().hasNickservPassword())
				setLoginStatus(lNickservAuthDone); // no Nickserv password
			else
			{
				setLoginStatus(lNickservAuthWait);
				sendLine(QString("NS IDENTIFY %1").arg(_config.accountConfig().nickservPassword()));
			}
			break;
		case lNickservAuthWait:
			break;
		case lNickservAuthDone:
			if (_config.accountConfig().hasAutoJoinChannels())
				setLoginStatus(lAutoJoin);
			else
				setLoginStatus(lReady);
			break;
		case lAutoJoin:
			sendAutoJoins();
			setLoginStatus(lReady);
			break;
		case lReady:
			setClientStatus(cReady);
			DebugLog(dlDebug, "IrcClient::loginTimerTick login process complete, stoping loginTimer");
            _loginTimer->stop();
            _lastReady = QDateTime::currentDateTime();
			break;
	} // switch
}

void IrcClient::pingTimerTick()
{
    if (isConnected())
        sendPing(QString("LAG%1").arg((qlonglong)QDateTime::currentDateTime().toTime_t()));
}

void IrcClient::watchdogTimerTick()
{
    //DebugLog(dlDebug, "IrcClient::watchdogTimerTick login status: %s", qPrintable(loginStatusString()));
    if (_clientStatus == cReady && isConnected())
    {
        qint64 elapsed = _handler.lastReceived().secsTo(QDateTime::currentDateTime());
        if (elapsed > cWatchdogReceiveTimeoutSecs || elapsed < 0) // should be > 0, maybe help during system time change
        {
            _lastReady = QDateTime::currentDateTime();
            DebugLog(dlDebug, "IrcClient::watchdogTimerTick received timed out. Not connected+ready: elapsed: %lld, client status: %s, login status: %s, is connected: %c", elapsed, qPrintable(clientStatusString()), qPrintable(loginStatusString()), (isConnected() ? 'Y' : 'N'));
            emit watchdogError(QString("Watchdog: Receiver timed out. Last received: %1 seconds ago.").arg(elapsed));
        }
    }
    else
    {
        qint64 elapsed = _lastReady.secsTo(QDateTime::currentDateTime());
        if (elapsed > cWatchdogReadyTimeoutSecs || elapsed < 0) // should be > 0, maybe help during system time change
        {
            _lastReady = QDateTime::currentDateTime();
            DebugLog(dlDebug, "IrcClient::watchdogTimerTick login ready timed out. Not connected+ready: elapsed: %lld, client status: %s, login status: %s, is connected: %c", elapsed, qPrintable(clientStatusString()), qPrintable(loginStatusString()), (isConnected() ? 'Y' : 'N'));
            emit watchdogError(QString("Watchdog: Login ready timed out. Last connected: %1 seconds ago.").arg(elapsed));
        }
    }
}

void IrcClient::receivedPong(QString nick, QString user, QString target, QString message)
{
    Q_UNUSED(nick);
    Q_UNUSED(user);
    Q_UNUSED(target);
    if (message.startsWith("LAG"))
    {
        bool ok;
        message = message.remove(0, 3);
        unsigned int timestamp = message.toUInt(&ok);
        if (ok)
        {
            _lastLag = QDateTime::fromTime_t(timestamp).msecsTo(QDateTime::currentDateTime());
            double total = (_averageLag * _averageLagCount /*last count*/) + _lastLag;
            _averageLagCount++;
            _averageLag = total / _averageLagCount;
            if (_averageLagCount > cPingAverageCountMax)
            {
                emit lagAverage(_averageLag);
                _averageLagCount = 1; // leave current average as first entry
            }
        }
    }
}

void IrcClient::connectionStatusChanged()
{
    DebugLog(dlDebug, "IrcClient::connectionStatusChanged status: %s", qPrintable(clientStatusString()));
	if (_connection.isConnected())
	{
		if (!isConnected())
		{
			setLoginStatus(lWaitFirstReply);
            _loginTimer->start(cLoginTimerInterval);
			setClientStatus(cConnected);
            _lastLag = 0.0;
            _averageLag = 0.0;
            _averageLagCount = 0;
			emit connected();
		}
	}
	else if (_connection.isDisconnected())
	{
		if (isConnected())
		{
			setLoginStatus(lNone);
			setClientStatus(cDisconnected);
            emit disconnected();
		}
	}
}

void IrcClient::send(QString str)
{
	sendLine(str);
}

void IrcClient::sendNickChange(QString newNick)
{
	if (newNick.isEmpty())
		newNick = _config.accountConfig().nick();
	sendLine(QString("NICK %1").arg(newNick));
}

void IrcClient::sendNickUserLogin()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendNickUserLogin not connected");
		return;
	}
	sendLine(QString("NICK %1").arg(_config.accountConfig().nick()));
	sendLine(QString("USER %1 %1 %2 :%3")
			 .arg(_config.accountConfig().user())
			 .arg(_connection.config().hostName())
			 .arg(_config.accountConfig().realName()));
}

void IrcClient::sendAuthenticateSASL()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendAuthenticateSASL not connected");
		return;
	}
	if (!_config.accountConfig().useSASL())
	{
		DebugLog(dlError, "IrcClient::sendAuthenticateSASL account not set to use SASL");
		return;
	}
	if (!_config.accountConfig().saslIsValid())
	{
		DebugLog(dlError, "IrcClient::sendAuthenticateSASL account SASL config not valid");
		return;
	}
	QString authHash = _config.accountConfig().saslAuthHash();
	//DebugLog(dlDebug, "IrcClient::sendAuthenticateSASL prepared hash: '%s'", qPrintable(authHash));
	while (authHash.size() >= 400)
	{
		QString authHashPart = authHash.left(400);
		authHash = authHash.remove(0, 400);
		sendLine(QString("AUTHENTICATE %1").arg(authHashPart));
	}
	if (!authHash.isEmpty()) // send what is left
		sendLine(QString("AUTHENTICATE %1").arg(authHash));
	else
		sendLine("AUTHENTICATE +"); // last part was exactly 400 bytes, inform the server that there's nothing more
}

void IrcClient::sendAutoJoins()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendAutoJoins not connected");
		return;
	}
	QStringList chanList = _config.accountConfig().autoJoinChannels().split(QChar(','));
	for (int i = 0;i < chanList.size();i++)
		sendLine(QString("JOIN %1").arg(chanList[i]));
}

void IrcClient::sendJoin(QString sChannel)
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendJoin not connected");
		return;
	}
	sendLine(QString("JOIN %1").arg(sChannel));
}

void IrcClient::sendPart(QString sChannel, QString sMessage)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendPart client not ready");
		return;
	}
	sendLine(QString("PART %1 :%2").arg(sChannel).arg(sMessage));
}

void IrcClient::sendPrivateMessage(QString sNickChannel, QString sMessage)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendPrivateMessage client not ready");
		return;
	}
	sendLine(QString("PRIVMSG %1 :%2").arg(sNickChannel).arg(sMessage));
}

void IrcClient::sendWhois(QString sNick)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendWhois client not ready");
		return;
	}
	sendLine(QString("WHOIS %1").arg(sNick));
}

void IrcClient::sendPing(QString sMessage)
{
    if (!isReady())
    {
        DebugLog(dlWarn, "IrcClient::sendPing client not ready");
        return;
    }
    sendLine(QString("PING %1").arg(sMessage));
}

void IrcClient::sendQuit(QString sMessage)
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendQuit not connected");
		return;
	}
	sendLine(QString("QUIT :%1").arg(sMessage));
}

void IrcClient::sendNickGHost()
{
	QString password;
	if (_config.accountConfig().useSASL())
		password = _config.accountConfig().saslPassword();
	else
		password = _config.accountConfig().nickservPassword();
	sendPrivateMessage("NickServ", QString("GHOST %1 %2")
						.arg(_config.accountConfig().nick())
						.arg(password));
}

void IrcClient::sendNickRelase()
{
	QString password = _config.accountConfig().nickservPassword();
	if (_config.accountConfig().useSASL())
		password = _config.accountConfig().saslPassword();
	else
		password = _config.accountConfig().nickservPassword();
	sendPrivateMessage("NickServ", QString("RELEASE %1 %2")
						.arg(_config.accountConfig().nick())
						.arg(password));
}

void IrcClient::configChanged()
{
	//_connection.config().copy(_config.serverConfig());
}

bool IrcClient::hasInputQueue()
{
	bool isEmpty;
	_inputQueueLock.lock();
	isEmpty = _inputQueue.isEmpty();
	_inputQueueLock.unlock();
	return !isEmpty;
}

QQueue<QString>& IrcClient::lockInputQueue()
{
	_inputQueueLock.lock();
	return _inputQueue;
}

void IrcClient::unlockInputQueue()
{
	_inputQueueLock.unlock();
}

