#include "comm/irc/ircclientrawlogger.h"
#include "logging/timestampedlog.h"
#include "comm/irc/irchandler.h"
#include "logging/debuglogger.h"

IrcClientRawLogger::IrcClientRawLogger(IrcClient& ircclient, QObject* parent /*= 0*/) :
	QObject(parent),
  _irc(ircclient),
  _enabled(false),
  _archiveSize(0),
  _archiveSubdir("archive"),
  _archiveFormat("MM-dd-yy_hh-mm-ss"),
  _logFile(new TimestampedLog(this))
{
	Q_ASSERT(_logFile);
	_logFile->setLogName("IrcClientLogFile");
}

void IrcClientRawLogger::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
	_irc.setRawLogEnabled(_enabled);
	_irc.handler().setRawLogEnabled(_enabled);
	if (!_enabled)
	{
		disconnect(&_irc, 0, this, 0);
		disconnect(&_irc.handler(), 0, this, 0);
	}
	else
	{
		connect(&_irc, SIGNAL(sendRawOutput(QString)), this, SLOT(sendRawOutput(QString)));
		connect(&_irc.handler(), SIGNAL(receivedRawInput(QString)), this, SLOT(receivedRawInput(QString)));
	}
}

bool IrcClientRawLogger::isEnabled() const
{
	return _enabled;
}

void IrcClientRawLogger::setFileName(const QString& fileName)
{
	_logFile->setFileName(fileName);
}

const QString& IrcClientRawLogger::fileName() const
{
	return _logFile->fileName();
}

void IrcClientRawLogger::setArchiving(qint64 size, const QString& archiveSubdir /*= QString("archive")*/, const QString& fileNameFormat /*= QString("MM-dd-yy_hh-mm-ss")*/)
{
	_archiveSize = size;
	_archiveFormat = fileNameFormat;
	_archiveSubdir = archiveSubdir;
}

TimestampedLog* IrcClientRawLogger::logFile()
{
	return _logFile;
}

const TimestampedLog* IrcClientRawLogger::logFile() const
{
	return _logFile;
}

void IrcClientRawLogger::log(const char* strFmt, ...)
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	_logFile->log(sMsg);
}

void IrcClientRawLogger::log(const QString& sMsg)
{
	_logFile->log(sMsg);
}

void IrcClientRawLogger::sendRawOutput(QString sRawOutput)
{
	_logFile->log(QString(">>") + sRawOutput);
}

void IrcClientRawLogger::receivedRawInput(QString sRawInput)
{
	_logFile->log(QString("<<") + sRawInput);
}
