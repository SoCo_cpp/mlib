#ifndef IRCCLIENTLOGGER_H
#define IRCCLIENTLOGGER_H

#include <QObject>
#include <QList>
#include <QTimer>
#include "comm/irc/ircclient.h"
#include "logging/timestampedlog.h"
#include "threads/onetomanylocker.h"

class IrcClientLogFile; // predeclared

class IrcClientLogger : public QObject
{
	Q_OBJECT
public:
	static const int C_LogFile_Expired_Time_Ms = 180000; // expires for reuse: 3 minutes
	static const int C_LogFile_Release_Time_Ms = 240000; // expires for release: 4 minutes
	static const int C_LogFile_Sweep_Interval_Ms = 300000; // scrub for releases every 5 minutes

	typedef enum {loConnectDisconnect = 0, loPrivMsg, loChannelMsg, loChannelJoinPart, loChannelQuit, loChannelKick, loChannelNick, loChannelMode, loChannelTopic,
				  loSelfPrivMsg, loSelfChanMsg, loSelfJoinPart, loSelfQuit, loSelfKick, loSelfNick, loSlefMode, loMax} LogOptions;

	explicit IrcClientLogger(IrcClient& ircclient, QObject* parent = 0);

	void setEnabled(bool enabled = true);
	bool isEnabled() const;

	void setBaseLogPath(const QString& sBaseLogPath);
	const QString& baseLogPath() const;

	void setArchiving(qint64 size, const QString& archiveSubdir = QString("archive"), const QString& fileNameFormat = QString("_MM-dd-yy_hh-mm-ss"));

	void clearLogOptions();
	void setAllLogOptions();
	quint64 rawLogOptionMask(LogOptions opt);
	void setRawLogOptions(quint64 rawOpt);
	quint64 rawLogOptions() const;
	void setLogOption(LogOptions opt);
	void clearLogOption(LogOptions opt);
	bool hasLogOption(LogOptions opt);

	bool log(const QString& logName, const char* strFmt, ...);
	bool log(const QString& logName, const QString& sMsg);

protected:
	IrcClient& _irc;
	bool _enabled;
	quint64 _logOptions;
	QString _baseLogPath;
	qint64  _archiveSize;
	QString _archiveSubdir;
	QString _archiveFormat;
	QList<IrcClientLogFile*> _logFiles;
	OneToManyLocker _logFilesLocker;
	QTimer _logFilesCleanupTimer;

	IrcClientLogFile* getLogFile(const QString& logName); //leaves many locked

signals:
	void logOptionsChanged();

public slots:
	void updateLogOptionBindings();
	void cleanupLogFilesTimerTick();
	void clientConnected();
	void clientDisconnected();
	void sendPrivateMessage(QString sNickChannel, QString sMessage);
	void sendQuit(QString sMessage);
	void receivedNotice(QString nick, QString user, QString target, QString sMessage);
	void receivedPrivmsg(QString nick, QString user, QString target, QString sMessage);
	void receivedJoin(QString nick, QString user, QString channel);
	void receivedPart(QString nick, QString user, QString channel, QString sMessage);
	void receivedQuit(QString nick, QString user, QString message);
	void receivedNick(QString nick, QString user,QString newNick);
	void receivedMode(QString nick, QString user, QString target, QString params);
	void receivedTopic(QString nick, QString user, QString target, QString message);
	void receivedKick(QString nick, QString user, QString channel, QString target, QString sMessage);
};

#endif // IRCCLIENTLOGGER_H
