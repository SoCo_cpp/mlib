#ifndef IRCCLIENTRAWLOGGER_H
#define IRCCLIENTRAWLOGGER_H

#include <QObject>
#include "comm/irc/ircclient.h"
#include "logging/timestampedlog.h"

class TimestampedLog; // predeclared

class IrcClientRawLogger : public QObject
{
	Q_OBJECT
public:
	explicit IrcClientRawLogger(IrcClient& ircclient, QObject* parent = 0);

	void setEnabled(bool enabled = true);
	bool isEnabled() const;

	void setFileName(const QString& fileName);
	const QString& fileName() const;

	void setArchiving(qint64 size, const QString& archiveSubdir = QString("archive"), const QString& fileNameFormat = QString("MM-dd-yy_hh-mm-ss"));

	TimestampedLog* logFile();
	const TimestampedLog* logFile() const;

	void log(const char* strFmt, ...);
	void log(const QString& sMsg);

protected:
	IrcClient& _irc;
	bool _enabled;
	qint64  _archiveSize;
	QString _archiveSubdir;
	QString _archiveFormat;
	TimestampedLog* _logFile;

signals:

public slots:
	void sendRawOutput(QString sRawOutput);
	void receivedRawInput(QString sRawInput);
};

#endif // IRCCLIENTRAWLOGGER_H
