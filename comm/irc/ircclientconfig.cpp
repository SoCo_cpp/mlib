#include "comm/irc/ircclientconfig.h"
#include "logging/debuglogger.h"

IrcClientConfig::IrcClientConfig(QObject* parent /*= 0*/) :
	QObject(parent)
{
	connect(&_accountConfig, SIGNAL(changed()), this, SIGNAL(changed()));
}

IrcClientConfig::IrcClientConfig(const IrcClientConfig& cpy) :
	QObject(0)
{
	copy(cpy);
}

IrcClientConfig& IrcClientConfig::operator =(const IrcClientConfig& cpy)
{
	copy(cpy);
	return *this;
}

void IrcClientConfig::copy(const IrcClientConfig& cpy)
{
	_accountConfig = cpy._accountConfig;
	_serverConfig = cpy._serverConfig;
}

CompositeConnectionConfig& IrcClientConfig::serverConfig()
{
	return _serverConfig;
}

const CompositeConnectionConfig& IrcClientConfig::serverConfig() const
{
	return _serverConfig;
}

IrcAccountConfig& IrcClientConfig::accountConfig()
{
	return _accountConfig;
}

const IrcAccountConfig& IrcClientConfig::accountConfig() const
{
	return _accountConfig;
}

/*
#ifdef JSON_CONFIG
void IrcClientConfig::saveJson(QJsonObject& jsonObj) const
{
	QJsonObject jsonAccountObj;
	_accountConfig.saveJson(jsonAccountObj);

	jsonObj["ver"]			= 1;
	jsonObj["account"]		= jsonAccountObj;
	ProxyServerConfig::saveJson(jsonObj);
}
#endif // #ifdef JSON_CONFIG

#ifdef JSON_CONFIG
bool IrcClientConfig::loadJson(const QJsonObject& jsonObj, bool bRequireAllFields , bool noChangeSignal )
{
	copy(IrcClientConfig(), true); //noChangeSignal
	if (bRequireAllFields)
		if ( !jsonObj.contains("account"))
		{
			DebugLog(dlWarn, "IrcClientConfig::loadJson all fields required, but account config is missing");
			return false;
		}

	if (!jsonObj.contains("ver"))
	{
		DebugLog(dlWarn, "IrcClientConfig::loadJson version field missing");
		return false;
	}

	if (jsonObj.contains("account"))
	{
		if (!jsonObj["account"].isObject())
		{
			DebugLog(dlWarn, "IrcClientConfig::loadJson account field is not an object");
			return false;
		}
		if (!_accountConfig.loadJson(jsonObj["account"].toObject(), bRequireAllFields, true)) ///noChangeSignal
		{
			DebugLog(dlWarn, "IrcClientConfig::loadJson account config failed to loadJson");
			return false;
		}
	}

	if (!ProxyServerConfig::loadJson(jsonObj, bRequireAllFields, true))//noChangeSignal
	{
		DebugLog(dlWarn, "IrcClientConfig::loadJson ProxyServerConfig::loadJson failed");
		return false;
	}
	if (!noChangeSignal)
		emit changed();
	return true;
}
#endif // #ifdef JSON_CONFIG
*/
