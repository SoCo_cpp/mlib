#ifndef IRCCLIENTCONFIG_H
#define IRCCLIENTCONFIG_H

#include <QObject>
#include "comm/irc/ircaccountconfig.h"
#include "comm/compositeconnectionconfig.h"

class IrcClientConfig : public QObject
{
	Q_OBJECT
public:
	explicit IrcClientConfig(QObject *parent = 0);
	IrcClientConfig(const IrcClientConfig& cpy);
	IrcClientConfig& operator =(const IrcClientConfig& cpy);
	void copy(const IrcClientConfig& cpy);

	CompositeConnectionConfig& serverConfig();
	const CompositeConnectionConfig& serverConfig() const;

	IrcAccountConfig& accountConfig();
	const IrcAccountConfig& accountConfig() const;

protected:
	CompositeConnectionConfig _serverConfig;
	IrcAccountConfig _accountConfig;

signals:
	void changed();

public slots:

};

#endif // IRCCLIENTCONFIG_H
