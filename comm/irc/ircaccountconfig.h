#ifndef IRCACCOUNTCONFIG_H
#define IRCACCOUNTCONFIG_H

#include <QObject>

class IrcAccountConfig : public QObject
{
	Q_OBJECT
public:
	explicit IrcAccountConfig(QObject *parent = 0, const QString& nick = "", const QString& user = "", const QString& realName = "", const QString& nickservPassword = "", const QString& _autoJoinChannelsCSV = "");
	IrcAccountConfig(const IrcAccountConfig& cpy);
	IrcAccountConfig& operator = (const IrcAccountConfig& cpy);
	void copy(const IrcAccountConfig& cpy, bool noChangeSignal = false);

	void setNick(const QString& nick);
	const QString& nick() const;

	void setUser(const QString& user);
	const QString& user() const;

	void setRealName(const QString& name);
	const QString& realName() const;

	void setNickservPassword(const QString& password);
	const QString& nickservPassword() const;
	bool hasNickservPassword() const;

	void set(const QString& nick, const QString& user = "", const QString& realName = "", const QString& nickservPassword = "");

	void setSASL(const QString& nick, const QString& password);
	void clearSASL();
	bool useSASL();
	void setUseSASL(bool usesasl = true);
	void setSASLNick(const QString& snick);
	void setSASLPassword(const QString& spass);
	const QString& saslNick() const;
	const QString& saslPassword() const;
	QString saslAuthHash() const;

	void setAutoJoinChannels(const QString& channels = "");
	void appendAutoJoinChannel(const QString& channel, const QString& delimiter = ",");
	const QString& autoJoinChannels() const;
	bool hasAutoJoinChannels() const;

	bool isValid(bool requireAuth = false) const;
	bool saslIsValid() const;

protected:
	QString _nick;
	QString _user;
	QString _realName;
	QString _nickservPassword;
	bool	_useSASL;
	QString _saslNick;
	QString _saslPassword;
	QString _autoJoinChannels;


signals:
	void changed();

public slots:

};

#endif // IRCACCOUNTCONFIG_H
