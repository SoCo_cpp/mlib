#include "comm/irc/ircaccountconfig.h"
#include "logging/debuglogger.h"

IrcAccountConfig::IrcAccountConfig(QObject *parent, const QString& nick /*= ""*/, const QString& user /*= ""*/, const QString& realName /*= ""*/, const QString& nickservPassword /*= ""*/, const QString& _autoJoinChannelsCSV /*= ""*/) :
	QObject(parent),
	_nick(nick),
	_user(user),
	_realName(realName),
	_nickservPassword(nickservPassword),
	_useSASL(false),
	_saslNick(""),
	_saslPassword(""),
	_autoJoinChannels(_autoJoinChannelsCSV)
{
}

IrcAccountConfig::IrcAccountConfig(const IrcAccountConfig& cpy) :
	QObject(0)
{
	copy(cpy);
}

IrcAccountConfig& IrcAccountConfig::operator = (const IrcAccountConfig& cpy)
{
	copy(cpy);
	return *this;
}

void IrcAccountConfig::copy(const IrcAccountConfig& cpy, bool noChangeSignal /*= false*/)
{
	_nick				= cpy._nick;
	_user				= cpy._user;
	_realName			= cpy._realName;
	_nickservPassword	= cpy._nickservPassword;
	_useSASL			= cpy._useSASL;
	_saslNick			= cpy._saslNick;
	_saslPassword		= cpy._saslPassword;
	_autoJoinChannels	= cpy._autoJoinChannels;
	if (!noChangeSignal)
		emit changed();
}

void IrcAccountConfig::setNick(const QString& nick)
{
	_nick = nick;
	emit changed();
}

const QString& IrcAccountConfig::nick() const
{
	return _nick;
}

void IrcAccountConfig::setUser(const QString& user)
{
	_user = user;
	emit changed();
}

const QString& IrcAccountConfig::user() const
{
	return _user;
}

void IrcAccountConfig::setRealName(const QString& name)
{
	_realName = name;
	emit changed();
}

const QString& IrcAccountConfig::realName() const
{
	return _realName;
}

void IrcAccountConfig::setNickservPassword(const QString& password)
{
	_nickservPassword = password;
	emit changed();
}

const QString& IrcAccountConfig::nickservPassword() const
{
	return _nickservPassword;
}

bool IrcAccountConfig::hasNickservPassword() const
{
	return !_nickservPassword.isEmpty();
}

void IrcAccountConfig::set(const QString& nick, const QString& user /*= ""*/, const QString& realName /*= ""*/, const QString& nickservPassword /*= ""*/)
{
	_nick				= nick;
	_user				= user;
	_realName			= realName;
	_nickservPassword	= nickservPassword;
	emit changed();
}

void IrcAccountConfig::setSASL(const QString& nick, const QString& password)
{
	_useSASL		= true;
	_saslNick		= nick;
	_saslPassword	= password;
	emit changed();
}

void IrcAccountConfig::clearSASL()
{
	_useSASL = false;
	emit changed();
}

bool IrcAccountConfig::useSASL()
{
	return _useSASL;
}

void IrcAccountConfig::setUseSASL(bool usesasl /*= true*/)
{
	_useSASL = usesasl;
	emit changed();
}

void IrcAccountConfig::setSASLNick(const QString& snick)
{
	_saslNick = snick;
	emit changed();
}

void IrcAccountConfig::setSASLPassword(const QString& spass)
{
	_saslPassword = spass;
	emit changed();
}

const QString& IrcAccountConfig::saslNick() const
{
	return _saslNick;
}

const QString& IrcAccountConfig::saslPassword() const
{
	return _saslPassword;
}

QString IrcAccountConfig::saslAuthHash() const
{
	QByteArray authData;
	authData.append(_saslNick);
	authData.append(QChar('\0'));
	authData.append(_saslNick);
	authData.append(QChar('\0'));
	authData.append(_saslPassword);
	return authData.toBase64();
}

void IrcAccountConfig::setAutoJoinChannels(const QString& channels /*= ""*/)
{
	_autoJoinChannels = channels;
	emit changed();
}

void IrcAccountConfig::appendAutoJoinChannel(const QString& channel, const QString& delimiter /*= ","*/)
{
	if (_autoJoinChannels.isEmpty())
		_autoJoinChannels = channel;
	else
		_autoJoinChannels += delimiter + channel;
	emit changed();
}

const QString& IrcAccountConfig::autoJoinChannels() const
{
	return _autoJoinChannels;
}

bool IrcAccountConfig::hasAutoJoinChannels() const
{
	return !_autoJoinChannels.isEmpty();
}

bool IrcAccountConfig::isValid(bool requireAuth /*= false*/) const
{
	if (_nick.isEmpty())
		return false;
	if (!_useSASL && (_saslNick.isEmpty() || _saslPassword.isEmpty()))
		return false; // SASL requires both a nick and password
	if (requireAuth && _nickservPassword.isEmpty() && !_useSASL)
		return false; // no auth (nickserv password isn't need with SASL)
	return true;
}

bool IrcAccountConfig::saslIsValid() const
{
	if (_saslNick.isEmpty())
		return false;
	if (_saslPassword.isEmpty())
		return false;
	return true;
}

/*
#ifdef JSON_CONFIG
void IrcAccountConfig::saveJson(QJsonObject& jsonObj) const
{
	jsonObj["ver"]					= 1;
	jsonObj["nick"]					= _nick;
	jsonObj["user"]					= _user;
	jsonObj["realname"]				= _realName;
	jsonObj["nickserv-password"]	= _nickservPassword;
	jsonObj["sasl-enabled"]			= _useSASL;
	jsonObj["sasl-nick"]			= _saslNick;
	jsonObj["sasl-password"]		= _saslPassword;
	jsonObj["auto-join"]			= _autoJoinChannels;
}
#endif // #ifdef JSON_CONFIG

*/
