#include "file/jsonconfigfile.h"
#include <QJsonDocument>
#include "logging/debuglogger.h"

JsonConfigFile::JsonConfigFile(QObject* parent /*= 0*/, const QString& sFileName /*= QString()*/, bool bCompactJson /*= true*/) :
	JsonFile(parent, sFileName, bCompactJson)
{
	_jsonSection = json()->object();
}

void JsonConfigFile::clear()
{
	JsonFile::clear();
	_sections.clear();
	_jsonSection = QJsonObject();
}

bool JsonConfigFile::save(bool bClearOnSuccess /*= true*/)
{
	if (_sections.size() != 0)
	{
		DebugLog(dlWarn, "JsonConfigFile::save not at base section");
		return false;
	}
	json()->setObject(_jsonSection);
	if (!JsonFile::save(bClearOnSuccess))
	{
		DebugLog(dlWarn, "JsonConfigFile::save JsonFile::save failed");
		return false;
	}
	return true;
}

bool JsonConfigFile::load()
{
	if (!JsonFile::load())
	{
		DebugLog(dlWarn, "JsonConfigFile::load JsonFile::load failed");
		return false;
	}
	_jsonSection = json()->object();
	return true;
}

bool JsonConfigFile::containsSection(const QString& name)
{
	return _jsonSection.contains(name) && _jsonSection[name].isObject();
}

bool JsonConfigFile::findSection(const QString& name, int maxVersion /*= -1*/)
{
	if (_jsonSection.contains(name) && _jsonSection[name].isObject())
	{
		_sections.append(_jsonSection);
		_jsonSection = _jsonSection[name].toObject();
		if (maxVersion != -1 && _jsonSection["cfgsec-version"].toInt() > maxVersion)
		{
			DebugLog(dlWarn, "JsonConfigFile::findSection version exceeded section: %s, version: %d, maxver: %d", qPrintable(name), _jsonSection["cfgsec-version"].toInt(), maxVersion);
			return false;
		}
		return true;
	}
	return false;
}

void JsonConfigFile::pushSection(const QString& name, int version)
{
	_sections.append(_jsonSection);
	_jsonSection = QJsonObject();
	_jsonSection["cfgsec-name"] = name;
	_jsonSection["cfgsec-version"] = version;
}

bool JsonConfigFile::popSection()
{
	if (_sections.size() == 0)
		return false;
	QJsonObject prevSection = _sections.takeLast();
	prevSection[_jsonSection["cfgsec-name"].toString()] = _jsonSection;
	_jsonSection = prevSection;
	return true;
}

bool JsonConfigFile::isBaseSection() const
{
	return (_sections.size() == 0);
}

QJsonObject& JsonConfigFile::currentSection()
{
	return _jsonSection;
}

const QJsonObject& JsonConfigFile::currentSection() const
{
	return _jsonSection;
}

QString JsonConfigFile::sectionName() const
{
	return _jsonSection["cfgsec-name"].toString();
}

int JsonConfigFile::sectionVersion() const
{
	return _jsonSection["cfgsec-version"].toInt();
}

bool JsonConfigFile::contains(const QString& key) const
{
	return _jsonSection.contains(key);
}

QVariant JsonConfigFile::value(const QString& key) const
{
	return _jsonSection[key].toVariant();

}

void JsonConfigFile::setValue(const QString& key, const QVariant& value)
{
	_jsonSection[key] = QJsonValue::fromVariant(value);
}


