#include "file/jsonfile.h"
#include "logging/debuglogger.h"
#include <QJsonDocument>
#include <QFile>

JsonFile::JsonFile(QObject* parent /*= 0*/, const QString& sFileName /*= QString()*/, bool bCompactJson /*= true*/) :
	QObject(parent),
	_jsonDoc(0),
	_fileName(sFileName),
	_compactJson(bCompactJson)
{
	_jsonDoc = new QJsonDocument();
}

JsonFile::~JsonFile()
{
	if (_jsonDoc)
	{
		delete _jsonDoc;
		_jsonDoc = 0;
	}
}

JsonFile::JsonFile(const JsonFile& cpy) :
	QObject(0)
{
	copy(cpy);
}

JsonFile& JsonFile::operator =(const JsonFile& rhs)
{
	copy(rhs);
	return *this;
}

void JsonFile::copy(const JsonFile& cpy)
{
	_fileName		= cpy._fileName;
	_compactJson	= cpy._compactJson;
	*_jsonDoc		= *cpy._jsonDoc;
}

QJsonDocument* JsonFile::json()
{
	return _jsonDoc;
}

const QJsonDocument* JsonFile::json() const
{
	return _jsonDoc;
}

void JsonFile::setFileName(const QString& sFileName)
{
	_fileName = sFileName;
}

const QString& JsonFile::fileName() const
{
	return _fileName;
}

void JsonFile::setCompactJson(bool bCompact /*= true*/)
{
	_compactJson = bCompact;
}

bool JsonFile::compactJson() const
{
	return _compactJson;
}

bool JsonFile::exists() const
{
	QFile file;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonFile::exists file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	return file.exists();
}

bool JsonFile::save(bool bClearOnSuccess /*= true*/)
{
	QFile file;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonFile::save file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
	{
		DebugLog(dlWarn, "JsonFile::save open file for writing failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	if (file.write(_jsonDoc->toJson( (_compactJson ? QJsonDocument::Compact : QJsonDocument::Indented) )) == -1)
	{
		DebugLog(dlWarn, "JsonFile::save ile write failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	if (bClearOnSuccess)
		clear();
	return true;
}

bool JsonFile::load()
{
	QFile file;
	QJsonParseError jsonParseError;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonFile::load file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		DebugLog(dlWarn, "JsonFile::load open file for reading failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	*_jsonDoc = QJsonDocument::fromJson(file.readAll(), &jsonParseError);
	if (jsonParseError.error != QJsonParseError::NoError)
	{
		DebugLog(dlWarn, "JsonFile::load parse json file failed: %s File name: %s", qPrintable(jsonParseError.errorString()), qPrintable(_fileName));
		return false;
	}
	return true;
}

void JsonFile::clear()
{
	*_jsonDoc = QJsonDocument();
}
