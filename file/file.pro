
!defined(MLIB_SRC,var):error("MLIB file (mlib/file/file.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/file

message("MLIB including file...")

SOURCES += \
            $$MSRC/jsonfile.cpp \
            $$MSRC/jsonconfigfile.cpp

HEADERS += \
            $$MSRC/jsonfile.h \
            $$MSRC/jsonconfigfile.h
