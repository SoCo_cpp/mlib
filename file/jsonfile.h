#ifndef JSONFILE_H
#define JSONFILE_H

#include <QObject>

class QJsonDocument; // predeclared

class JsonFile : public QObject
{
	Q_OBJECT
public:
	explicit JsonFile(QObject* parent = 0, const QString& sFileName = QString(), bool bCompactJson = true);
	~JsonFile();
	JsonFile(const JsonFile& cpy);
	JsonFile& operator =(const JsonFile& rhs);
	void copy(const JsonFile& cpy);

	QJsonDocument* json();
	const QJsonDocument* json() const;

	void setFileName(const QString& sFileName);
	const QString& fileName() const;

	void setCompactJson(bool bCompact = true);
	bool compactJson() const;

	bool exists() const;
	virtual bool save(bool bClearOnSuccess = true);
	virtual bool load();

	virtual void clear();

protected:
	QJsonDocument* _jsonDoc;
	QString _fileName;
	bool _compactJson;

signals:

public slots:

};

#endif // JSONFILE_H
