#ifndef JSONCONFIGFILE_H
#define JSONCONFIGFILE_H

#include <QObject>
#include <QVariant>
#include <QJsonObject>
#include <QList>
#include "file/jsonfile.h"


class JsonConfigFile : public JsonFile
{
	Q_OBJECT
public:
	explicit JsonConfigFile(QObject* parent = 0, const QString& sFileName = QString(), bool bCompactJson = true);

	virtual void clear();
	virtual bool save(bool bClearOnSuccess = true);
	virtual bool load();

	bool containsSection(const QString& name);
	bool findSection(const QString& name, int maxVersion = -1);
	void pushSection(const QString& name, int version);
	bool popSection();

	bool isBaseSection() const;
	QJsonObject& currentSection();
	const QJsonObject& currentSection() const;

	QString sectionName() const;
	int sectionVersion() const;

	bool contains(const QString& key) const;
	QVariant value(const QString& key) const;

	void setValue(const QString& key, const QVariant& value);


protected:
	QJsonObject _jsonSection;
	QList<QJsonObject> _sections;

signals:

public slots:

};

#endif // JSONCONFIGFILE_H
