#ifndef ONETOMANYLOCKER_H
#define ONETOMANYLOCKER_H

#include <QObject>
#include <QMutex>

class OneToManyLocker : public QObject
{
	Q_OBJECT
public:
	explicit OneToManyLocker(QObject* parent = 0);

	void lockOne();
	bool tryLockOne(int timeout = 0);
	void unlockOne();

	void unlockOneAndLockMany();

	void lockMany();
	bool tryLockMany(int timeout = 0);
	void unlockMany();

protected:
	int _refCountMany;
	QMutex _mxOne;
	QMutex _mxMany;

signals:

public slots:

};

#endif // ONETOMANYLOCKER_H
