
!defined(MLIB_SRC,var):error("MLIB file (mlib/threads/threads.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/threads

message("MLIB including threads...")

HEADERS += \
        $$MSRC/onetomanylocker.h

SOURCES += \
        $$MSRC/onetomanylocker.cpp
