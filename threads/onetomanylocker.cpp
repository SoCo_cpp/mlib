#include "threads/onetomanylocker.h"
#include <QThread>
#include <QTime>

OneToManyLocker::OneToManyLocker(QObject *parent) :
	QObject(parent),
	_refCountMany(0)
{
}

void OneToManyLocker::lockOne()
{
	_mxOne.lock();
	while (_refCountMany)
		QThread::usleep(100);
}

bool OneToManyLocker::tryLockOne(int timeout /*= 0*/)
{
	QTime time;
	time.start();
	if (!_mxOne.tryLock(timeout))
		return false; // timed out
	while (_refCountMany && time.elapsed() < timeout)
		QThread::usleep(100);
	if (_refCountMany) // timed out
	{
		_mxOne.unlock();
		return false;
	}
	return true;
}

void OneToManyLocker::unlockOne()
{
	_mxOne.unlock();
}

void OneToManyLocker::unlockOneAndLockMany()
{
	_mxMany.lock();
	_refCountMany++;
	_mxMany.unlock();
	_mxOne.unlock();
}

void OneToManyLocker::lockMany()
{
	_mxOne.lock();
	_mxMany.lock();
	_refCountMany++;
	_mxMany.unlock();
	_mxOne.unlock();
}

bool OneToManyLocker::tryLockMany(int timeout /*= 0*/)
{
	bool r = false;
	if (_mxOne.tryLock(timeout))
	{
		if (_mxMany.tryLock(timeout))
		{
			_refCountMany++;
			_mxMany.unlock();
			r = true;
		}
		_mxOne.unlock();
	}
	return r;
}

void OneToManyLocker::unlockMany()
{
	_mxMany.lock();
	_refCountMany--;
	_mxMany.unlock();
}
