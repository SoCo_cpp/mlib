#ifndef CIRCLECONSTS_H
#define CIRCLECONSTS_H

namespace MLibMath
{
	static const double pi = 3.14159265358979323846;
	static const double pi2 = 1.57079632679489661923;
	static const double raddeg = 57.2957795129;
	static const double degrad = 0.01745329252;

}

#endif // CIRCLECONSTS_H
