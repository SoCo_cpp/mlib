#ifndef VECTOR3DENABLED_H
#define VECTOR3DENABLED_H

#include <QVector3D>

class Vector3DEnabled : public QVector3D
{
public:
	Vector3DEnabled(bool enabled = false);
	Vector3DEnabled(float x, float y, float z, bool enabled = false);
	Vector3DEnabled(const QVector3D& vector, bool enabled = false);
	Vector3DEnabled(const Vector3DEnabled& cpy);
	const Vector3DEnabled& operator=(const Vector3DEnabled& rhs);
	void copy(const Vector3DEnabled& rhs);
	bool isEnabled() const;
	bool isDisabled() const;
	void setEnabled(bool enabled = true);
	void setDisabled(bool disabled = true);
	void set(float xyz);
	void set(float xyz, bool enabled);
	void set(float x, float y, float z);
	void set(float x, float y, float z, bool enabled);
	void add(float dx, float dy = 0.0, float dz = 0.0);
	void multiply(float factor);
	void multiply(float dx, float dy, float dz);

protected:
	bool _enabled;
};

//-------------------------------------------------------------

class Vector3DAngleEnabled : public Vector3DEnabled
{
public:
	Vector3DAngleEnabled(bool enabled = false);
	Vector3DAngleEnabled(float x, float y, float z, float angle = 0.0, bool enabled = false);
	Vector3DAngleEnabled(const QVector3D& vector, float angle = 0.0, bool enabled = false);
	Vector3DAngleEnabled(const Vector3DEnabled& enVector, float angle = 0.0);
	Vector3DAngleEnabled(const Vector3DAngleEnabled& cpy);
	const Vector3DAngleEnabled& operator=(const Vector3DAngleEnabled& rhs);
	void copy(const Vector3DAngleEnabled& rhs);
	float angle() const;
	void setAngle(float angle);
protected:
	float _angle;
};


#endif // VECTOR3DENABLED_H
