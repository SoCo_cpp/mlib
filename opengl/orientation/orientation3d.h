#ifndef ORIENTATION_H
#define ORIENTATION_H

#include "opengl/orientation/vector3denabled.h"

class GLView; // predeclared
class Orientation3D
{
public:
	Orientation3D();
	Orientation3D(const Orientation3D& cpy);
	const Orientation3D& operator=(const Orientation3D& rhs);
	void copy(const Orientation3D& cpy);

	void reset();

	Vector3DEnabled& position();
	const Vector3DEnabled& position() const;

	Vector3DEnabled& scale();
	const Vector3DEnabled& scale() const;

	Vector3DEnabled& componentRotation();
	const Vector3DEnabled& componentRotation() const;

	Vector3DAngleEnabled& vectorRotation();
	const Vector3DAngleEnabled& vectorRotation() const;

    void moveBy(float dx, float dy = 0.0, float dz = 0.0);
    void rotateBy(float dx, float dy = 0.0, float dz = 0.0);
    void scaleBy(float dx, float dy = 0.0, float dz = 0.0);

	void paintGL(GLView* glview);

protected:
	Vector3DEnabled _position;
	Vector3DEnabled _scale;
	Vector3DEnabled _componentRotation;
	Vector3DAngleEnabled _vectorRotation;
};

#endif // ORIENTATION_H
