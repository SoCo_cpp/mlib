#include "opengl/orientation/orientation3d.h"
#include "opengl/view/glview.h"

Orientation3D::Orientation3D()
{
	reset();
}

Orientation3D::Orientation3D(const Orientation3D& cpy)
{
	copy(cpy);
}

const Orientation3D& Orientation3D::operator=(const Orientation3D& rhs)
{
	copy(rhs);
	return *this;
}

void Orientation3D::copy(const Orientation3D& cpy)
{
	_position = cpy._position;
	_scale = cpy._scale;
	_componentRotation = cpy._componentRotation;
	_vectorRotation = cpy._vectorRotation;
}

void Orientation3D::reset()
{
	_position = Vector3DEnabled(0.0, 0.0, 0.0);
	_scale    = Vector3DEnabled(1.0, 1.0, 1.0);
	_componentRotation = Vector3DEnabled(0.0, 0.0, 0.0);
	_vectorRotation = Vector3DAngleEnabled(0.0, 0.0, 0.0);
}

Vector3DEnabled& Orientation3D::position()
{
	return _position;
}

const Vector3DEnabled& Orientation3D::position() const
{
	return _position;
}

Vector3DEnabled& Orientation3D::scale()
{
	return _scale;
}

const Vector3DEnabled& Orientation3D::scale() const
{
	return _scale;
}

Vector3DEnabled& Orientation3D::componentRotation()
{
	return _componentRotation;
}

const Vector3DEnabled& Orientation3D::componentRotation() const
{
	return _componentRotation;
}

Vector3DAngleEnabled& Orientation3D::vectorRotation()
{
	return _vectorRotation;
}

const Vector3DAngleEnabled& Orientation3D::vectorRotation() const
{
	return _vectorRotation;
}

void Orientation3D::moveBy(float dx, float dy /*= 0.0*/, float dz /*= 0.0*/)
{
	_position.add(dx, dy, dz);
}

void Orientation3D::rotateBy(float dx, float dy /*= 0.0*/, float dz /*= 0.0*/)
{
   _componentRotation.add(dx, dy, dz);
}

void Orientation3D::scaleBy(float dx, float dy /*= 0.0*/, float dz /*= 0.0*/)
{
	_scale.add(dx, dy, dz);
}

void Orientation3D::paintGL(GLView* glview)
{
	Q_UNUSED(glview);
	if (_position.isEnabled())
		glTranslated(_position.x(), _position.y(), _position.z());
	if (_vectorRotation.isEnabled())
		glRotated(_vectorRotation.angle(), _vectorRotation.x(), _vectorRotation.y(), _vectorRotation.z());
	if (_scale.isEnabled())
		glScaled(_scale.x(),_scale.y(),_scale.z());
	if (_componentRotation.isEnabled())
	{
		glRotatef(_componentRotation.x() / 16.0, 1.0, 0.0, 0.0);
		glRotatef(_componentRotation.y() / 16.0, 0.0, 1.0, 0.0);
		glRotatef(_componentRotation.z() / 16.0, 0.0, 0.0, 1.0);
	}
}
