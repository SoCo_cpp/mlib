#include "opengl/orientation/vector3denabled.h"

Vector3DEnabled::Vector3DEnabled(bool enabled /*= false*/) :
	_enabled(enabled)
{

}

Vector3DEnabled::Vector3DEnabled(float x, float y, float z, bool enabled /*= false*/) :
	QVector3D(x, y, z),
	_enabled(enabled)
{

}

Vector3DEnabled::Vector3DEnabled(const QVector3D& vector, bool enabled /*= false*/) :
	QVector3D(vector),
	_enabled(enabled)
{

}

Vector3DEnabled::Vector3DEnabled(const Vector3DEnabled& cpy) :
	QVector3D()
{
	copy(cpy);
}

const Vector3DEnabled& Vector3DEnabled::operator=(const Vector3DEnabled& rhs)
{
	copy(rhs);
	return *this;
}

void Vector3DEnabled::copy(const Vector3DEnabled& rhs)
{
	setX(rhs.x());
	setY(rhs.y());
	setZ(rhs.z());
	_enabled = rhs._enabled;
}

bool Vector3DEnabled::isEnabled() const
{
	return _enabled;
}

bool Vector3DEnabled::isDisabled() const
{
	return !_enabled;
}

void Vector3DEnabled::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
}

void Vector3DEnabled::setDisabled(bool disabled /*= true*/)
{
	_enabled = !disabled;
}

void Vector3DEnabled::set(float xyz)
{
	setX(xyz);
	setY(xyz);
	setZ(xyz);
}

void Vector3DEnabled::set(float xyz, bool enabled)
{
	set(xyz);
	_enabled = enabled;
}

void Vector3DEnabled::set(float x, float y, float z)
{
	setX(x);
	setY(y);
	setZ(z);
}

void Vector3DEnabled::set(float x, float y, float z, bool enabled)
{
	set(x, y, z);
	_enabled = enabled;
}

void Vector3DEnabled::add(float dx, float dy /*= 0.0*/, float dz /*= 0.0*/)
{
	if (dx != 0.0)
		setX( x() + dx);
	if (dy != 0.0)
		setY( y() + dy);
	if (dz != 0.0)
		setZ( z() + dz);
}

void Vector3DEnabled::multiply(float factor)
{
	*((Vector3DEnabled*)this) *= factor;
}

void Vector3DEnabled::multiply(float dx, float dy, float dz)
{
	setX( x() * dx);
	setY( y() * dy);
	setZ( z() * dz);
}

//-------------------------------------------------------------

Vector3DAngleEnabled::Vector3DAngleEnabled(bool enabled  /*= false*/) :
	Vector3DEnabled(enabled),
	_angle(0.0)
{

}

Vector3DAngleEnabled::Vector3DAngleEnabled(float x, float y, float z, float angle  /*= 0.0*/, bool enabled /*= false*/) :
	Vector3DEnabled(x, y, z, enabled),
	_angle(angle)
{

}

Vector3DAngleEnabled::Vector3DAngleEnabled(const QVector3D& vector, float angle  /*= 0.0*/, bool enabled  /*= false*/) :
	Vector3DEnabled(vector, enabled),
	_angle(angle)
{

}

Vector3DAngleEnabled::Vector3DAngleEnabled(const Vector3DEnabled& enVector, float angle  /*= 0.0*/) :
	Vector3DEnabled(enVector),
	_angle(angle)

{

}

Vector3DAngleEnabled::Vector3DAngleEnabled(const Vector3DAngleEnabled& cpy) :
	Vector3DEnabled()
{
	copy(cpy);
}

const Vector3DAngleEnabled& Vector3DAngleEnabled::operator=(const Vector3DAngleEnabled& rhs)
{
	copy(rhs);
	return *this;
}

void Vector3DAngleEnabled::copy(const Vector3DAngleEnabled& rhs)
{
	setX(rhs.x());
	setY(rhs.y());
	setZ(rhs.z());
	_enabled = rhs._enabled;
	_angle = rhs._angle;
}

float Vector3DAngleEnabled::angle() const
{
	return _angle;
}

void Vector3DAngleEnabled::setAngle(float angle)
{
	_angle = angle;
}



