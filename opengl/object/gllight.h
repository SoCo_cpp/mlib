#ifndef GLLIGHT_H
#define GLLIGHT_H

#include "opengl/object/globject.h"
#include "opengl/material/glcolor.h"

class GLLight : public GLObject
{
    Q_OBJECT
public:
    GLLight();
    GLLight(const GLLight& cpy);
    GLLight& operator=(const GLLight& rhs);
    void initLight();
    void copy(const GLLight& cpy);
    ~GLLight();

    Vector3DEnabled& position();
    const Vector3DEnabled& position() const;

    GLColor& specular();
    const GLColor& specular() const;

    GLColor& diffuse();
    const GLColor& diffuse() const;

    GLColor& ambient();
    const GLColor& ambient() const;

    virtual bool paintGL(GLView* glview);
    virtual bool resizeGL(GLView* glview, int width, int height);

protected:
    GLColor _specular;
    GLColor _diffuse;
    GLColor _ambient;

signals:

public slots:
};

#endif // GLLIGHT_H
