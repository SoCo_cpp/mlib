#include "opengl/object/gllight.h"
#include "opengl/view/glview.h"
#include "opengl/object/glscene.h"
#include "logging/debuglogger.h"

GLLight::GLLight() :
    GLObject()
{
    initLight();
}

GLLight::GLLight(const GLLight& cpy) :
    GLObject()
{
    initLight();
    copy(cpy);
}

GLLight& GLLight::operator=(const GLLight& rhs)
{
    copy(rhs);
    return *this;
}

void GLLight::initLight()
{
    // clear GLObject defaults
    _params.clear();
    _paintStates.clear();
    _paintCaps.clear();
    _params.set("materials-enabled",	QVariant(false));
    _params.set("textures-enabled",		QVariant(false));
    _params.set("orientation-enabled",	QVariant(false));
    _params.set("children-enabled",		QVariant(true));
}

void GLLight::copy(const GLLight& cpy)
{

    GLObject::copy( *(static_cast<const GLObject*>(&cpy)) );
}

GLLight::~GLLight()
{

}

Vector3DEnabled& GLLight::position()
{
    return _orientation.position();
}

const Vector3DEnabled& GLLight::position() const
{
    return _orientation.position();
}

GLColor& GLLight::specular()
{
    return _specular;
}

const GLColor& GLLight::specular() const
{
    return _specular;
}

GLColor& GLLight::diffuse()
{
    return _diffuse;
}

const GLColor& GLLight::diffuse() const
{
    return _diffuse;
}

GLColor& GLLight::ambient()
{
    return _ambient;
}

const GLColor& GLLight::ambient() const
{
    return _ambient;
}

bool GLLight::paintGL(GLView* glview)
{
    unsigned int glId;
    if (!GLObject::beginPaintGL(glview))
    {
        DebugLog(dlWarn, "GLLight::paintGL GLObject::beginPaintGL failed");
        return false;
    }
    if (!glview || !glview->scene())
        DebugLog(dlWarn, "GLLight::paintGL couldn't resolve scene");
    else
    {
        glId = glview->scene()->getLightID();
        if (glId == UINT_MAX)
            DebugLog(dlWarn, "GLLight::paintGL GLScene::getLightID failed, max lights?");
        else
        {
            glEnable(glId);
            if (_orientation.position().isEnabled())
            {
                GLfloat pos[4];
                pos[0] = _orientation.position().x();
                pos[1] = _orientation.position().y();
                pos[2] = _orientation.position().z();
                pos[3] = 0.0;
                glLightfv(glId, GL_POSITION, pos);
            }
            if (_specular.isEnabled())
                glLightfv(glId, GL_SPECULAR, _specular.color());
            if (_diffuse.isEnabled())
                glLightfv(glId, GL_DIFFUSE, _diffuse.color());
            if (_ambient.isEnabled())
                glLightfv(glId, GL_AMBIENT, _ambient.color());
        }
    }
    if (!GLObject::endPaintGL(glview))
    {
        DebugLog(dlWarn, "GLLight::paintGL GLObject::endPaintGL failed");
        return false;
    }
    return true;
}

bool GLLight::resizeGL(GLView* glview, int width, int height)
{
    Q_UNUSED(glview);
    Q_UNUSED(width);
    Q_UNUSED(height);
    return true;
}
