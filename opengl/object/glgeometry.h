#ifndef GLGEOMETRY_H
#define GLGEOMETRY_H

#include <QObject>
#include "opengl/parameter/parameterlist.h"
#include "opengl/parameter/glcaplist.h"

class GLObject; // predeclared
class GLView; // predeclared
class GLGeometry : public QObject
{
	Q_OBJECT
public:
	GLGeometry();
	GLGeometry(const GLGeometry& cpy);
	GLGeometry& operator=(const GLGeometry& rhs);
	void initSignals();
	virtual void copy(const GLGeometry& cpy);

	bool isInitted() const;

	ParameterList& params();
	const ParameterList& params() const;

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

	GLGeometry* asGeomentry();

	const GLCapList& paintStates() const;
	const GLCapList& paintCaps() const;

protected:
	bool _initted;
	bool _updatingCaps;
	ParameterList _params;
	GLCapList _paintStates; // GLenum (GL_VERTEX_ARRAY, GL_NORMAL_ARRAY, GL_TEXTURE_COORD_ARRAY)
	GLCapList _paintCaps; // GLenum (GL_DEPTH_TEST, GL_CULL_FACE)

signals:
	void changed();
	void paramsChanged();
	void paintStatesChanged();
	void paintCapsChanged();

public slots:
	void onParamsChanged();
	void onPaintStatesChanged();
	void onPaintCapsChanged();

public:
	static bool buildDisc(char axis, float axisOffset, float radius, unsigned int slices, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals = 0, QVector<QVector2D>* vlTexcoord = 0, bool half = false);
	static bool buildDisc(QVector<QVector2D>* vlCircleVerts, char axis, float axisOffset, float radius, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals = 0, QVector<QVector2D>* vlTexcoord = 0);

	static bool buildTube(char axis, float length, float radius, unsigned int slices, unsigned int stacks, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals = 0, QVector<QVector2D>* vlTexcoord = 0, bool half = false);
	static bool buildTube(QVector<QVector2D>* vlCircleVerts, char axis, float length, float radius, unsigned int stacks, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals = 0, QVector<QVector2D>* vlTexcoord = 0);
	static bool getCircle2D(QVector<QVector2D>* vlVertices, unsigned int slices, float radius = 1.0, bool reversed = false, bool half = false);


};

#endif // GLGEOMETRY_H
