#include "opengl/object/globject.h"
#include "opengl/view/glview.h"
#include "opengl/texture/texture.h"
#include "logging/debuglogger.h"
#include "opengl/object/glgeometry.h"
#include <QDebug>

GLObject::GLObject() :
	QObject(0),
	_geometry(0)
{
	init();
}

GLObject::GLObject(const GLObject& cpy) :
	QObject(0),
	_geometry(0)
{
	init();
	copy(cpy);
}

GLObject& GLObject::operator=(const GLObject& rhs)
{
	copy(rhs);
	return *this;
}

void GLObject::init()
{
	connect(&_params, SIGNAL(changed()), this, SLOT(onParamsChanged()));
	connect(&_effectiveParams, SIGNAL(changed()), this, SLOT(onEffectiveParamsChanged()));

	_params.set("materials-enabled",	QVariant(true));
	_params.set("textures-enabled",		QVariant(false));
	_params.set("orientation-enabled",	QVariant(true));
	_params.set("children-enabled",		QVariant(true));

	_paintCaps.add(GL_LIGHTING);

}

void GLObject::copy(const GLObject& cpy)
{
	int i;
	_orientation = cpy._orientation;
	_params = cpy._params;

	if (cpy._geometry)
		setGeometry(new GLGeometry(*cpy._geometry)); // makes new instance copy, initted set false on copy

	// do not copy parents!

	for (i = 0;i < cpy._textures.size();i++)
		addTexture(cpy._textures[i]); // reciprocated

	for (i = 0;i < cpy._children.size();i++)
		addChild(cpy._children[i]); // reciprocated
}

GLObject::~GLObject()
{
	setGeometry(0);
	qDebug() << "GLObject::~GLObject...children...";
	removeAllChildren();
	qDebug() << "GLObject::~GLObject...parents...";
	removeAllParents();
	qDebug() << "GLObject::~GLObject...textures...";
	removeAllTextures();
	qDebug() << "GLObject::~GLObject...Done";
}

void GLObject::setGeometry(GLGeometry* geometry)
{
	if (_geometry)
	{
		disconnect(_geometry, 0, 0, 0);
		_geometry->deleteLater();
	}
	_geometry = geometry;
	if (_geometry)
	{
		connect(_geometry, SIGNAL(changed()), this, SLOT(onGeometryChanged()));
		connect(_geometry, SIGNAL(paintStatesChanged()), this, SLOT(onGeometryPaintStatessChanged()));
		connect(_geometry, SIGNAL(paintCapsChanged()), this, SLOT(onGeometryPaintCapsChanged()));
	}
	updateEffectiveParams();
	updateEffectivePaintStates();
	updateEffectivePaintCaps();
}

bool GLObject::hasGeometry() const
{
	return (_geometry != 0);
}

GLGeometry* GLObject::geometry()
{
	return _geometry;
}

const GLGeometry* GLObject::geometry() const
{
	return _geometry;
}

ParameterList& GLObject::params()
{
	return _params;
}

const ParameterList& GLObject::params() const
{
	return _params;
}

ParameterList& GLObject::effectiveParams()
{
	return _effectiveParams;
}

const ParameterList& GLObject::effectiveParams() const
{
	return _effectiveParams;
}

bool GLObject::initGeometry(GLView*)
{
	if (_geometry && !_geometry->isInitted() && !_geometry->init(this))
	{
		DebugLog(dlWarn, "GLObject::initGeometry geometry failed to init");
		return false;
	}
	return true;
}

bool GLObject::initializeGL(GLView* glview)
{
	if (!initGeometry(glview))
	{
		DebugLog(dlWarn, "GLObject::initializeGL initGeometry failed");
		return false;
	}
	if (!_material.initializeGL(glview))
	{
		DebugLog(dlWarn, "GLObject::initializeGL material  initializeGL failed");
		return false;
	}
	for (int i = 0;i < _children.size();i++)
		if (!_children[i]->initializeGL(glview))
		{
			DebugLog(dlWarn, "GLObject::initializeGL initializeGL child [%d] failed", i);
			return false;
		}
	return true;
}

bool GLObject::paintGL(GLView* glview)
{
	if (!initGeometry(glview))
	{
		DebugLog(dlWarn, "GLObject::paintGL initGeometry failed");
		return false;
	}

	if (!beginPaintGL(glview))
	{
		DebugLog(dlWarn, "GLObject::paintGL beginPaintGL failed");
		return false;
	}

	if (_geometry && !_geometry->paint(this, glview))
	{
		DebugLog(dlWarn, "GLObject::paintGL geometry failed to paint");
		return false;
	}

	if (!endPaintGL(glview))
	{
		DebugLog(dlWarn, "GLObject::paintGL endPaintGL failed");
		return false;
	}
	return true;
}

bool GLObject::beginPaintGL(GLView* glview)
{
	int i;

	if (_effectiveParams["orientation-enabled"].toBool())
		_orientation.paintGL(glview);

	if (_effectiveParams["materials-enabled"].toBool())
		if (!_material.paintGL(glview))
		{
			DebugLog(dlWarn, "GLObject::beginPaintGL material paintGL failed");
			return false;
		}

	if (_effectiveParams["textures-enabled"].toBool())
		for (i = 0;i < _textures.size();i++)
			if (!_textures[i]->bind())
			{
				DebugLog(dlWarn, "GLObject::beginPaintGL failed to bind texture (%d) fileName: %s", i, qPrintable(_textures[i]->fileName()));
				return false;
			}
	//glFrontFace(GL_CW); opposite culling dir GL_CCW default
	//glShadeModel(GL_FLAT); //GL_SMOOTH); // GL_FLAT
	//glEnable (GL_BLEND);
	//glBlendFunc (GL_ONE, GL_ONE);

	for (i = 0;i < _effectivePaintStates.size();i++)
		glEnableClientState(_effectivePaintStates[i]);
	for (i = 0;i < _effectivePaintCaps.size();i++)
		glEnable(_effectivePaintCaps[i]);
	return true;
}

bool GLObject::endPaintGL(GLView* glview)
{
	int i;
	for (i = 0;i < _effectivePaintCaps.size();i++)
		glDisable(_effectivePaintCaps[i]);
	for (i = 0;i < _effectivePaintStates.size();i++)
		glDisableClientState(_effectivePaintStates[i]);

	if (_effectiveParams["textures-enabled"].toBool())
		for (i = 0;i < _textures.size();i++)
			_textures[i]->unbind();
	if (_effectiveParams["children-enabled"].toBool())
		for (i = 0;i < _children.size();i++)
        {
			_children[i]->paintGL(glview);
            if (_effectiveParams["orientate-each-child"].toBool() && _effectiveParams["orientation-enabled"].toBool())
            {
                glLoadIdentity();
                _orientation.paintGL(glview);
            }
        }
	return true;
}

bool GLObject::resizeGL(GLView* glview, int width, int height)
{
	for (int i = 0;i < _children.size();i++)
		if (!_children[i]->resizeGL(glview, width, height))
		{
			DebugLog(dlWarn, "GLObject::resizeGL child [%d] resizeGL failed", i);
			return false;
		}
	return true;
}

const QList<GLObject*>& GLObject::children() const
{
	return _children;
}

void GLObject::addChild(GLObject* obj, bool reciprocate /*= true*/)
{
	if (reciprocate)
		obj->addParent(this, false/*reciprocate*/);
	_children.append(obj);
}

bool GLObject::removeChild(GLObject* obj, bool reciprocate /*= true*/)
{
	if (reciprocate)
		obj->removeParent(this);
	_children.removeOne(obj);
	return true;
}

void GLObject::removeAllChildren()
{
	for (int i = 0;i < _children.size();i++)
		_children[i]->removeParent(this, false/*reciprocate*/);
	_children.clear();
}

const QList<GLObject*>& GLObject::parents() const
{
	return _parents;
}

void GLObject::addParent(GLObject* obj, bool reciprocate /*= true*/)
{
	if (reciprocate)
		obj->addChild(this, false/*reciprocate*/);
	_parents.append(obj);
}

bool GLObject::removeParent(GLObject* obj, bool reciprocate /*= true*/)
{
	if (reciprocate)
		obj->removeChild(this);
	_parents.removeOne(obj);
	return true;
}


void GLObject::removeAllParents()
{
	for (int i = 0;i < _parents.size();i++)
		_parents[i]->removeChild(this, false/*reciprocate*/);
	_parents.clear();

}

const QList<Texture*>& GLObject::textures() const
{
	return _textures;
}

void GLObject::addTexture(Texture* texture, bool reciprocate /*= true*/)
{
	if (reciprocate)
		texture->addObject(this, false/*reciprocate*/);
	_textures.append(texture);
	_params.set("textures-enabled",	QVariant(true));
}

bool GLObject::removeTexture(Texture* texture, bool reciprocate /*= true*/)
{
	if (reciprocate)
		texture->removeObject(this);
	_textures.removeOne(texture);
	if (!_textures.size())
		_params.set("textures-enabled",	QVariant(false));
	return true;
}

void GLObject::removeAllTextures()
{
	for (int i = 0;i < _textures.size();i++)
		_textures[i]->removeObject(this, false/*reciprocate*/);
	_textures.clear();
	_params.set("textures-enabled",	QVariant(false));
}

Orientation3D& GLObject::orientation()
{
	return _orientation;
}

const Orientation3D& GLObject::orientation() const
{
	return _orientation;
}

GLMaterial& GLObject::material()
{
	return _material;
}

const GLMaterial& GLObject::material() const
{
	return _material;
}

GLCapList& GLObject::paintStates()
{
	return _paintStates;
}

const GLCapList& GLObject::paintStates() const
{
	return _paintStates;
}

GLCapList& GLObject::paintCaps()
{
	return _paintCaps;
}

const GLCapList& GLObject::paintCaps() const
{
	return _paintCaps;
}

void GLObject::onGeometryChanged()
{
	emit changed();
}

void GLObject::onGeometryPaintStatessChanged()
{
	updateEffectivePaintStates();
}

void GLObject::onGeometryPaintCapsChanged()
{
	updateEffectivePaintCaps();
}

void GLObject::onParamsChanged()
{
	updateEffectiveParams();
	emit paramsChanged();
	emit changed();
}

void GLObject::onEffectiveParamsChanged()
{
	emit effectiveParamsChanged();
	emit changed();
}

void GLObject::updateEffectiveParams()
{
	if (_geometry)
		_effectiveParams = _geometry->params() + _params;
	else
		_effectiveParams = _params;
}

void GLObject::updateEffectivePaintStates()
{
	if (_geometry)
		_effectivePaintStates = _geometry->paintStates() + _paintStates;
	else
		_effectivePaintStates = _paintStates;
	emit changed();
}

void GLObject::updateEffectivePaintCaps()
{
	if (_geometry)
		_effectivePaintCaps = _geometry->paintCaps() + _paintCaps;
	else
		_effectivePaintCaps = _paintCaps;
	emit changed();
}
