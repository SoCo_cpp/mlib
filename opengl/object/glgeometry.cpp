#include "opengl/object/glgeometry.h"
#include "math/circleconsts.h"
#include "logging/debuglogger.h"
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QtMath>

GLGeometry::GLGeometry() :
	QObject(0),
	_initted(false),
	_updatingCaps(false)
{
	initSignals();
	_params.set("GEO_TYPE", QVariant("BaseGeometry"));
}

GLGeometry::GLGeometry(const GLGeometry& cpy) :
	QObject(0)
{
	initSignals();
	copy(cpy);
}

GLGeometry& GLGeometry::operator=(const GLGeometry& rhs)
{
	copy(rhs);
	return *this;
}

void GLGeometry::initSignals()
{
	connect(&_params, SIGNAL(changed()), this, SLOT(onParamsChanged()));
	connect(&_paintStates, SIGNAL(changed()), this, SLOT(onPaintStatesChanged()));
	connect(&_paintCaps, SIGNAL(changed()), this, SLOT(onPaintCapsChanged()));
}

void GLGeometry::copy(const GLGeometry& cpy)
{
	_params = cpy._params;
	_initted = false;
	//_paintStates and _paintCaps should be rebuilt on init
	emit changed();
}

bool GLGeometry::isInitted() const
{
	return _initted;
}

ParameterList& GLGeometry::params()
{
	return _params;
}

const ParameterList& GLGeometry::params() const
{
	return _params;
}

bool GLGeometry::init(GLObject*)
{
	// blank, base geometry
	_initted = true;
	return true;
}

bool GLGeometry::paint(GLObject*, GLView*)
{
	// blank, base geometry
	return true;
}

GLGeometry* GLGeometry::asGeomentry()
{
	return dynamic_cast<GLGeometry*>(this);
}

const GLCapList& GLGeometry::paintStates() const
{
	return _paintStates;
}

const GLCapList& GLGeometry::paintCaps() const
{
	return _paintCaps;
}

void GLGeometry::onParamsChanged()
{
	_initted = false;
	emit paramsChanged();
	emit changed();
}

void GLGeometry::onPaintStatesChanged()
{
	emit paintStatesChanged();
	emit changed();
}

void GLGeometry::onPaintCapsChanged()
{
	emit paintCapsChanged();
	emit changed();
}

//---------------------------------------------------------------------------------------------------

/*static*/
bool GLGeometry::buildDisc(char axis, float axisOffset, float radius, unsigned int slices, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals /*= 0*/, QVector<QVector2D>* vlTexcoord /*= 0*/, bool half /*= false*/)
{
	Q_ASSERT(vlVertices);
	QVector<QVector2D> circleVerts;
	if (!getCircle2D(&circleVerts, slices, 1.0 /*radius*/, false/*reversed*/, half))
	{
		DebugLog(dlWarn, "GLGeometry::buildDisc getCircle2D failed");
		return false;
	}
	return buildDisc(&circleVerts, axis, axisOffset, radius, vlVertices, vlNormals, vlTexcoord);
}

/*static*/
bool GLGeometry::buildDisc(QVector<QVector2D>* vlCircleVerts, char axis, float axisOffset, float radius, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals /*= 0*/, QVector<QVector2D>* vlTexcoord /*= 0*/)
{
	Q_ASSERT(vlVertices);
	int cv;
	float axisDirection;
	QVector3D normal;
	if (vlCircleVerts->size() == 0)
	{
		DebugLog(dlWarn, "GLGeometry::buildDisc circle verticies list is empty");
		return false;
	}
	switch (axis)
	{
		case 'x': //  <-O
		case 'X': //  O->
			axisDirection = (axis == 'x' ? -1.0 : 1.0);
			normal = QVector3D( axisDirection, 0.0, 0.0 );
			for (cv = 0;cv < vlCircleVerts->size();cv++)
			{
				vlVertices->append( QVector3D(axisOffset, ((*vlCircleVerts)[cv].y() * radius), (axisDirection * (*vlCircleVerts)[cv].x() * radius) ));
				if (vlNormals)
					vlNormals->append(normal);
				if (vlTexcoord)
					vlTexcoord->append( QVector2D( (axisDirection * (*vlCircleVerts)[cv].x()),  (*vlCircleVerts)[cv].y()) );
			}
			break;
		case 'y': // T
		case 'Y': // _|_
			axisDirection = (axis == 'y' ? -1.0 : 1.0);
			normal = QVector3D( 0.0, axisDirection, 0.0 );
			for (cv = 0;cv < vlCircleVerts->size();cv++)
			{
				vlVertices->append( QVector3D(((*vlCircleVerts)[cv].x() * radius), axisOffset, (axisDirection * (*vlCircleVerts)[cv].y() * radius) ));
				if (vlNormals)
					vlNormals->append(normal);
				if (vlTexcoord)
					vlTexcoord->append( QVector2D( (*vlCircleVerts)[cv].x(), (axisDirection * (*vlCircleVerts)[cv].y()) ) );
			}
			break;
		case 'z': //   o/
		case 'Z': //  /o
			axisDirection = (axis == 'z' ? -1.0 : 1.0);
			normal = QVector3D( 0.0, 0.0, axisDirection );
			for (cv = 0;cv < vlCircleVerts->size();cv++)
			{
				vlVertices->append( QVector3D( (axisDirection * (*vlCircleVerts)[cv].x() * radius), ((*vlCircleVerts)[cv].y() * radius), axisOffset ));
				if (vlNormals)
					vlNormals->append(normal);
				if (vlTexcoord)
					vlTexcoord->append( QVector2D( (axisDirection * (*vlCircleVerts)[cv].x()),  (*vlCircleVerts)[cv].y()) );
			}
			break;
		default:
			DebugLog(dlWarn, "GLGeometry::buildDisc axis '%c' invalid. x, y, or z for negative and X, Y, Z for positive");
			return false;
	} // switch (axis)
	return true;
}

/*static*/
bool GLGeometry::buildTube(char axis, float length, float radius, unsigned int slices, unsigned int stacks, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals /*= 0*/, QVector<QVector2D>* vlTexcoord /*= 0*/, bool half /*= false*/)
{
	Q_ASSERT(vlVertices);
	QVector<QVector2D> circleVerts;
	if (getCircle2D(&circleVerts, slices, 1.0 /*radius*/, false/*reversed*/, half))
	{
		DebugLog(dlWarn, "GLGeometry::buildTube getCircle2D failed");
		return false;
	}
	return buildTube(&circleVerts, axis, length, radius, stacks, vlVertices, vlNormals, vlTexcoord);
}

/*static*/
bool GLGeometry::buildTube(QVector<QVector2D>* vlCircleVerts, char axis, float length, float radius, unsigned int stacks, QVector<QVector3D>* vlVertices, QVector<QVector3D>* vlNormals /*= 0*/, QVector<QVector2D>* vlTexcoord /*= 0*/)
{
	Q_ASSERT(vlVertices);
	if (vlCircleVerts->size() == 0)
	{
		DebugLog(dlWarn, "GLGeometry::buildTube circle verticies list is empty");
		return false;
	}
	unsigned int s, cv;
	float fStacks = (float)stacks;
	float fCircleVerts = (float)vlCircleVerts->size();
	float lengthStep = length / fStacks;
	float axisDirection;
	QVector3D A, B, L, N;
	length = 0.0;
	//stacks--;
	switch (axis)
	{
		case 'x': //  <-O
		case 'X': //  O->
			axisDirection = (axis == 'x' ? -1.0 : 1.0);
			lengthStep *= axisDirection;
			L = QVector3D((lengthStep * ((float)stacks-1)), ((*vlCircleVerts)[vlCircleVerts->size()-1].y() * radius), (axisDirection * (*vlCircleVerts)[vlCircleVerts->size()-1].x() * radius));
			for (s = 0;s < stacks;s++, length += lengthStep)
				for (cv = 0;cv < (unsigned int)vlCircleVerts->size();cv++)
				{
					A = QVector3D(length, ((*vlCircleVerts)[cv].y() * radius), (axisDirection * (*vlCircleVerts)[cv].x() * radius));
					B = QVector3D(length + lengthStep, ((*vlCircleVerts)[cv].y() * radius), (axisDirection * (*vlCircleVerts)[cv].x() * radius));
					N = QVector3D::normal(A, L, B);
					L = B;
					vlVertices->append( B );
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s + 1)/fStacks) ,  (((float)cv)/fCircleVerts) ));
					vlVertices->append( A );
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s)/fStacks) ,  (((float)cv)/fCircleVerts) ));
				}
			break;
		case 'y': // T
		case 'Y': // _|_
			axisDirection = (axis == 'y' ? -1.0 : 1.0);
			lengthStep *= axisDirection;
			L = QVector3D( ((*vlCircleVerts)[vlCircleVerts->size()-1].x() * radius), (lengthStep * ((float)stacks-1)), (axisDirection * (*vlCircleVerts)[vlCircleVerts->size()-1].y() * radius) );
			for (s = 0;s < stacks;s++, length += lengthStep)
				for (cv = 0;cv < (unsigned int)vlCircleVerts->size();cv++)
				{
					A = QVector3D( ((*vlCircleVerts)[cv].x() * radius), length, (axisDirection * (*vlCircleVerts)[cv].y() * radius) );
					B = QVector3D( ((*vlCircleVerts)[cv].x() * radius), length + lengthStep, (axisDirection * (*vlCircleVerts)[cv].y() * radius) );
					N = QVector3D::normal(A, L, B);
					L = B;
					vlVertices->append( B );
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s + 1)/fStacks) ,  (((float)cv)/fCircleVerts) ));
					vlVertices->append( A );
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s)/fStacks) ,  (((float)cv)/fCircleVerts) ));
				}
			break;
		case 'z': //   o/
		case 'Z': //  /o
			axisDirection = (axis == 'z' ? -1.0 : 1.0);
			lengthStep *= axisDirection;
			L = QVector3D( (axisDirection * (*vlCircleVerts)[vlCircleVerts->size()-1].x() * radius), ((*vlCircleVerts)[vlCircleVerts->size()-1].y() * radius), (lengthStep * ((float)stacks - 1)) );
			for (s = 0;s < stacks;s++, length += lengthStep)
				for (cv = 0;cv < (unsigned int)vlCircleVerts->size();cv++)
				{
					A = QVector3D( (axisDirection * (*vlCircleVerts)[cv].x() * radius), ((*vlCircleVerts)[cv].y() * radius), length );
					B = QVector3D( (axisDirection * (*vlCircleVerts)[cv].x() * radius), ((*vlCircleVerts)[cv].y() * radius), length + lengthStep );
					N = QVector3D::normal(A, L, B);
					L = B;
					vlVertices->append(B );
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s + 1)/fStacks) ,  (((float)cv)/fCircleVerts) ));
					vlVertices->append(A);
					if (vlNormals)
						vlNormals->append( N );
					if (vlTexcoord)
						vlTexcoord->append( QVector2D( (((float)s)/fStacks) ,  (((float)cv)/fCircleVerts) ));
				}
			break;
		default:
			DebugLog(dlWarn, "GLGeometry::buildTube axis '%c' invalid. x, y, or z for negative and X, Y, Z for positive");
			return false;
	} // switch (axis)
	return true;
}

bool GLGeometry::getCircle2D(QVector<QVector2D>* vlVertices, unsigned int slices, float radius /*= 1.0*/, bool reversed /*= false*/, bool half /*= false*/)
{
	Q_ASSERT(vlVertices);
	if (slices == 0)
	{
		DebugLog(dlWarn, "GLGeometry::getCircle2D slices is invalid, zero");
		return false;
	}
	float angle;
	float angleStep = MLibMath::pi / slices;
	if (!half)
		angleStep *= 2.0;
	if (reversed)
		angleStep *= -1.0;
	angle = angleStep;
	if (radius == 1.0)
	{
		vlVertices->append( QVector2D(1.0, 0.0) );
		for (unsigned int s = 1;s < slices;s++, angle += angleStep)
			vlVertices->append( QVector2D( qCos(angle), qSin(angle) ) );
		if (half)
			vlVertices->append( QVector2D(-1.0, 0.0) );
		else
			vlVertices->append( QVector2D(1.0, 0.0) );
	}
	else
	{
		vlVertices->append( QVector2D(radius, 0.0) );
		for (unsigned int s = 1;s < slices;s++, angle += angleStep)
			vlVertices->append( QVector2D( (qCos(angle) * radius), (qSin(angle) * radius) ) );
		if (half)
			vlVertices->append( QVector2D(radius, 0.0) );
		else
			vlVertices->append( QVector2D(radius, 0.0) );
	}
	return true;
}
