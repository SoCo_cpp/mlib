#ifndef GLOBJECT_H
#define GLOBJECT_H

#include <QObject>
#include <QList>
#include "opengl/orientation/orientation3d.h"
#include "opengl/parameter/parameterlist.h"
#include "opengl/parameter/glcaplist.h"
#include "opengl/material/glmaterial.h"

class GLGeometry; // predeclared
class GLView; // predeclared
class Texture; // predeclared
class GLObject : public QObject
{
	Q_OBJECT
public:
	GLObject();
	GLObject(const GLObject& cpy);
	GLObject& operator=(const GLObject& rhs);
	void init();
	void copy(const GLObject& cpy);
	~GLObject();

	void setGeometry(GLGeometry* geometry);
	bool hasGeometry() const;
	GLGeometry* geometry();
	const GLGeometry* geometry() const;

	ParameterList& params();
	const ParameterList& params() const;

	ParameterList& effectiveParams();
	const ParameterList& effectiveParams() const;

	virtual bool initGeometry(GLView* glview);

	virtual bool initializeGL(GLView* glview);
	virtual bool paintGL(GLView* glview);
	virtual bool beginPaintGL(GLView* glview);
	virtual bool endPaintGL(GLView* glview);
	virtual bool resizeGL(GLView* glview, int width, int height);

	const QList<GLObject*>& children() const;
	void addChild(GLObject* obj, bool reciprocate = true);
	bool removeChild(GLObject* obj, bool reciprocate = true);
	void removeAllChildren();

	const QList<GLObject*>& parents() const;
	void addParent(GLObject* obj, bool reciprocate = true);
	bool removeParent(GLObject* obj, bool reciprocate = true);
	void removeAllParents();

	const QList<Texture*>& textures() const;
	void addTexture(Texture* texture, bool reciprocate = true);
	bool removeTexture(Texture* texture, bool reciprocate = true);
	void removeAllTextures();

	Orientation3D& orientation();
	const Orientation3D& orientation() const;

	GLMaterial& material();
	const GLMaterial& material() const;

	GLCapList& paintStates();
	const GLCapList& paintStates() const;

	GLCapList& paintCaps();
	const GLCapList& paintCaps() const;

	void updateEffectiveParams();
	void updateEffectivePaintStates();
	void updateEffectivePaintCaps();

protected:
	GLGeometry* _geometry;
	ParameterList _params;
	ParameterList _effectiveParams;
	GLCapList _paintStates; // GLenum (GL_VERTEX_ARRAY, GL_NORMAL_ARRAY, GL_TEXTURE_COORD_ARRAY)
	GLCapList _effectivePaintStates;
	GLCapList _paintCaps; // GLenum (GL_DEPTH_TEST, GL_CULL_FACE)
	GLCapList _effectivePaintCaps;

	QList<GLObject*> _children;
	QList<GLObject*> _parents;
	QList<Texture*> _textures;
	Orientation3D _orientation;
	GLMaterial _material;

signals:
	void changed();
	void paramsChanged();
	void effectiveParamsChanged();

public slots:
	void onGeometryChanged();
	void onGeometryPaintStatessChanged();
	void onGeometryPaintCapsChanged();
	void onParamsChanged();
	void onEffectiveParamsChanged();
};

#endif // GLOBJECT_H
