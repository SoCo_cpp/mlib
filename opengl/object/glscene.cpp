
#include "opengl/view/glview.h"
#include "opengl/object/glscene.h"
#include "opengl/object/gllight.h"
#include "logging/debuglogger.h"
#include <QDebug>

GLScene::GLScene() :
    GLObject(),
    _lightId(0)
{
    _params.set("orientate-each-child",	QVariant(true));
}

GLScene::GLScene(const GLScene& cpy) :
	GLObject()
{
    copy(cpy);
}

GLScene& GLScene::operator=(const GLScene& rhs)
{
	copy(rhs);
	return *this;
}

void GLScene::copy(const GLScene& cpy)
{
	// don't copy veiws as they are parents
	_backgroundColor = cpy._backgroundColor;
    _lightId = 0;
    GLObject::copy( *(static_cast<const GLObject*>(&cpy)) );
}

GLScene::~GLScene()
{
	qDebug() << "GLScene::~GLScene...";
	removeAllViews();
	qDebug() << "GLScene::~GLScene...DONE";
}

const QList<GLView*>& GLScene::views() const
{
    return _views;
}

void GLScene::addView(GLView* view, bool reciprocate /*= true*/)
{
    if (reciprocate)
		view->setScene(this, true/*removePrevious*/, false/*reciprocate*/);
	_views.append(view);
}

bool GLScene::removeView(GLView* view, bool reciprocate /*= true*/)
{
	if (reciprocate)
		view->unsetScene(this, false/*reciprocate*/);
    return _views.removeOne(view);
}

void GLScene::removeAllViews()
{
	for (int i = 0;i < _views.size();i++)
		_views[i]->unsetScene(this, false/*reciprocate*/);
	_views.clear();
}

unsigned int GLScene::getLightID(bool increment /*= true*/)
{
    if (_lightId >= GL_MAX_LIGHTS)
    {
        DebugLog(dlInfo, "GLScene::getLightID at max lights. Start ID %d, current ID %d, max ID %d", GL_LIGHT0, _lightId, GL_MAX_LIGHTS);
        return UINT_MAX;
    }
    if (increment)
        return GL_LIGHT0 + _lightId++;
    return GL_LIGHT0 + _lightId;
}

bool GLScene::paintGL(GLView* glview)
{
    _lightId = 0;
	glLoadIdentity();
    if (!GLObject::beginPaintGL(glview))
	{
		DebugLog(dlWarn, "GLScene::paintGL GLObject::beginPaintGL failed");
		return false;
	}

	glClearColor(_backgroundColor.redF(), _backgroundColor.greenF(), _backgroundColor.blueF(), _backgroundColor.alphaF());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // wireframe
	//glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	glEnable(GL_LIGHTING);

    if (!GLObject::endPaintGL(glview))
	{
		DebugLog(dlWarn, "GLScene::paintGL GLObject::endPaintGL failed");
		return false;
    }
    return true;
}

bool GLScene::resizeGL(GLView* glview, int width, int height)
{
	Q_UNUSED(glview)
	glViewport(0,0,width, height);
    glMatrixMode(GL_PROJECTION);
    //glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glOrtho(-0.5, +0.5, +0.5, -0.5, 4.0, 15.0);
    //glOrtho(-1.0, +1.0, -1.0, +1.0, -90.0, +90.0);
    //glFrustum(-1.0, +1.0, -1.0, +1.0, 0.0, 20.0);
    //glDepthRange(0.0, 1.0);
    // glFrustrum note: both nearZ/farZ must be non-zero positive, they corrispond to negative Z values
    glFrustum(-1.0, +1.0, -1.0, +1.0, 1.0, 200.0);
    //glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0 , 50.0);
    glMatrixMode(GL_MODELVIEW);

   /*
  glViewport (0, 0, (GLsizei) width, (GLsizei) height);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity();
   if (width <= height)
      glOrtho (-1.5, 1.5, -1.5*(GLfloat)height/(GLfloat)width,
         1.5*(GLfloat)height/(GLfloat)width, -10.0, 10.0);
   else
      glOrtho (-1.5*(GLfloat)width/(GLfloat)height,
         1.5*(GLfloat)width/(GLfloat)height, -1.5, 1.5, -10.0, 10.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   */
	return true;
}
