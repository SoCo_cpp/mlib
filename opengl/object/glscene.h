#ifndef GLSCENE_H
#define GLSCENE_H

#include <QColor>
#include "opengl/object/globject.h"

class GLView; // predeclared
class GLScene : public GLObject
{
	Q_OBJECT
public:
	GLScene();
	GLScene(const GLScene& cpy);
	GLScene& operator=(const GLScene& rhs);
	void copy(const GLScene& cpy);
	~GLScene();

    const QList<GLView*>& views() const;
	void addView(GLView* view, bool reciprocate = true);
	bool removeView(GLView* view, bool reciprocate = true);
	void removeAllViews();

    unsigned int getLightID(bool increment = true); // UINT_MAX on max lights

	virtual bool paintGL(GLView* glview);
	virtual bool resizeGL(GLView* glview, int width, int height);

protected:
    unsigned int _lightId;
    QColor _backgroundColor;
    QList<GLView*> _views;


signals:

public slots:

};

#endif // GLSCENE_H
