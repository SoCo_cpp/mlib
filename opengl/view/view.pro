
!defined(MLIB_SRC,var):error("MLIB opengl view (mlib/opengl/view/view.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/opengl/view

message("MLIB including opengl view...")

QT *= gui

SOURCES += $$MSRC/glview.cpp
HEADERS += $$MSRC/glview.h
