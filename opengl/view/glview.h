#ifndef GLVIEW_H
#define GLVIEW_H

#include <QGLWidget>
#include <QObject>
#include <QGLFormat>
#include <QList>
#include "ui/mouse_tracking/mousetracker.h"

class GLScene; // predeclared
class GLView : public QGLWidget
{
	Q_OBJECT
public:
	explicit GLView(QWidget* parent = 0, const QGLFormat& glFmt = QGLFormat());
	~GLView();

    GLScene* scene();
    const GLScene* scene() const;
	bool hasScene() const;
	void setScene(GLScene* scene, bool removePrevious = true, bool reciprocate = true, bool reciprocateRemove = true);
	bool unsetScene(GLScene* scene = 0, bool reciprocate = true);

    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void mouseDoubleClickEvent(QMouseEvent* e);
    void wheelEvent(QWheelEvent* e) ;

protected:
	GLScene* _pscene;
    MouseTracker _mouseTracker;

	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);

signals:

    void mouseMove(const MouseButtonStatus* s);
    void mouseDrageBegin(const MouseButtonStatus* s);
    void mouseDrageMove(const MouseButtonStatus* s);
    void mouseDrageEnd(const MouseButtonStatus* s);
    void mouseClicked(const MouseButtonStatus* s);
    void mouseDblClicked(const MouseButtonStatus* s);
    void mouseWheel(const MouseButtonStatus* s);

public slots:

};

#endif // GLVIEW_H
