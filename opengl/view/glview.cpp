#include "opengl/view/glview.h"
#include "opengl/object/glscene.h"
#include "logging/debuglogger.h"
#include <QDebug>
GLView::GLView(QWidget* parent /*= 0*/, const QGLFormat& glFmt /*= QGLFormat()*/) :
	  QGLWidget(glFmt, parent),
      _pscene(0)
{
    connect(&_mouseTracker, SIGNAL(mouseMove(const MouseButtonStatus*)), this, SIGNAL(mouseMove(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseDrageBegin(const MouseButtonStatus*)), this, SIGNAL(mouseDrageBegin(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseDrageMove(const MouseButtonStatus*)), this, SIGNAL(mouseDrageMove(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseDrageEnd(const MouseButtonStatus*)), this, SIGNAL(mouseDrageEnd(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseClicked(const MouseButtonStatus*)), this, SIGNAL(mouseClicked(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseDblClicked(const MouseButtonStatus*)), this, SIGNAL(mouseDblClicked(const MouseButtonStatus*)));
    connect(&_mouseTracker, SIGNAL(mouseWheel(const MouseButtonStatus*)), this, SIGNAL(mouseWheel(const MouseButtonStatus*)));
}

GLView::~GLView()
{
	qDebug() << "GLView::~GLView...";
	unsetScene();
	qDebug() << "GLView::~GLView...DONE";
}

GLScene* GLView::scene()
{
    return _pscene;
}

const GLScene* GLView::scene() const
{
	return _pscene;
}

bool GLView::hasScene() const
{
	return (_pscene);
}

void GLView::setScene(GLScene* scene, bool removePrevious /*= true*/, bool reciprocate /*= true*/, bool reciprocateRemove /*= true*/)
{
	if (_pscene && removePrevious)
		unsetScene(0, reciprocateRemove);
	if (reciprocate)
		scene->addView(this, false/*reciprocate*/);
	_pscene = scene;
}

bool GLView::unsetScene(GLScene* scene /*= 0*/, bool reciprocate /*= true*/)
{
	if (scene && scene != _pscene ) // unset only specific scene
		return false;
	if (_pscene && reciprocate)
		_pscene->removeView(this, false/*reciprocate*/);
	_pscene = 0;
	return true;
}

void GLView::initializeGL()
{
    if (_pscene && !_pscene->initializeGL(this))
		DebugLog(dlWarn, "GLView::initializeGL scene initializeGL failed");
}

void GLView::paintGL()
{
    if (_pscene && !_pscene->paintGL(this))
		DebugLog(dlWarn, "GLView::initializeGL scene initializeGL failed");
}

void GLView::resizeGL(int width, int height)
{
    if (_pscene && !_pscene->resizeGL(this, width, height))
        DebugLog(dlWarn, "GLView::resizeGL scene resizeGL failed");
}

void GLView::mouseMoveEvent(QMouseEvent* e)
{
    _mouseTracker.mouseMove(e);
}

void GLView::mousePressEvent(QMouseEvent* e)
{
    _mouseTracker.mousePress(e);
}

void GLView::mouseReleaseEvent(QMouseEvent* e)
{
    _mouseTracker.mouseRelease(e);
}

void GLView::mouseDoubleClickEvent(QMouseEvent* e)
{
    _mouseTracker.mouseDblClick(e);
}

void GLView::wheelEvent(QWheelEvent* e)
{
    _mouseTracker.mouseWheel(e);
}

