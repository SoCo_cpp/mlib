
!defined(MLIB_SRC,var):error("MLIB opengl (mlib/opengl/opengl.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/opengl

message("MLIB including opengl...")

QT *= opengl

SOURCES +=  $$MSRC/object/globject.cpp \
            $$MSRC/object/glgeometry.cpp \
            $$MSRC/object/glscene.cpp \
            $$MSRC/object/gllight.cpp


HEADERS +=  $$MSRC/object/globject.h \
            $$MSRC/object/glgeometry.h \
            $$MSRC/object/glscene.h \
            $$MSRC/object/gllight.h


SOURCES +=  $$MSRC/orientation/vector3denabled.cpp \
            $$MSRC/orientation/orientation3d.cpp \
            $$MSRC/texture/textureimage.cpp \
            $$MSRC/texture/texture.cpp \
            $$MSRC/material/glmaterial.cpp \
            $$MSRC/material/glcolor.cpp \
            $$MSRC/parameter/parameterlist.cpp \
            $$MSRC/parameter/glcaplist.cpp


HEADERS +=  $$MSRC/orientation/vector3denabled.h \
            $$MSRC/orientation/orientation3d.h \
            $$MSRC/texture/textureimage.h \
            $$MSRC/texture/texture.h \
            $$MSRC/material/glmaterial.h \
            $$MSRC/material/glcolor.h \
            $$MSRC/parameter/parameterlist.h \
            $$MSRC/parameter/glcaplist.h
