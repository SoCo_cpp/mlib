#ifndef GEODISC_H
#define GEODISC_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include "opengl/object/glgeometry.h"

class GEODisc : public GLGeometry
{
	Q_OBJECT
public:
	GEODisc();

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

protected:
	QVector<QVector3D> _vertices;
	QVector<QVector3D> _normals;
	QVector<QVector2D> _texCoord;

signals:

public slots:

};

#endif // GEODISC_H
