#include "opengl/geometry/geodisc.h"
#include "logging/debuglogger.h"
#include "opengl/object/globject.h"
#include "opengl/view/glview.h"

GEODisc::GEODisc() :
	GLGeometry()
{
	_params.set("GEO_TYPE", QVariant("Disc"));
	_params.set("axis", QVariant('Z'));
	_params.set("axis-offset", QVariant(0.0));
	_params.set("radius", QVariant(1.0));
	_params.set("slices", QVariant(10));
}

bool GEODisc::init(GLObject*)
{
	char axis		 = _params["axis"].toChar().toLatin1();
	float axisOffset = _params["axis-offset"].toFloat();
	float radius	 = _params["radius"].toFloat();
	int slices		 = _params["slices"].toInt();

	_vertices.clear();
	_normals.clear();
	_texCoord.clear();

	if (!GLGeometry::buildDisc(axis, axisOffset, radius, slices, &_vertices, &_normals, &_texCoord))
	{
		DebugLog(dlWarn, "GEODisc::init GLGeometry::buildDisc failed");
		return false;
	}

	_paintStates.beginUpdate();
	_paintStates.clear();
	_paintStates.add(GL_VERTEX_ARRAY);
	_paintStates.add(GL_NORMAL_ARRAY);
	_paintStates.add(GL_TEXTURE_COORD_ARRAY);
	_paintStates.endUpdate();

	_paintCaps.beginUpdate();
	_paintCaps.clear();
	//_paintCaps.add(GL_DEPTH_TEST);
	//_paintCaps.add(GL_CULL_FACE);
	_paintCaps.endUpdate();

	_initted = true;
	return true;
}

bool GEODisc::paint(GLObject* , GLView*)
{

	glVertexPointer(3, GL_FLOAT, 0, _vertices.constData());
	glNormalPointer(GL_FLOAT, 0, _normals.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord.constData());

	glDrawArrays(GL_TRIANGLE_FAN, 0, _vertices.size());

	return true;
}

