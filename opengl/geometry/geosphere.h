#ifndef GEOSPHERE_H
#define GEOSPHERE_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include "opengl/object/glgeometry.h"

class GEOSphere : public GLGeometry
{
	Q_OBJECT
public:
	GEOSphere();

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

protected:
	QVector<QVector3D> _vertices;
	QVector<QVector2D> _texCoord;
	QVector<QVector3D> _normals;

signals:

public slots:

};

#endif // GEOSPHERE_H
