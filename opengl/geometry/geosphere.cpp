#include <QtMath>
#include "opengl/view/glview.h"
#include "opengl/geometry/geosphere.h"
#include "math/circleconsts.h"
#include "logging/debuglogger.h"
#include "opengl/object/globject.h"

GEOSphere::GEOSphere() :
	GLGeometry()
{
	_params.set("GEO_TYPE", QVariant("Sphere"));
	_params.set("radius", QVariant(1.0));
	_params.set("slices", QVariant(10));
	_params.set("stacks", QVariant(10));
}

bool GEOSphere::init(GLObject*)
{
	int x, y;
	float radius	= _params["radius"].toFloat();
	int slices		= _params["slices"].toInt();
	int stacks		= _params["stacks"].toInt();
	float latInc = 180.0 / stacks;
	float lonInc = 360.0 / slices;
	float phi1, phi2, theta1, theta2;
	QVector3D U, V, W, N;

	_vertices.clear();
	_normals.clear();
	_texCoord.clear();

	for (y = 0;y < stacks;y++)
	{
		phi1 = (y * lonInc) * MLibMath::degrad;
		phi2 = ((y + 1) * lonInc) * MLibMath::degrad;
		for (x = 0;x < slices;x++)
		{
			theta1 = (-90.0 + (x * latInc)) * MLibMath::degrad;
			theta2 = (-90.0 + ((x + 1) * latInc)) * MLibMath::degrad;

			U.setX( (float)(radius * qCos(phi1) * qCos(theta1)) );
			U.setY( (float)(radius * qSin(theta1)) );
			U.setZ( (float)(radius * qSin(phi1) * qCos(theta1)) );

			V.setX( (float)(radius * qCos(phi1) * qCos(theta2)) );
			V.setY( (float)(radius * qSin(theta2)) );
			V.setZ( (float)(radius * qSin(phi1) * qCos(theta2)) );

			W.setX( (float)(radius * qCos(phi2) * qCos(theta2)) );
			W.setY( (float)(radius * qSin(theta2)) );
			W.setZ( (float)(radius * qSin(phi2) * qCos(theta2)) );

			N = QVector3D::normal(V - U, W - U);

			_vertices.append(U);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi1 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta1 + MLibMath::pi2)) / 180.0) );

			_vertices.append(V);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi1 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta2 + MLibMath::pi2)) / 180.0) );

			_vertices.append(W);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi2 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta2 + MLibMath::pi2)) / 180.0) );

			V.setX( (float)(radius * qCos(phi2) * qCos(theta1)) );
			V.setY( (float)(radius * qSin(theta1)) );
			V.setZ( (float)(radius * qSin(phi2) * qCos(theta1)) );

			_vertices.append(U);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi1 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta1 + MLibMath::pi2)) / 180.0) );

			_vertices.append(W);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi2 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta2 + MLibMath::pi2)) / 180.0) );

			_vertices.append(V);
			_normals.append(N);
			_texCoord.append(QVector2D( (180.0 - (phi2 * MLibMath::raddeg)) / 360.0, (MLibMath::raddeg * (theta1 + MLibMath::pi2)) / 180.0) );
		} // for (x = 0;x < slices;x++)
	} // for (y = 0;y < stacks;y++)

	_paintStates.beginUpdate();
	_paintStates.clear();
	_paintStates.add(GL_VERTEX_ARRAY);
	_paintStates.add(GL_NORMAL_ARRAY);
	_paintStates.add(GL_TEXTURE_COORD_ARRAY);
	_paintStates.endUpdate();

	_paintCaps.beginUpdate();
	_paintCaps.clear();
	_paintCaps.add(GL_DEPTH_TEST);
	_paintCaps.add(GL_CULL_FACE);
	_paintCaps.endUpdate();

	_initted = true;
	return true;
}

bool GEOSphere::paint(GLObject* , GLView* )
{
	glVertexPointer(3, GL_FLOAT, 0, _vertices.constData());
	glNormalPointer(GL_FLOAT, 0, _normals.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord.constData());

	glDrawArrays(GL_TRIANGLES, 0, _vertices.size());

	return true;
}
