#ifndef GEOCYLINDER_H
#define GEOCYLINDER_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include "opengl/object/glgeometry.h"

class GEOCylinder : public GLGeometry
{
	Q_OBJECT
public:
	GEOCylinder();

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

protected:
	QVector<QVector3D> _capVertices;
	QVector<QVector3D> _capNormals;
	QVector<QVector2D> _capTexCoord;

	QVector<QVector3D> _coreVertices;
	QVector<QVector3D> _coreNormals;
	QVector<QVector2D> _coreTexCoord;

signals:

public slots:

};

#endif // GEOCYLINDER_H
