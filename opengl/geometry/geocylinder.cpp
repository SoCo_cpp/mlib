#include <QtMath>
#include "opengl/geometry/geocylinder.h"
#include "opengl/view/glview.h"
#include "logging/debuglogger.h"
#include "opengl/object/globject.h"

GEOCylinder::GEOCylinder() :
	GLGeometry()
{
	_params.set("GEO_TYPE", QVariant("Cylinder"));
	_params.set("axis", QVariant('Z'));
	_params.set("capped", QVariant(true));
	_params.set("length", QVariant(1.0));
	_params.set("radius", QVariant(1.0));
	_params.set("slices", QVariant(10));
	_params.set("stacks", QVariant(4));
}


bool GEOCylinder::init(GLObject*)
{
	QVector<QVector2D> circleVerts;

	char axis		= _params["axis"].toChar().toLatin1();
	bool capped		= _params["capped"].toBool();
	float length	= _params["length"].toFloat();
	float radius	= _params["radius"].toFloat();
	int slices		= _params["slices"].toInt();
	int stacks		= _params["stacks"].toInt();

	_capVertices.clear();
	_capNormals.clear();
	_capTexCoord.clear();

	_coreVertices.clear();
	_coreNormals.clear();
	_coreTexCoord.clear();

	if (!GLGeometry::getCircle2D(&circleVerts, slices, 1.0/*radius*/))
	{
		DebugLog(dlWarn, "GEOCylinder::init GLGeometry::getCircle2D failed");
		return false;
	}

	if (capped)
	{
		if (!GLGeometry::buildDisc(&circleVerts, axis, 0.0/*axisOffset*/, radius, &_capVertices, &_capNormals, &_capTexCoord))
		{
			DebugLog(dlWarn, "GEOCylinder::init GLGeometry::buildDisc failed");
			return false;
		}

		if (!GLGeometry::buildDisc(&circleVerts, axis, length/*axisOffset*/, radius, &_capVertices, &_capNormals, &_capTexCoord))
		{
			DebugLog(dlWarn, "GEOCylinder::init GLGeometry::buildDisc failed");
			return false;
		}
	} // if (capped)

	if (!GLGeometry::buildTube(&circleVerts, axis, length, radius, stacks, &_coreVertices, &_coreNormals, &_coreTexCoord))
	{
		DebugLog(dlWarn, "GEOCylinder::init GLGeometry::buildTube failed");
		return false;
	}

	_paintStates.beginUpdate();
	_paintStates.clear();
	_paintStates.add(GL_VERTEX_ARRAY);
	_paintStates.add(GL_NORMAL_ARRAY);
	_paintStates.add(GL_TEXTURE_COORD_ARRAY);
	_paintStates.endUpdate();

	_paintCaps.beginUpdate();
	_paintCaps.clear();
	_paintCaps.add(GL_DEPTH_TEST);
	_paintCaps.add(GL_CULL_FACE);
	_paintCaps.endUpdate();

	_initted = true;
	return true;
}


bool GEOCylinder::paint(GLObject* , GLView* )
{
	bool capped		= _params["capped"].toBool();
	int halfCapVerts = _capVertices.size() / 2;

	glVertexPointer(3, GL_FLOAT, 0, _coreVertices.constData());
	glNormalPointer(GL_FLOAT, 0, _coreNormals.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, _coreTexCoord.constData());

	glDrawArrays(GL_TRIANGLE_STRIP, 0, _coreVertices.size());


	if (capped)
	{
		glVertexPointer(3, GL_FLOAT, 0, _capVertices.constData());
		glNormalPointer(GL_FLOAT, 0, _capNormals.constData());
		glTexCoordPointer(2, GL_FLOAT, 0, _capTexCoord.constData());

		glDrawArrays(GL_TRIANGLE_FAN, 0, halfCapVerts);

		glDrawArrays(GL_TRIANGLE_FAN, halfCapVerts, halfCapVerts);
	}
	return true;
}
