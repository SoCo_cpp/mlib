#ifndef GEOBOX_H
#define GEOBOX_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include "opengl/object/glgeometry.h"

class GEOBox : public GLGeometry
{
	Q_OBJECT
public:
	GEOBox();

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

protected:
	QVector<QVector3D> _vertices;
	QVector<QVector2D> _texCoord;

signals:

public slots:

};

#endif // GEOBOX_H
