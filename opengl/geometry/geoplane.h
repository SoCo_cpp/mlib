#ifndef GEOPLANE_H
#define GEOPLANE_H

#include <QObject>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include "opengl/object/glgeometry.h"

class GEOPlane : public GLGeometry
{
	Q_OBJECT
public:
	GEOPlane();

	virtual bool init(GLObject* pobject);
	virtual bool paint(GLObject* pobject, GLView* glview);

protected:
	QVector<QVector3D> _vertices;
	QVector<QVector2D> _texCoord;

signals:

public slots:

};

#endif // GEOPLANE_H
