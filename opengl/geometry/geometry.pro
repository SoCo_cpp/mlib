
!defined(MLIB_SRC,var):error("MLIB opengl geometry (mlib/opengl/geometry/geometry.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/opengl/geometry

message("MLIB including opengl geometry...")

SOURCES +=  $$MSRC/geosphere.cpp \
            $$MSRC/geobox.cpp \
            $$MSRC/geoplane.cpp \
            $$MSRC/geocylinder.cpp \
            $$MSRC/geodisc.cpp

HEADERS +=  $$MSRC/geosphere.h \
            $$MSRC/geobox.h \
            $$MSRC/geoplane.h \
            $$MSRC/geocylinder.h \
            $$MSRC/geodisc.h
