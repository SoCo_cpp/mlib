#include "opengl/geometry/geobox.h"
#include "opengl/view/glview.h"
#include "opengl/object/globject.h"
#include "logging/debuglogger.h"

GEOBox::GEOBox() :
	GLGeometry()
{
	_params.set("GEO_TYPE", QVariant("Box"));
	_params.set("size-x", QVariant(1.0));
	_params.set("size-y", QVariant(1.0));
	_params.set("size-z", QVariant(1.0));
}

bool GEOBox::init(GLObject*)
{
	static const int coords[6][4][3] = {
    { { -1, -1, +1 }, { +1, -1, +1 }, { -1, +1, +1 }, { +1, +1, +1 } }, // front
    { { +1, -1, +1 }, { +1, -1, -1 }, { +1, +1, +1 }, { +1, +1, -1 } }, // right
    { { +1, -1, -1 }, { -1, -1, -1 }, { +1, +1, -1 }, { -1, +1, -1 } }, // back
    { { -1, -1, -1 }, { -1, -1, +1 }, { -1, +1, -1 }, { -1, +1, +1 } }, // left
    { { -1, -1, -1 }, { +1, -1, -1 }, { -1, -1, +1 }, { +1, -1, +1 } }, // bottom
    { { -1, +1, +1 }, { +1, +1, +1 }, { -1, +1, -1 }, { +1, +1, -1 } }  // top
    };

	QVector3D dimensions(_params["size-x"].toFloat(), _params["size-y"].toFloat(), _params["size-z"].toFloat());

	_vertices.clear();
	_texCoord.clear();

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 4; ++j)
		{
			_texCoord.append(QVector2D(j == 0 || j == 3, j == 0 || j == 1));
			_vertices.append(QVector3D(coords[i][j][0], coords[i][j][1], coords[i][j][2]) * dimensions);
		}

	_paintStates.beginUpdate();
	_paintStates.clear();
	_paintStates.add(GL_VERTEX_ARRAY);
	_paintStates.add(GL_TEXTURE_COORD_ARRAY);
	_paintStates.endUpdate();

	_paintCaps.beginUpdate();
	_paintCaps.clear();
	_paintCaps.add(GL_DEPTH_TEST);
	_paintCaps.add(GL_CULL_FACE);
	_paintCaps.endUpdate();

	_initted = true;
	return true;
}


bool GEOBox::paint(GLObject*, GLView*)
{
	glVertexPointer(3, GL_FLOAT, 0, _vertices.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord.constData());

	glDrawArrays(GL_TRIANGLE_STRIP, 0, _vertices.size());

	return true;
}
