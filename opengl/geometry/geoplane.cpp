#include "opengl/view/glview.h"
#include "opengl/geometry/geoplane.h"
#include "opengl/object/globject.h"
#include "logging/debuglogger.h"

GEOPlane::GEOPlane() :
	GLGeometry()
{
	_params.set("GEO_TYPE", QVariant("Plane"));
}

bool GEOPlane::init(GLObject*)
{
	static const int coords[4][3] = { { -1, -1, +1 }, { +1, -1, +1 }, { -1, +1, +1 }, { +1, +1, +1 } };

	_texCoord.clear();
	_vertices.clear();
	for (int j = 0; j < 4; ++j)
	{
		_texCoord.append(QVector2D(j == 0 || j == 3, j == 0 || j == 1));
		_vertices.append(QVector3D(coords[j][0], coords[j][1], coords[j][2]));
	}

	_paintStates.beginUpdate();
	_paintStates.clear();
	_paintStates.add(GL_VERTEX_ARRAY);
	_paintStates.add(GL_TEXTURE_COORD_ARRAY);
	_paintStates.endUpdate();

	_paintCaps.beginUpdate();
	_paintCaps.clear();
	_paintCaps.endUpdate();

	_initted = true;
	return true;
}

bool GEOPlane::paint(GLObject* , GLView*)
{
	glVertexPointer(3, GL_FLOAT, 0, _vertices.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, _texCoord.constData());

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glEnable(GL_DEPTH_TEST);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, _vertices.size());

	glDisable(GL_DEPTH_TEST);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	return true;
}
