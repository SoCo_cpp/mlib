#ifndef TEXTUREIMAGE_H
#define TEXTUREIMAGE_H

#include <QObject>
#include <QImage>

class TextureImage : public QObject
{
	Q_OBJECT
public:
	TextureImage();

	const QString& fileName() const;
	void setFileName(const QString& fileName);

	QImage& image();
	const QImage& image() const;

	bool isLoaded() const;
	bool load(const QString& fileName = QString());
	void unload();

protected:
	QString _fileName;
	QImage _image;

signals:

public slots:

};

#endif // TEXTUREIMAGE_H
