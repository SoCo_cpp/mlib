#include <QGLWidget>
#include "opengl/texture/texture.h"
#include "logging/debuglogger.h"
#include <QDebug>

Texture::Texture() :
	QObject(0),
	_glId(UINT_MAX)
{
}

Texture::~Texture()
{
	qDebug() << "Texture::~Texture...";
	removeAllObjects();
	qDebug() << "Texture::~Texture...DONE";
}

const QList<GLObject*>& Texture::objects() const
{
	return _objects;
}

void Texture::addObject(GLObject* object, bool reciprocate /*= true*/)
{
	if (reciprocate)
		object->addTexture(this, false/*reciprocate*/);
	_objects.append(object);
}

void Texture::removeObject(GLObject* object, bool reciprocate /*= true*/, bool unloadNoRef /*= true*/, bool fullUnload /*= true*/)
{
	DebugLog(dlDebug, "Texture::removeObject...");
	if (reciprocate)
		object->removeTexture(this, false/*reciprocate*/);
	_objects.removeOne(object);
	if (unloadNoRef && _objects.isEmpty())
		unload(fullUnload);
	DebugLog(dlDebug, "Texture::removeObject...DONE");
}

void Texture::removeAllObjects(bool doUnload /*= true*/, bool fullUnload /*= true*/)
{
	for (int i = 0;i < _objects.size();i++)
		_objects[i]->removeTexture(this, false/*reciprocate*/);
	_objects.clear();
	if (doUnload)
		unload(fullUnload);
}

TextureImage& Texture::image()
{
	return _image;
}

const TextureImage& Texture::image() const
{
	return _image;
}

const QString& Texture::fileName() const
{
	return _image.fileName();
}

void Texture::setFileName(const QString& fileName)
{
	_image.setFileName(fileName);
}

bool Texture::preLoad(const QString& fileName /*= QString()*/)
{
	if (!_image.load(fileName))
	{
		DebugLog(dlDebug, "Texture::preLoad texture image failed to load file");
		return false;
	}
	return true;
}

void Texture::preUnload()
{
	_image.unload();
}

bool Texture::isLoaded() const
{
	return (_glId != UINT_MAX);
}

bool Texture::load(const QString& fileName /*= QString()*/)
{
	if (!_image.isLoaded())
		if (!preLoad(fileName))
		{
			DebugLog(dlDebug, "Texture::load preLoad failed");
			return false;
		}
	if (!isLoaded())
	{
		glGenTextures(1, &_glId);
		glBindTexture(GL_TEXTURE_2D, _glId);
		glTexImage2D(GL_TEXTURE_2D, 0, 4, _image.image().width(), _image.image().height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, _image.image().convertToFormat(QImage::Format_ARGB32).bits());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	return true;
}

void Texture::unload(bool fullUnload /*= true*/)
{
	if (isLoaded())
	{
		glDeleteTextures(1, &_glId);
		_glId = UINT_MAX;
	}
	if (fullUnload)
		preUnload();
}

bool Texture::bind()
{
	if (!isLoaded())
		if (!load())
		{
			DebugLog(dlWarn, "Texture::bind not loaded and load failed");
			return false;
		}
	glBindTexture(GL_TEXTURE_2D, _glId);
	glEnable(GL_TEXTURE_2D);
	return true;
}

void Texture::unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}
