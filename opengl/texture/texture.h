#ifndef TEXTURE_H
#define TEXTURE_H

#include <QObject>
#include <QList>
#include "opengl/texture/textureimage.h"
#include "opengl/object/globject.h"

class Texture : public QObject
{
	Q_OBJECT
public:
	Texture();
	~Texture();

	TextureImage& image();
	const TextureImage& image() const;

	const QList<GLObject*>& objects() const;
	void addObject(GLObject* object, bool reciprocate = true);
	void removeObject(GLObject* object, bool reciprocate = true, bool unloadNoRef = true, bool fullUnload = true);
	void removeAllObjects(bool doUnload = true, bool fullUnload = true);

	const QString& fileName() const;
	void setFileName(const QString& fileName);

	bool preLoad(const QString& fileName = QString());
	void preUnload();

	bool isLoaded() const;
	bool load(const QString& fileName = QString());
	void unload(bool fullUnload = true);

	bool bind();
	void unbind();

protected:
	unsigned int _glId;
	TextureImage _image;
	QList<GLObject*> _objects;

signals:

public slots:

};

#endif // TEXTURE_H
