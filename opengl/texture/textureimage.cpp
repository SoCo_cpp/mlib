#include "opengl/texture/textureimage.h"
#include "logging/debuglogger.h"

TextureImage::TextureImage() :
	QObject(0)
{
}

const QString& TextureImage::fileName() const
{
	return _fileName;
}

void TextureImage::setFileName(const QString& fileName)
{
	_fileName = fileName;
}

QImage& TextureImage::image()
{
	return _image;
}

const QImage& TextureImage::image() const
{
	return _image;
}


bool TextureImage::isLoaded() const
{
	return !_image.isNull();
}

bool TextureImage::load(const QString& fileName /*= QString()*/)
{
	if (!fileName.isEmpty())
		_fileName = fileName;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "TextureImage::load fileName is empty");
		return false;
	}
	if (!_image.load(_fileName))
	{
		DebugLog(dlWarn, "TextureImage::load image failed to load file: %s", qPrintable(_fileName));
		return false;
	}
	return true;
}

void TextureImage::unload()
{
	_image = QImage();
}
