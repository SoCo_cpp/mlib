#ifndef GLCOLOR_H
#define GLCOLOR_H

#include <QObject>
#include "qgl.h"

class GLColor : public QObject
{
	Q_OBJECT
public:

	GLColor();
	GLColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha = 1.0, bool enabled = true);
	GLColor(const GLColor& cpy);
	GLColor& operator=(const GLColor& rhs);
	void copy(const GLColor& cpy);

	bool isEnabled() const;
	void setEnabled(bool enabled = true);

	const GLfloat& r() const;
	const GLfloat& g() const;
	const GLfloat& b() const;
	const GLfloat& a() const;
	const GLfloat* color() const;

	void setr(GLfloat red);
	void setg(GLfloat green);
	void setb(GLfloat blue);
	void seta(GLfloat alpha);
	void set(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha = -1.0, bool enabled = true);

	void setWhite(GLfloat alpha = 1.0, bool enabled = true);
	void setBlack(GLfloat alpha = 1.0, bool enabled = true);
	void setRed(GLfloat alpha = 1.0, bool enabled = true);
	void setGreen(GLfloat alpha = 1.0, bool enabled = true);
	void setBlue(GLfloat alpha = 1.0, bool enabled = true);
	void setGray(GLfloat intensity, GLfloat alpha = 1.0, bool enabled = true);
	void setClear();
	void setNull(bool enabled = false);
	void setOpaque();

	bool isNull() const;
	bool isClear() const;
	bool isOpaque() const;

protected:
	typedef enum {ciRed = 0, ciGreen, ciBlue, ciAlpha} ColorIndex;
	bool _enabled;
	GLfloat _color[4];

signals:
	void changed();

public slots:

};

#endif // GLCOLOR_H
