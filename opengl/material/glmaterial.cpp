#include "opengl/material/glmaterial.h"
#include "opengl/view/glview.h"

GLMaterial::GLMaterial() :
	QObject(0),
	_enabled(false),
	_face(GL_FRONT_AND_BACK),
	_shineEnabled(false),
	_shine(0.0)
{
}

GLMaterial::GLMaterial(const GLMaterial& cpy) :
	QObject(0),
	_enabled(false),
	_face(GL_FRONT_AND_BACK),
	_shineEnabled(false),
	_shine(0.0)

{
	copy(cpy);
}

GLMaterial& GLMaterial::operator=(const GLMaterial& rhs)
{
	copy(rhs);
	return *this;
}

void GLMaterial::copy(const GLMaterial& cpy)
{
	_enabled		= cpy._enabled;
	_ambient		= cpy._ambient;
	_specular		= cpy._specular;
	_diffuse		= cpy._diffuse;
	_emission		= cpy._emission;
	_shineEnabled	= cpy._shineEnabled;
	_shine			= cpy._shine;
	emit changed();
}

void GLMaterial::init()
{
	connect(&_ambient, SIGNAL(changed()), this, SIGNAL(changed()));
	connect(&_specular, SIGNAL(changed()), this, SIGNAL(changed()));
	connect(&_diffuse, SIGNAL(changed()), this, SIGNAL(changed()));
	connect(&_emission, SIGNAL(changed()), this, SIGNAL(changed()));
}

bool GLMaterial::isEnabled() const
{
	return _enabled;
}

void GLMaterial::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
	emit changed();
}

GLenum GLMaterial::face() const
{
	return _face;
}

void GLMaterial::setFace(GLenum f)
{
	_face = f;
	emit changed();
}

void GLMaterial::setFront()
{
	setFace(GL_FRONT);
}

void GLMaterial::setBack()
{
	setFace(GL_BACK);
}

void GLMaterial::setFrontBack()
{
	setFace(GL_FRONT_AND_BACK);
}

GLColor& GLMaterial::ambient()
{
	return _ambient;
}

const GLColor& GLMaterial::ambient() const
{
	return _ambient;
}

GLColor& GLMaterial::specular()
{
	return _specular;
}

const GLColor& GLMaterial::specular() const
{
	return _specular;
}

GLColor& GLMaterial::diffuse()
{
	return _diffuse;
}

const GLColor& GLMaterial::diffuse() const
{
	return _diffuse;
}

GLColor& GLMaterial::emission()
{
	return _emission;
}

const GLColor& GLMaterial::emission() const
{
	return _emission;
}

const GLfloat& GLMaterial::shine() const
{
	return _shine;
}

void GLMaterial::setShine(GLfloat sh, bool enabled /*= true*/)
{
	_shine = sh;
	_shineEnabled = enabled;
	emit changed();
}

bool GLMaterial::isShineEnabled() const
{
	return _shineEnabled;
}

void GLMaterial::enableShine(bool enabled /*= true*/)
{
	_shineEnabled = enabled;
	emit changed();
}

bool GLMaterial::initializeGL(GLView* )
{
	return true;
}

bool GLMaterial::paintGL(GLView* )
{
	if (!_enabled)
		return true; // nothing to do

	if (_shineEnabled)
		glMaterialfv(_face, GL_SHININESS, &_shine);
	if (_ambient.isEnabled())
		glMaterialfv(_face, GL_AMBIENT, _ambient.color());
	if (_specular.isEnabled())
		glMaterialfv(_face, GL_SPECULAR, _specular.color());
	if (_diffuse.isEnabled())
		glMaterialfv(_face, GL_DIFFUSE, _diffuse.color());
	if (_emission.isEnabled())
		glMaterialfv(_face, GL_EMISSION, _emission.color());

	return true;
}

