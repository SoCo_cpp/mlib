#include "opengl/material/glcolor.h"

GLColor::GLColor() :
	QObject(0),
	_enabled(false)
{
	setNull();
}

GLColor::GLColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha /*= 1.0*/, bool enabled /*= true*/) :
	QObject(0)
{
	set(red, green, blue, alpha, enabled);
}

GLColor::GLColor(const GLColor& cpy) :
	QObject(0)
{
	copy(cpy);
}

GLColor& GLColor::operator=(const GLColor& rhs)
{
	copy(rhs);
	return *this;
}

void GLColor::copy(const GLColor& cpy)
{
	_enabled		= cpy._enabled;
	_color[ciRed]	= cpy._color[ciRed];
	_color[ciGreen]	= cpy._color[ciGreen];
	_color[ciBlue]	= cpy._color[ciBlue];
	_color[ciAlpha]	= cpy._color[ciAlpha];
	emit changed();
}

bool GLColor::isEnabled() const
{
	return _enabled;
}

void GLColor::setEnabled(bool enabled /*= true*/)
{
	_enabled = enabled;
	emit changed();
}

const GLfloat& GLColor::r() const
{
	return _color[ciRed];
}

const GLfloat& GLColor::g() const
{
	return _color[ciGreen];
}

const GLfloat& GLColor::b() const
{
	return _color[ciBlue];
}

const GLfloat& GLColor::a() const
{
	return _color[ciAlpha];
}

const GLfloat* GLColor::color() const
{
	return _color;
}

void GLColor::setr(GLfloat red)
{
	_color[ciRed] = red;
	emit changed();
}

void GLColor::setg(GLfloat green)
{
	_color[ciGreen] = green;
	emit changed();
}

void GLColor::setb(GLfloat blue)
{
	_color[ciBlue] = blue;
	emit changed();
}

void GLColor::seta(GLfloat alpha)
{
	_color[ciAlpha] = alpha;
	emit changed();
}

void GLColor::set(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha /*= -1.0*/, bool enabled /*= true*/)
{
	_color[ciRed]	= red;
	_color[ciGreen]	= green;
	_color[ciBlue]	= blue;
	if (alpha != -1.0)
		_color[ciAlpha]	= alpha;
	_enabled = enabled;
	emit changed();
}

void GLColor::setWhite(GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(1.0, 1.0, 1.0, alpha, enabled);
}

void GLColor::setBlack(GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(0.0, 0.0, 0.0, alpha, enabled);
}

void GLColor::setRed(GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(1.0, 0.0, 0.0, alpha, enabled);
}

void GLColor::setGreen(GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(0.0, 1.0, 0.0, alpha, enabled);
}

void GLColor::setBlue(GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(0.0, 0.0, 1.0, alpha, enabled);
}

void GLColor::setGray(GLfloat intensity, GLfloat alpha /*= 1.0*/, bool enabled /*= true*/)
{
	set(intensity, intensity, intensity, alpha, enabled);
}

void GLColor::setClear()
{
	_color[ciAlpha]	= 0.0;
}

void GLColor::setNull(bool enabled /*= false*/)
{
	set(0.0, 0.0, 0.0, 0.0, enabled);
}

void GLColor::setOpaque()
{
	_color[ciAlpha]	= 1.0;
}

bool GLColor::isNull() const
{
	return (_color[ciRed] == 0.0 && _color[ciGreen] == 0.0 && _color[ciBlue] == 0.0 && _color[ciAlpha] == 0.0);
}

bool GLColor::isClear() const
{
	return (_color[ciAlpha] == 0.0);
}

bool GLColor::isOpaque() const
{
	return (_color[ciAlpha] == 1.0);
}
