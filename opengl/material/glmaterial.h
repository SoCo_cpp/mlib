#ifndef GLMATERIAL_H
#define GLMATERIAL_H

#include <QObject>
#include "opengl/material/glcolor.h"

class GLView; // predeclared
class GLMaterial : public QObject
{
	Q_OBJECT
public:
	GLMaterial();
	GLMaterial(const GLMaterial& cpy);
	GLMaterial& operator=(const GLMaterial& rhs);
	void copy(const GLMaterial& cpy);
	void init();

	bool isEnabled() const;
	void setEnabled(bool enabled = true);

	GLenum face() const;
	void setFace(GLenum f);
	void setFront();
	void setBack();
	void setFrontBack();

	GLColor& ambient();
	const GLColor& ambient() const;

	GLColor& specular();
	const GLColor& specular() const;

	GLColor& diffuse();
	const GLColor& diffuse() const;

	GLColor& emission();
	const GLColor& emission() const;

	const GLfloat& shine() const;
	void setShine(GLfloat sh, bool enabled = true);
	bool isShineEnabled() const;
	void enableShine(bool enabled = true);

	virtual bool initializeGL(GLView* glview);
	virtual bool paintGL(GLView* glview);

protected:
	bool _enabled;
	GLenum _face;
	bool _shineEnabled;
	GLfloat _shine;
	GLColor _ambient;
	GLColor _specular;
	GLColor _diffuse;
	GLColor _emission;

signals:
	void changed();

public slots:

};

#endif // GLMATERIAL_H
