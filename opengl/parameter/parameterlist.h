#ifndef PARAMETERLIST_H
#define PARAMETERLIST_H

#include <QObject>
#include <QHash>
#include <QVariant>

class ParameterList : public QObject
{
	Q_OBJECT
public:
	ParameterList();
	ParameterList(const ParameterList& cpy);
	ParameterList& operator=(const ParameterList& rhs);
	ParameterList& operator+=(const ParameterList& rhs);
	ParameterList operator+(const ParameterList& rhs) const;
	QVariant& operator[](const QString& param);
	const QVariant operator[](const QString& param) const;
	void copy(const ParameterList& cpy, bool suppressChangedSignal = false);

	void append(const ParameterList& other, bool suppressChangedSignal = false);
	void append(const QHash<QString,QVariant>& other, bool suppressChangedSignal = false);

	QHash<QString,QVariant>& list();
	const QHash<QString,QVariant>& list() const;

	void touch(bool suppressChangedSignal = false);

	int count() const;
	int size() const;
	bool isEmpty() const;
	void clear(bool suppressChangedSignal = false);

	void beginUpdate();
	void endUpdate();

	bool add(const QString& param, QVariant value, bool suppressChangedSignal = false); // fails if exists
	void set(const QString& param, QVariant value, bool suppressChangedSignal = false);
	QVariant get(const QString& param) const;
	bool contains(const QString& param) const;
	bool remove(const QString& param, bool suppressChangedSignal = false);

protected:
	bool _updating;
	bool _modified;
	QHash<QString,QVariant> _params;

signals:
	void changed();

public slots:

};

#endif // PARAMETERLIST_H
