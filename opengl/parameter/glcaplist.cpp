#include "opengl/parameter/glcaplist.h"

GLCapList::GLCapList() :
	QObject(0),
	_updating(false),
	_modified(false)
{
}

GLCapList::GLCapList(const GLCapList& cpy) :
	QObject(0),
	_updating(false),
	_modified(false)
{
	copy(cpy);
}

GLCapList& GLCapList::operator=(const GLCapList& rhs)
{
	copy(rhs);
	return *this;
}

GLCapList& GLCapList::operator+=(const GLCapList& rhs)
{
	append(rhs);
	return *this;
}

GLCapList GLCapList::operator+(const GLCapList& rhs) const
{
	GLCapList newList(*this);
	newList.append(rhs);
	return newList;
}

qint32& GLCapList::operator[](int idx)
{
	return _caps[idx];
}

const qint32& GLCapList::operator[](int idx) const
{
	return _caps[idx];
}

void GLCapList::copy(const GLCapList& cpy, bool suppressChangedSignal /*= false*/)
{
	_caps = cpy._caps;
	touch(suppressChangedSignal);
}

void GLCapList::append(const GLCapList& other, bool suppressChangedSignal /*= false*/)
{
	append(other.list(), suppressChangedSignal);
}

void GLCapList::append(const QList<qint32>& other, bool suppressChangedSignal /*= false*/)
{
	for (int i = 0;i < other.size();i++)
		if (!_caps.contains(other[i]))
			_caps.append(other[i]);
	touch(suppressChangedSignal);
}

QList<qint32>& GLCapList::list()
{
	return _caps;
}

const QList<qint32>& GLCapList::list() const
{
	return _caps;
}

void GLCapList::touch(bool suppressChangedSignal /*= false*/)
{
	if (_updating)
		_modified = true;
	else if (!suppressChangedSignal)
		emit changed();
}

int GLCapList::count() const
{
	return _caps.size();
}

int GLCapList::size() const
{
	return _caps.size();
}

bool GLCapList::isEmtpy() const
{
	return _caps.isEmpty();
}

void GLCapList::clear(bool suppressChangedSignal /*= false*/)
{
	_caps.clear();
	touch(suppressChangedSignal);
}

void GLCapList::beginUpdate()
{
	_updating = true;
}

void GLCapList::endUpdate()
{
	_updating = false;
	if (_modified)
		emit changed();
	_modified = false;
}

bool GLCapList::add(qint32 cap, bool suppressChangedSignal /*= false*/)
{
	if (contains(cap))
		return false;
	_caps.append(cap);
	touch(suppressChangedSignal);
	return true;
}

bool GLCapList::contains(qint32 cap)
{
	return _caps.contains(cap);
}

bool GLCapList::remove(qint32 cap, bool suppressChangedSignal /*= false*/)
{
	bool r = _caps.removeOne(cap);
	touch(suppressChangedSignal);
	return r;
}

