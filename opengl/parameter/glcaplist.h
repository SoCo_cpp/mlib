#ifndef GLCAPLIST_H
#define GLCAPLIST_H

#include <QObject>
#include <QList>

class GLCapList : public QObject
{
	Q_OBJECT
public:
	GLCapList();
	GLCapList(const GLCapList& cpy);
	GLCapList& operator=(const GLCapList& rhs);
	GLCapList& operator+=(const GLCapList& rhs);
	GLCapList operator+(const GLCapList& rhs) const;
	qint32& operator[](int idx);
	const qint32& operator[](int idx) const;
	void copy(const GLCapList& cpy, bool suppressChangedSignal = false);

	void append(const GLCapList& other, bool suppressChangedSignal = false);
	void append(const QList<qint32>& other, bool suppressChangedSignal = false);

	QList<qint32>& list();
	const QList<qint32>& list() const;

	void touch(bool suppressChangedSignal = false);

	int count() const;
	int size() const;
	bool isEmtpy() const;
	void clear(bool suppressChangedSignal = false);

	void beginUpdate();
	void endUpdate();

	bool add(qint32 cap, bool suppressChangedSignal = false); // returns false if already exists
	bool contains(qint32 cap);
	bool remove(qint32 cap, bool suppressChangedSignal = false);

protected:
	bool _updating;
	bool _modified;
	QList<qint32> _caps; // GLenum

signals:
	void changed();

public slots:

};

#endif // GLCAPLIST_H
