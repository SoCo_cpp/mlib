#include "opengl/parameter/parameterlist.h"

ParameterList::ParameterList() :
	QObject(0),
	_updating(false),
	_modified(false)
{
}

ParameterList::ParameterList(const ParameterList& cpy) :
	QObject(0),
	_updating(false),
	_modified(false)
{
	copy(cpy);
}

ParameterList& ParameterList::operator=(const ParameterList& rhs)
{
	copy(rhs);
	return *this;
}

ParameterList& ParameterList::operator+=(const ParameterList& rhs)
{
	append(rhs);
	return *this;
}

ParameterList ParameterList::operator+(const ParameterList& rhs) const
{
	ParameterList newList(*this);
	newList.append(rhs);
	return newList;
}

QVariant& ParameterList::operator[](const QString& param)
{
	return _params[param];
}

const QVariant ParameterList::operator[](const QString& param) const
{
	return _params[param];
}

void ParameterList::copy(const ParameterList& cpy, bool suppressChangedSignal /*= false*/)
{
	_params = cpy._params;
	touch(suppressChangedSignal);
}

void ParameterList::append(const ParameterList& other, bool suppressChangedSignal /*= false*/)
{
	append(other.list(), suppressChangedSignal);
}

void ParameterList::append(const QHash<QString,QVariant>& other, bool suppressChangedSignal /*= false*/)
{
	QList<QString> otherKeys = other.uniqueKeys();
	for (int i = 0;i < otherKeys.size();i++)
		_params.insert(otherKeys[i], other[otherKeys[i]]);
	touch(suppressChangedSignal);
}

QHash<QString,QVariant>& ParameterList::list()
{
	return _params;
}

const QHash<QString,QVariant>& ParameterList::list() const
{
	return _params;
}

void ParameterList::touch(bool suppressChangedSignal /*= false*/)
{
	if (_updating)
		_modified = true;
	else if (!suppressChangedSignal)
		emit changed();
}

int ParameterList::count() const
{
	return _params.size();
}

int ParameterList::size() const
{
	return _params.size();
}

bool ParameterList::isEmpty() const
{
	return _params.isEmpty();
}

void ParameterList::clear(bool suppressChangedSignal /*= false*/)
{
	_params.clear();
	touch(suppressChangedSignal);
}

void ParameterList::beginUpdate()
{
	_updating = true;
}

void ParameterList::endUpdate()
{
	_updating = false;
	if (_modified)
		emit changed();
	_modified = false;
}

bool ParameterList::add(const QString& param, QVariant value, bool suppressChangedSignal /*= false*/)
{
	if (contains(param))
		return false;
	_params.insert(param, value);
	touch(suppressChangedSignal);
	return true;
}

void ParameterList::set(const QString& param, QVariant value, bool suppressChangedSignal /*= false*/)
{
	_params.insert(param, value);
	touch(suppressChangedSignal);
}

QVariant ParameterList::get(const QString& param) const
{
	return _params[param];
}

bool ParameterList::contains(const QString& param) const
{
	return (_params.find(param) != _params.end());
}

bool ParameterList::remove(const QString& param, bool suppressChangedSignal /*= false*/)
{
	bool r = (_params.remove(param) != 0); // returns count removed
	touch(suppressChangedSignal);
	return r;
}
