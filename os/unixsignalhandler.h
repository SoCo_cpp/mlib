#ifndef UNIXSIGNALHANDLER_H
#define UNIXSIGNALHANDLER_H

#include <QObject>

// #include <signal.h> SIGHUP, SIGTERM, SIGABRT

class QSocketNotifier; // predeclared

class UnixSignalHandler : public QObject
{
	Q_OBJECT
public:

	UnixSignalHandler(QObject* parent = 0);

	static int initUnitSignalHandlers();

protected:
	virtual void unixSignalEvent(int sigNum) = 0;

#ifdef Q_OS_UNIX
public:
	// Unix signal handlers.
	static void USH_signalHandler(int sigNum);

private:
	static int _USH_Fd[2];

	QSocketNotifier* _USH_sn;

public slots:
	void UnixSignalHandler_handleSig();

#endif // Q_OS_UNIX
};

#endif // UNIXSIGNALHANDLER_H
