
!defined(MLIB_SRC,var):error("MLIB file (os/unixsignals.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/os

message("MLIB including unixsignals...")


HEADERS += \
    $$MSRC/unixsignalhandler.h

SOURCES += \
    $$MSRC/unixsignalhandler.cpp
