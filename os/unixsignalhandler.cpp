#include "os/unixsignalhandler.h"
#include <QSocketNotifier>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>

#ifdef Q_OS_UNIX
/*static*/
int UnixSignalHandler::_USH_Fd[2];
#endif //Q_OS_UNIX

UnixSignalHandler::UnixSignalHandler(QObject* parent /*= 0*/) :
	QObject(parent)
{
#ifdef Q_OS_UNIX
	if (::socketpair(AF_UNIX, SOCK_STREAM, 0, _USH_Fd))
	   qFatal("Couldn't create signal socketpair");

	_USH_sn = new QSocketNotifier(_USH_Fd[1], QSocketNotifier::Read, this);

	connect(_USH_sn, SIGNAL(activated(int)), this, SLOT(UnixSignalHandler_handleSig()));
#endif //Q_OS_UNIX
}

#ifdef Q_OS_UNIX
/*static*/
void UnixSignalHandler::USH_signalHandler(int sigNum)
{
	ssize_t l = ::write(_USH_Fd[0], &sigNum, sizeof(sigNum));
	Q_UNUSED(l);
}

void UnixSignalHandler::UnixSignalHandler_handleSig()
{
	_USH_sn->setEnabled(false);
	int sigNum;
	ssize_t l = ::read(_USH_Fd[1], &sigNum, sizeof(sigNum));
	Q_UNUSED(l);

	unixSignalEvent(sigNum);

	_USH_sn->setEnabled(true);
}
#endif //Q_OS_UNIX

/*static*/
int UnixSignalHandler::initUnitSignalHandlers()
{
	#ifdef Q_OS_UNIX
		struct sigaction act;

		act.sa_handler = UnixSignalHandler::USH_signalHandler;
		sigemptyset(&act.sa_mask);
		act.sa_flags = 0;
		act.sa_flags |= SA_RESTART;

		if (sigaction(SIGHUP, &act, 0) > 0)
		   return 1;
		if (sigaction(SIGTERM, &act, 0) > 0)
		   return 2;
		if (sigaction(SIGINT, &act, 0) > 0)
		   return 3;
        if (sigaction(SIGABRT, &act, 0) > 0)
           return 4;
    #endif //Q_OS_UNIX
	return 0;
}
