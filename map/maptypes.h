#ifndef MAPTYPES_H
#define MAPTYPES_H

class MapTypes
{
public:
	typedef enum {mtOSM = 0 } MapType;
	typedef enum {tpZXY = 0, tpZYX, tpXYZ} MapTilePathOrder;
	typedef enum {tsNone = 0, tsCache, tsDownload} MapTileSource;
};
#endif // MAPTYPES_H
