#ifndef MAPCONFIG_H
#define MAPCONFIG_H

#include <QObject>
#include "map/maptypes.h"

#define C_DEFAULT_MAPCONFIG_MAPTYPE				MapTypes::mtOSM
#define C_DEFAULT_MAPCONFIG_EXTENSION			".PNG"
#define C_DEFAULT_MAPCONFIG_PATHORDER			MapTypes::tpZXY // download overriden based on map type

class MapConfig : public QObject
{
	Q_OBJECT
public:
	MapConfig(MapTypes::MapType mapType = C_DEFAULT_MAPCONFIG_MAPTYPE, const QString& imageExtension = QString(C_DEFAULT_MAPCONFIG_EXTENSION), const QString& cacheDir = QString(""), const QString& downloadUrlBase = QString(""), MapTypes::MapTilePathOrder cachePathOrder = C_DEFAULT_MAPCONFIG_PATHORDER, MapTypes::MapTilePathOrder downloadPathOrder = C_DEFAULT_MAPCONFIG_PATHORDER);
	MapConfig(const MapConfig& cpy);
	MapConfig& operator=(const MapConfig& rhs);
	void copy(const MapConfig& cpy);

	void setMapType(MapTypes::MapType mapType);
	void setTilePathOrders(MapTypes::MapTilePathOrder cachePathOrder, MapTypes::MapTilePathOrder downloadPathOrder);
	void setCacheTilePathOrder(MapTypes::MapTilePathOrder cachePathOrder);
	MapTypes::MapType mapType() const;
	MapTypes::MapTilePathOrder cacheTilePathOrder() const;
	MapTypes::MapTilePathOrder downloadTilePathOrder() const;

	const QString& imageExtension() const;
	void setImageExtension(const QString& ext);

	bool cacheEnabled() const;
	bool enableCache(const QString& sCacheDir = QString("")); // checks for valid cachDir
	void disableCache();
	const QString& cacheDir() const;

	bool downloadEnbaled() const;
	bool enableDownload(const QString& sDownloadUrlBase = QString(""));
	void disableDownload();
	const QString& downloadUrlBase() const;

	int tileSizePx() const;

protected:
	static const QString _emptyString;
	MapTypes::MapType _mapType;
	bool _cacheEnabled;
	bool _downloadEnabled;
	QString _imageExtension;
	QString _cacheDir;
	QString _downloadUrlBase;
	MapTypes::MapTilePathOrder _cacheTilePathOrder;
	MapTypes::MapTilePathOrder _downloadTilePathOrder;

signals:
	void changed();

public slots:

};

#endif // MAPCONFIG_H
