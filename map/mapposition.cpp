#include <QtMath>
#include <math.h>
#include "logging/debuglogger.h"
#include "map/mapview.h"
#include "map/mapposition.h"

MapPosition::MapPosition(MapView* mapView) :
	QObject(0),
	_mapView(mapView),
	_set(false),
	_dx(0.0),
	_dy(0.0),
	_x(0),
	_y(0),
	_zoom(0),
	_key("")
{
	Q_ASSERT(_mapView);
}

MapPosition::MapPosition(const MapPosition& cpy) :
	QObject(0)
{
	copy(cpy);
}

MapPosition& MapPosition::operator=(const MapPosition& rhs)
{
	copy(rhs);
	return *this;
}

void MapPosition::copy(const MapPosition& cpy)
{
	_mapView = cpy._mapView;
	Q_ASSERT(_mapView);
	_set = cpy._set;
	_x = cpy._x;
	_y = cpy._y;
	_zoom = cpy._zoom;
	updateKey();
	emit changed();
}

bool MapPosition::isSet() const
{
	return _set;
}

bool MapPosition::set(double dx, double dy, int zoom /*= -1*/)
{
	if (zoom == -1)
		zoom = _zoom; ///keep copy so there are no changes on fail
	else if (zoom  < cMinZoom || zoom > cMaxZoom)
	{
		DebugLog(dlWarn, "MapPosition::set zoom invalid: %d (%d to %d)", zoom, cMinZoom, cMaxZoom);
		return false;
	}
	int max = xyMax(zoom);
	int x = qFloor(dx);
	int y = qFloor(dy);
	if (x < 0 || x >= max || y < 0 || y >= max)
	{
		DebugLog(dlWarn, "MapPosition::set x/y invalid. Max: %d (%d, %d)", max, x, y);
		return false;
	}
	_dx = dx;
	_dy = dy;
	_x = x;
	_y = y;
	_zoom = zoom;
	_set = true;
	updateKey();
	emit changed();
	return true;
}

bool MapPosition::setLonLat(double lon, double lat, int zoom /*= -1*/)
{
	double dx, dy, phi;
	if (zoom == -1)
		zoom = _zoom; ///keep copy so there are no changes on fail
	else if (zoom  < cMinZoom || zoom > cMaxZoom)
	{
		DebugLog(dlWarn, "MapPosition::setLonLat zoom invalid: %d (%d to %d)", zoom, cMinZoom, cMaxZoom);
		return false;
	}
	dx = ((lon + 180.0) / 360.0) * qPow(2.0, _zoom);
	phi = lat * M_PI / 180.0;
	dy = (1.0 - qLn(  (qTan(phi) + 1.0) / qCos(phi) ) / M_PI) / 2.0 * qPow(2.0, _zoom);
	int max = xyMax(zoom);
	int x = qFloor(dx);
	int y = qFloor(dy);
	if (x < 0 || x >= max || y < 0 || y >= max)
	{
		DebugLog(dlWarn, "MapPosition::setLonLat x/y invalid. Max: %d (%d, %d) LonLat: (%0.6f, %0.6f)", max, x, y, (float)lon, (float)lat);
		return false;
	}
	_dx = dx;
	_dy = dy;
	_x = x;
	_y = y;
	_zoom = zoom;
	_set = true;
	updateKey();
	emit changed();
	return true;
}

bool MapPosition::setZoom(int zoom)
{
	Q_ASSERT(_mapView);
	if (zoom  < cMinZoom || zoom > cMaxZoom)
	{
		DebugLog(dlWarn, "MapPosition::setZoom zoom invalid: %d (%d to %d)", zoom, cMinZoom, cMaxZoom);
		return false;
	}
	double maxDiff = ((double)xyMax(zoom)) / ((double)xyMax());
	_dx *= maxDiff;
	_dy *= maxDiff;
	_x = qFloor(_dx);
	_y = qFloor(_dy);
	_zoom = zoom;
	_set = true;
	updateKey();
	emit changed();
	return true;
}

void MapPosition::clear()
{
	_set = false;
	_x = 0;
	_y = 0;
	_dx = 0.0;
	_dy = 0.0;
	_zoom = 0;
	updateKey();
	emit changed();
}

int MapPosition::x() const
{
	return _x;
}

int MapPosition::y() const
{
	return _y;
}

int MapPosition::zoom() const
{
	return _zoom;
}

double MapPosition::dx() const
{
	return _dx;
}

double MapPosition::dy() const
{
	return _dy;
}

double MapPosition::xFraction() const
{
	return (_dx - ((double)_x));
}

double MapPosition::yFraction() const
{
	return (_dy - ((double)_y));
}

QString MapPosition::buildKey(int x, int y, int zoom)
{
	return QString("%1/%2/%3").arg(zoom).arg(x).arg(y);
}

void MapPosition::updateKey()
{
	if (!_set)
	{
		DebugLog(dlWarn, "MapPosition::updateKey not set");
		_key = "";
	}
	else
		_key = buildKey(_x, _y, _zoom);
}

const QString& MapPosition::key() const
{
	return _key;
}

int MapPosition::xyMax(int forZoom /*= -1*/) const
{
	if (forZoom == -1)
		forZoom = _zoom;
	return 1 << forZoom;
}

double MapPosition::lon() const
{
	if (!_set)
		return 0.0;
	return lon(_dx, _zoom);
}

double MapPosition::lat() const
{
	if (!_set)
		return 0.0;
	return lat(_dy, _zoom);
}

double MapPosition::lon(double x, int zoom)
{
	return x / qPow(2.0, zoom) * 360.0 - 180.0;
}

double MapPosition::lat(double y, int zoom)
{
	double n = M_PI - 2.0 * M_PI * y / qPow(2.0, zoom);
	return 180.0 / M_PI * qAtan(0.5 * (qExp(n) - qExp(-n)));
}

void MapPosition::moveBy(double mdx, double mdy)
{
	double max = xyMax();
	double dx = _dx + mdx;
	double dy = _dy + mdy;
	if (dx < 0.0)
		dx += max;
	else if (dx >= max)
		dx -= max;
	if (dy < 0.0)
		dy += max;
	else if (dy >= max)
		dy -= max;
	if (!set(dx, dy))
		DebugLog(dlWarn, "MapPosition::moveBy set failed, orig (%d, %d), move by (%0.3f, %0.3f), result (%0.3f, %0.3f)", _dx, _dy, (float)mdx, (float)mdy, (float)dx, (float)dy);
}
