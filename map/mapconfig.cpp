#include "map/mapconfig.h"
#include "logging/debuglogger.h"

const QString MapConfig::_emptyString("");

MapConfig::MapConfig(MapTypes::MapType mapType /*= C_DEFAULT_MAPCONFIG_MAPTYPE*/, const QString& imageExtension /*= QString(C_DEFAULT_MAPCONFIG_EXTENSION)*/, const QString& cacheDir /*= QString("")*/, const QString& downloadUrlBase /*= QString("")*/, MapTypes::MapTilePathOrder cachePathOrder /*= C_DEFAULT_MAPCONFIG_PATHORDER*/, MapTypes::MapTilePathOrder downloadPathOrder /*= C_DEFAULT_MAPCONFIG_PATHORDER*/) :
	QObject(0),
	_mapType(mapType),
	_cacheEnabled(false),
	_downloadEnabled(false),
	_imageExtension(imageExtension),
	_cacheDir(cacheDir),
	_downloadUrlBase(downloadUrlBase),
	_cacheTilePathOrder(cachePathOrder),
	_downloadTilePathOrder(downloadPathOrder)
{
	_cacheEnabled		= !_cacheDir.isEmpty();
	_downloadEnabled	= !_downloadUrlBase.isEmpty();
	setMapType(_mapType); // initialize path order defaults
}

MapConfig::MapConfig(const MapConfig& cpy) :
	QObject(0)
{
	copy(cpy);
}

MapConfig& MapConfig::operator=(const MapConfig& rhs)
{
	copy(rhs);
	return *this;
}

void MapConfig::copy(const MapConfig& cpy)
{
	_mapType				= cpy._mapType;
	_cacheEnabled			= cpy._cacheEnabled;
	_downloadEnabled		= cpy._downloadEnabled;
	_imageExtension			= cpy._imageExtension;
	_cacheDir				= cpy._cacheDir;
	_downloadUrlBase		= cpy._downloadUrlBase;
	_cacheTilePathOrder		= cpy._cacheTilePathOrder;
	_downloadTilePathOrder	= cpy._downloadTilePathOrder;
	emit changed();
}

void MapConfig::setMapType(MapTypes::MapType mapType)
{
	_mapType = mapType;
	if (_mapType == MapTypes::mtOSM)
		_downloadTilePathOrder = MapTypes::tpZXY;
	emit changed();
}

void MapConfig::setTilePathOrders(MapTypes::MapTilePathOrder cachePathOrder, MapTypes::MapTilePathOrder downloadPathOrder)
{
	_cacheTilePathOrder = cachePathOrder;
	_downloadTilePathOrder = downloadPathOrder;
	emit changed();
}

void MapConfig::setCacheTilePathOrder(MapTypes::MapTilePathOrder cachePathOrder)
{
	_cacheTilePathOrder	= cachePathOrder;
	emit changed();
}

MapTypes::MapType MapConfig::mapType() const
{
	return _mapType;
}

MapTypes::MapTilePathOrder MapConfig::cacheTilePathOrder() const
{
	return _cacheTilePathOrder;
}

MapTypes::MapTilePathOrder MapConfig::downloadTilePathOrder() const
{
	return _downloadTilePathOrder;
}

const QString& MapConfig::imageExtension() const
{
	return _imageExtension;
}

void MapConfig::setImageExtension(const QString& ext)
{
	_imageExtension = ext;
	emit changed();
}

bool MapConfig::cacheEnabled() const
{
	return (_cacheEnabled && !_cacheDir.isEmpty());
}

bool MapConfig::enableCache(const QString& sCacheDir /*= QString("")*/)
{
	if (!sCacheDir.isEmpty())
		_cacheDir = sCacheDir;
	if (_cacheDir.isEmpty())
		return false;
	_cacheEnabled = true;
	emit changed();
	return true;
}

void MapConfig::disableCache()
{
	_cacheEnabled = false;
	emit changed();
}

const QString& MapConfig::cacheDir() const
{
	if (!_cacheEnabled)
		return _emptyString;
	return _cacheDir;
}

bool MapConfig::downloadEnbaled() const
{
	return _downloadEnabled;
}

bool MapConfig::enableDownload(const QString& sDownloadUrlBase /*= QString("")*/)
{
	if (!sDownloadUrlBase.isEmpty())
		_downloadUrlBase = sDownloadUrlBase;
	if (_downloadUrlBase.isEmpty())
		return false;
	_downloadEnabled = true;
	emit changed();
	return true;
}

void MapConfig::disableDownload()
{
	_downloadEnabled = false;
	emit changed();
}

const QString& MapConfig::downloadUrlBase() const
{
	if (!_downloadEnabled)
		return _emptyString;
	return _downloadUrlBase;
}

int MapConfig::tileSizePx() const
{
	if (_mapType == MapTypes::mtOSM)
		return 256;
	DebugLog(dlWarn, "MapConfig::tileSizePx unsupported mapType: %d", (int)_mapType);
	return 256;
}

