#ifndef MAPVIEW_H
#define MAPVIEW_H

#include <QObject>
#include <QRectF>
#include <QHash>
#include "map/mapposition.h"
#include "map/maptile.h"
#include "map/maptypes.h"
#include "map/mapconfig.h"

class MapView : public QObject
{
	Q_OBJECT
public:
	typedef QVector<MapTile*> MapTileRow;
	typedef QVector<MapTileRow> MapTileLines;
	typedef struct
	{
		bool done;
		QPoint pixelPos;
		MapTile* mapTile;
	} VisibleMapTile;

	MapView();

	MapConfig& config();
	const MapConfig& config() const;
	void setConfig(const MapConfig& cpyConfig);

	MapPosition& pos();
	const MapPosition& pos() const;

	int width() const;
	int height() const;

	void calcRects();
	const QRect& getTileRect();
	const QRectF& getTileRectF();
	const QRectF& getLatLonRect();
	const QPoint& getPixelOffset();

	void buildVisibleTiles(bool loadAll = true);
	QVector<VisibleMapTile>& visibleTiles(bool loadAllOnRebuild = true);

	MapTile* createMapTile(int x, int y, int zoom);

	void prepVisibleTiles();

	void panByPixels(int x, int y);

	bool isTileVisible(MapTile* mapTile) const;
	VisibleMapTile* findVisibleMapTile(MapTile* mapTile);

protected:
	MapConfig _config;
	MapPosition _pos;
	int _width;
	int _height;
	bool _recsChanged;
	QRect _tileRect;
	QRectF _tileRectF;
	QRectF _latlonRect;
	QPoint _tileDemensions;
	QPoint _pixelOffset;

	QHash<QString,MapTile*> _tiles;
	bool _visibleTilesChanged;
	QVector<VisibleMapTile> _visibleTiles;

signals:
	void changed();
	void viewChanged(); // needs redraw
	void viewTilesUpdated(); // needs selective redraw

public slots:
	void configChanged();
	void viewPositionChanged();
	void resized(int width, int height);
	void tileChanged();
	void tileReady();
	void tileLoadFailed(QString message);
	void tileSaveFailed(QString message);
};

#endif // MAPVIEW_H
