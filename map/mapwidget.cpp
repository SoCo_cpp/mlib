#include <QPainter>
#include "logging/debuglogger.h"
#include "map/mapwidget.h"
#include "map/maptile.h"

MapWidget::MapWidget(QWidget *parent) :
	QWidget(parent)
{
	_mapView.config().setMapType(MapTypes::mtOSM);
	_mapView.config().setImageExtension(".png");
	_mapView.config().enableCache("/home/soco/.osm");
	_mapView.config().enableDownload("http://tile.openstreetmap.org");
	_mapView.pos().setLonLat(89.0000, 40.0000);

	connect(&_mapView, SIGNAL(viewChanged()), this, SLOT(viewChanged()));
	connect(&_mapView, SIGNAL(viewTilesUpdated()), this, SLOT(viewChanged()));
	connect(this, SIGNAL(resized(int,int)), &_mapView, SLOT(resized(int,int)));
}

MapView& MapWidget::mapView()
{
	return _mapView;
}

const MapView& MapWidget::mapView() const
{
	return _mapView;
}

void MapWidget::prepTiles()
{
	_mapView.prepVisibleTiles();
}

void MapWidget::resizeEvent(QResizeEvent*)
{
	emit resized(width(), height());
}

void MapWidget::paintEvent(QPaintEvent*)
{
	 QPainter painter(this);
	 drawTiles(&painter);
}

void MapWidget::drawTiles(QPainter* painter)
{
	MapView::VisibleMapTile* visTile;
	DebugLog(dlDebug, "MapWidget::drawTiles full draw...");
	QVector<MapView::VisibleMapTile>& visibleTiles = _mapView.visibleTiles();
	for (QVector<MapView::VisibleMapTile>::iterator itr = visibleTiles.begin();itr != visibleTiles.end();++itr)
	{
		visTile = (&(*itr));
		if ( visTile->mapTile && visTile->mapTile->isReady() && !visTile->done )
		{
			painter->drawImage(visTile->pixelPos, visTile->mapTile->image());
			visTile->done = true;
		}
	}
	DebugLog(dlDebug, "MapWidget::drawTiles full draw...DONE");
}

void MapWidget::viewChanged()
{
	update();
}

