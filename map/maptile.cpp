#include <QFile>
#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "logging/debuglogger.h"
#include "map/mapview.h"
#include "map/maptile.h"


MapTile::MapTile(MapView* pMapView) :
	QObject(pMapView),
	_mapView(pMapView),
	_pos(pMapView),
	_ready(false),
	_busy(false),
	_cached(false),
	_source(MapTypes::tsNone),
	_image(),
	_lastErrorMessage("")
{
	Q_ASSERT(_mapView);
	connect(&_netAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkRequestFinished(QNetworkReply*)));
	connect(&_pos, SIGNAL(changed()), this, SIGNAL(changed()));
}

MapTile::MapTile(const MapTile& cpy) :
	QObject(cpy.parent()),
	_pos(cpy._pos)
{
	copy(cpy);
	connect(&_netAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkRequestFinished(QNetworkReply*)));
	connect(&_pos, SIGNAL(changed()), this, SIGNAL(changed()));
}

MapTile& MapTile::operator=(const MapTile& rhs)
{
	copy(rhs);
	return *this;
}

void MapTile::copy(const MapTile& cpy)
{
	setParent(cpy.parent()); // copy parent so parent owner auto destruction can be used
	_mapView = cpy._mapView;
	Q_ASSERT(_mapView);
	_image = cpy._image; // shared data until deep copy is needed
	if (cpy._ready && !_image.isNull())
	{
		_ready	= true;
		_busy	= false;
		_cached = cpy._cached;
		_source = cpy._source;
	}
	else
	{
		_ready	= false;
		_busy	= false;
		_cached = false;
		_source = MapTypes::tsNone;
	}
	_pos					= cpy._pos;
	_lastErrorMessage		= "";
	emit changed();
}

MapPosition& MapTile::pos()
{
	return _pos;
}

const MapPosition& MapTile::pos() const
{
	return _pos;
}

bool MapTile::isReady() const
{
	return _ready;
}

bool MapTile::isBusy() const
{
	return _busy;
}

bool MapTile::isCached() const
{
	return _cached;
}

const QImage& MapTile::image() const
{
	return _image;
}

QString MapTile::getCacheFilename() const
{
	Q_ASSERT(_mapView);
	if (!_pos.isSet())
	{
		DebugLog(dlWarn, "MapTile::getCacheFilename position not set");
		return "";
	}
	switch (_mapView->config().cacheTilePathOrder())
	{
		case MapTypes::tpZXY:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().cacheDir())
										   .arg(_pos.zoom()).arg(_pos.x()).arg(_pos.y())
										   .arg(_mapView->config().imageExtension());
		case MapTypes::tpZYX:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().cacheDir())
										   .arg(_pos.zoom()).arg(_pos.y()).arg(_pos.x())
										   .arg(_mapView->config().imageExtension());
		case MapTypes::tpXYZ:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().cacheDir())
										   .arg(_pos.x()).arg(_pos.y()).arg(_pos.zoom())
										   .arg(_mapView->config().imageExtension());
		default:
			DebugLog(dlWarn, "MapTile::getCacheFilename unsupported cacheTitlePathOrder: %d", (int)_mapView->config().cacheTilePathOrder());
			return "";
	} // switch
}

QString MapTile::getDownloadFilename() const
{
	Q_ASSERT(_mapView);
	if (!_pos.isSet())
	{
		DebugLog(dlWarn, "MapTile::getDownloadFilename position not set");
		return "";
	}
	switch (_mapView->config().downloadTilePathOrder())
	{
		case MapTypes::tpZXY:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().downloadUrlBase())
										   .arg(_pos.zoom()).arg(_pos.x()).arg(_pos.y())
										   .arg(_mapView->config().imageExtension());
		case MapTypes::tpZYX:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().downloadUrlBase())
										   .arg(_pos.zoom()).arg(_pos.y()).arg(_pos.x())
										   .arg(_mapView->config().imageExtension());
		case MapTypes::tpXYZ:
			return QString("%1/%2/%3/%4%5").arg(_mapView->config().downloadUrlBase())
										   .arg(_pos.x()).arg(_pos.y()).arg(_pos.zoom())
										   .arg(_mapView->config().imageExtension());
		default:
			DebugLog(dlWarn, "MapTile::getDownloadFilename unsupported downloadTilePathOrder: %d", (int)_mapView->config().downloadTilePathOrder());
			return "";
	} // switch
}

bool MapTile::fileExists()
{
	Q_ASSERT(_mapView);
	if (!_mapView->config().cacheEnabled())
		return false;
	QString fileName = getCacheFilename();
	if (fileName.isEmpty())
		return false;
	QFile file(fileName);
	return file.exists();
}

bool MapTile::loadFromFile()
{
	Q_ASSERT(_mapView);
	if (!_mapView->config().cacheEnabled())
	{
		DebugLog(dlWarn, "MapTile::loadFromFile cache disabled");
		_lastErrorMessage = "MapTile cache disabled";
		return false;
	}
	QString fileName = getCacheFilename();
	if (fileName.isEmpty())
	{
		DebugLog(dlWarn, "MapTile::loadFromFile filename empty");
		_lastErrorMessage = "MapTile cache filename empty";
		return false;
	}
	if (!_image.load(fileName))
	{
		DebugLog(dlInfo, "MapTile::loadFromFile load image failed fileName: %s", qPrintable(fileName));
		_lastErrorMessage = "MapTile cache load file failed: " + fileName;
		return false;
	}
	_cached = true;
	return true;
}

bool MapTile::saveToFile(QString fileName /*= QString("")*/)
{
	Q_ASSERT(_mapView);
	if (!_mapView->config().cacheEnabled())
	{
		DebugLog(dlWarn, "MapTile::saveToFile cache disabled");
		_lastErrorMessage = "MapTile save cache disabled";
		return false;
	}
	if (fileName.isEmpty())
		fileName = getCacheFilename();
	if (fileName.isEmpty())
	{
		DebugLog(dlWarn, "MapTile::saveToFile filename empty");
		_lastErrorMessage = "MapTile save cache filename empty";
		return false;
	}
	if (_image.isNull())
	{
		DebugLog(dlWarn, "MapTile::saveToFile image null");
		_lastErrorMessage = "MapTile save cache image is null";
		return false;
	}
	QString filePath = QFileInfo(fileName).absolutePath();
	if (!QDir("/").mkpath(filePath))
	{
		DebugLog(dlInfo, "MapTile::saveToFile make directories failed path: %s", qPrintable(filePath));
		_lastErrorMessage = "MapTile save make directories failed: " + filePath;
		return false;
	}
	if (!_image.save(fileName, 0/*format*/, 100/*full quality*/))
	{
		DebugLog(dlInfo, "MapTile::saveToFile save image failed fileName: %s", qPrintable(fileName));
		_lastErrorMessage = "MapTile save cache image failed: " + fileName;
		return false;
	}
	return true;
}

bool MapTile::download()
{
	Q_ASSERT(_mapView);
	if (_ready)
	{
		DebugLog(dlDebug, "MapTile::download already ready key: %s", qPrintable(_pos.key()));
		return false;
	}
	if (!_mapView->config().downloadEnbaled())
	{
		DebugLog(dlWarn, "MapTile::download disabled");
		_lastErrorMessage = "MapTile download disabled";
		return false;
	}
	QUrl url = getDownloadFilename();
	if (url.isEmpty())
	{
		DebugLog(dlWarn, "MapTile::download Url empty");
		_lastErrorMessage = "MapTile download Url empty";
		return false;
	}
	if (!url.isValid())
	{
		DebugLog(dlWarn, "MapTile::download Url invalid: %s", qPrintable(url.toString()));
		_lastErrorMessage = "MapTile download Url invalid: " + url.toString();
		return false;
	}
	QNetworkRequest netRequest(url);
	netRequest.setRawHeader("User-Agent", "Custome Qt 5.3 QNetworkRequest");
	netRequest.setRawHeader("Accept", "image/png");
	_netAccessManager.get(netRequest);
	return true;
}

void MapTile::load()
{
	Q_ASSERT(_mapView);
	if (_ready)
	{
		DebugLog(dlDebug, "MapTile::load already ready key: %s", qPrintable(_pos.key()));
		return;
	}
	if (_busy)
	{
		DebugLog(dlDebug, "MapTile::load still busy key: %s", qPrintable(_pos.key()));
		return;
	}
	_cached = false;
	_ready = false;
	_busy = true;
	_lastErrorMessage = "";
	if (fileExists()) // ensures that cache is enabled
	{
		_source = MapTypes::tsCache;
		if (!loadFromFile())
		{
			_busy = false;
			emit loadFailed(_lastErrorMessage);
		}
		else
		{
			_busy = false;
			_ready = true;
			emit ready();
		}
	}
	else
	{
		_source = MapTypes::tsDownload;
		if (!download()) // ensures that download is enabled
		{
			_busy = false;
			emit loadFailed(_lastErrorMessage);
		}
		// otherwise, leave _busy = true and _ready = false until reply comes
	}
}

void MapTile::loadFileOnly()
{
	if (_ready)
	{
		DebugLog(dlDebug, "MapTile::loadFileOnly already ready key: %s", qPrintable(_pos.key()));
		return;
	}
	if (_busy)
	{
		DebugLog(dlDebug, "MapTile::loadFileOnly still busy key: %s", qPrintable(_pos.key()));
		return;
	}
	_cached = false;
	_ready = false;
	_busy = true;
	_lastErrorMessage = "";
	_source = MapTypes::tsCache;

	if (!loadFromFile())
	{
		_busy = false;
		emit loadFailed(_lastErrorMessage);
	}
	else
	{
		_busy = false;
		_ready = true;
		emit ready();
	}
}

void MapTile::loadDownloadOnly()
{
	if (_ready)
	{
		DebugLog(dlDebug, "MapTile::loadDownloadOnly already ready key: %s", qPrintable(_pos.key()));
		return;
	}
	if (_busy)
	{
		DebugLog(dlDebug, "MapTile::loadDownloadOnly still busy key: %s", qPrintable(_pos.key()));
		return;
	}
	_cached = false;
	_ready = false;
	_busy = true;
	_lastErrorMessage = "";
	_source = MapTypes::tsDownload;
	if (!download())
	{
		_busy = false;
		emit loadFailed(_lastErrorMessage);
	}
	// otherwise, leave _busy = true and _ready = false until reply comes
}

void MapTile::saveCache()
{
	if (!saveToFile())
		emit saveFailed(_lastErrorMessage);
	else
		_cached = true;
	if (_ready)
		_busy = false;
}

void MapTile::networkRequestFinished(QNetworkReply* reply)
{
	Q_ASSERT(_mapView);
	if (_ready || !_busy || _source != MapTypes::tsDownload)
	{
		DebugLog(dlWarn, "MapTile::networkRequestFinished reply not expectecd key: %s, Url: %s, ready: %c, busy: %c", qPrintable(_pos.key()), qPrintable(reply->url().toString()), BoolYN(_ready), BoolYN(_busy));
		_lastErrorMessage = "MapTile download reply not expectecd key: " + _pos.key() + " Url: " + reply->url().toString();
		emit loadFailed(_lastErrorMessage);
	}
	else if (reply->error() != QNetworkReply::NoError)
	{
		DebugLog(dlInfo, "MapTile::networkRequestFinished download error key: %s, Url: %s, error: %s", qPrintable(_pos.key()), qPrintable(reply->url().toString()), qPrintable(reply->errorString()));
		_lastErrorMessage = "MapTile download error: " + reply->errorString();
		emit loadFailed(_lastErrorMessage);
	}
	else if (reply->bytesAvailable() == 0)
	{
		DebugLog(dlInfo, "MapTile::networkRequestFinished reply empty key: %s, Url: %s", qPrintable(_pos.key()), qPrintable(reply->url().toString()));
		_lastErrorMessage = "MapTile download reply is empty";
		emit loadFailed(_lastErrorMessage);
	}
	else
	{
		if (!_image.loadFromData(reply->readAll()))
		{
			DebugLog(dlInfo, "MapTile::networkRequestFinished load image from data failed key: %s, Url: %s", qPrintable(_pos.key()), qPrintable(reply->url().toString()));
			_lastErrorMessage = "MapTile download load image from data failed";
			emit loadFailed(_lastErrorMessage);
		}
		else
		{
			_ready = true;
			emit ready();
			if (_mapView->config().cacheEnabled())
				emit saveCache(); // leave busy until done saving
			else
				_busy = false;
		}
	}
	if (!_ready)
		_busy = false; // clear busy on fail
	reply->deleteLater();
}
