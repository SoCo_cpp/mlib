#include <QPointF>
#include <QtMath>
#include "logging/debuglogger.h"
#include "map/mapview.h"

MapView::MapView() :
	QObject(0),
	_config(),
	_pos(this),
	_width(0),
	_height(0),
	_recsChanged(true), // force calcRects
	_visibleTilesChanged(true) // force rebuild
{
	connect(&_config, SIGNAL(changed()), this, SLOT(configChanged()));
	connect(&_pos, SIGNAL(changed()), this, SLOT(viewPositionChanged()));
}

MapConfig& MapView::config()
{
	return _config;
}

const MapConfig& MapView::config() const
{
	return _config;
}

void MapView::setConfig(const MapConfig& cpyConfig)
{
	_config = cpyConfig;
}

MapPosition& MapView::pos()
{
	return _pos;
}

const MapPosition& MapView::pos() const
{
	return _pos;
}

int MapView::width() const
{
	return _width;
}

int MapView::height() const
{
	return _height;
}

void MapView::calcRects()
{
	double tileSize = _config.tileSizePx();
	double wht = _width / 2.0 / tileSize;
	double hht = _height / 2.0 / tileSize;

	_tileRectF.setLeft(  _pos.dx() - wht);
	_tileRectF.setRight( _pos.dx() + wht);
	_tileRectF.setTop(   _pos.dy() - hht);
	_tileRectF.setBottom(_pos.dy() + hht);

	_tileRect = QRect( QPoint(qFloor(_tileRectF.left()), qFloor(_tileRectF.top())), QPoint(qCeil(_tileRectF.right()), qCeil(_tileRectF.bottom())) );
	_latlonRect.setLeft(   MapPosition::lon(_tileRectF.left(),   _pos.zoom()) );
	_latlonRect.setRight(  MapPosition::lon(_tileRectF.right(),  _pos.zoom()) );
	_latlonRect.setTop(    MapPosition::lat(_tileRectF.top(),    _pos.zoom()) );
	_latlonRect.setBottom( MapPosition::lat(_tileRectF.bottom(), _pos.zoom()) );

	double xOffset = _tileRectF.left() - ((double)_tileRect.left());
	double yOffset = _tileRectF.top() - ((double)_tileRect.top());

	_pixelOffset.setX( (-qFloor(xOffset * tileSize)) );
	_pixelOffset.setY( (-qFloor(yOffset * tileSize)) );

	_recsChanged = false;
	_visibleTilesChanged = true;
}

const QRect& MapView::getTileRect()
{
	if (_recsChanged)
		calcRects();
	return _tileRect;
}

const QRectF& MapView::getTileRectF()
{
	return _tileRectF;
}

const QRectF& MapView::getLatLonRect()
{
	if (_recsChanged)
		calcRects();
	return _latlonRect;
}

const QPoint& MapView::getPixelOffset()
{
	if (_recsChanged)
		calcRects();
	return _pixelOffset;
}

void MapView::resized(int width, int height)
{
	_width = width;
	_height = height;
	_recsChanged = true;
	_visibleTilesChanged = true;
	emit viewChanged();
}

void MapView::buildVisibleTiles(bool loadAll /*= true*/)
{
	if (_recsChanged)
		calcRects();
	VisibleMapTile visibleMapTile;
	int tx, ty; // tile x/y
	int ax, ay; // adjusted tile x/y wrapped around contraints
	int px, py; // pixel x/y
	QHash<QString,MapTile*>::iterator itr;
	int xyMax = _pos.xyMax();
	int tileSize = _config.tileSizePx();


	_visibleTiles.clear();

	py = _pixelOffset.y();
	for (ty = _tileRect.top();ty < _tileRect.bottom();ty++)
	{
		px = _pixelOffset.x();
		for (tx = _tileRect.left();tx != _tileRect.right();tx++)
		{
			ax = tx;
			ay = ty;
			while (ax < 0)
				ax += xyMax;
			while (ax >= xyMax)
				ax -= xyMax;
			while (ay < 0)
				ay += xyMax;
			while (ay >= xyMax)
				ay -= xyMax;
			visibleMapTile.done = false;
			visibleMapTile.pixelPos = QPoint(px, py);
			itr = _tiles.find(MapPosition::buildKey(ax, ay, _pos.zoom()));
			if (itr == _tiles.end())
			{
				MapTile* newTile = createMapTile(ax, ay, _pos.zoom());
				visibleMapTile.mapTile = newTile;
				_visibleTiles.append(visibleMapTile);
				if (loadAll)
					emit newTile->load();
			}
			else
			{
				visibleMapTile.mapTile = (*itr);
				_visibleTiles.append(visibleMapTile);
				if (loadAll && !(*itr)->isReady() && !(*itr)->isBusy())
					emit (*itr)->load();
			}
			px += tileSize;
		}
		py += tileSize;
	}
}

QVector<MapView::VisibleMapTile>& MapView::visibleTiles(bool loadAllOnRebuild /*= true*/)
{
	if (_visibleTilesChanged)
		buildVisibleTiles(loadAllOnRebuild);
	return _visibleTiles;
}

MapTile* MapView::createMapTile(int x, int y, int zoom)
{
	MapTile* newTile = new MapTile(this);
	if (!newTile->pos().set(x, y, zoom))
		DebugLog(dlError, "MapView::createMapTile set newTile position failed (%d, %d, %d)", x, y , zoom);

	_tiles.insert(newTile->pos().key(), newTile);

	connect(newTile, SIGNAL(ready()), this, SLOT(tileReady()));
	connect(newTile, SIGNAL(changed()), this, SLOT(tileChanged()));
	connect(newTile, SIGNAL(loadFailed(QString)), this, SLOT(tileLoadFailed(QString)));
	connect(newTile, SIGNAL(saveFailed(QString)), this, SLOT(tileSaveFailed(QString)));
	return newTile;
}

void MapView::prepVisibleTiles()
{
	if (_visibleTilesChanged)
		buildVisibleTiles(true /*loadAll*/);
	else
	{
		for (QVector<VisibleMapTile>::iterator itr = _visibleTiles.begin();itr != _visibleTiles.end();++itr)
				if ( (*itr).mapTile && !(*itr).mapTile->isReady() && !(*itr).mapTile->isBusy())
					emit (*itr).mapTile->load();
	}
}

void MapView::configChanged()
{
	_recsChanged = true;
	_visibleTilesChanged = true;
	emit viewChanged();
}

void MapView::viewPositionChanged()
{
	_recsChanged = true;
	_visibleTilesChanged = true;
	emit viewChanged();
}

void MapView::tileChanged()
{
	_visibleTilesChanged = true;
	emit viewChanged();
}

void MapView::tileReady()
{
	emit viewTilesUpdated();
}

void MapView::tileLoadFailed(QString message)
{
	DebugLog(dlInfo, "MapView::tileLoadFailed message: %d", qPrintable(message));
}

void MapView::tileSaveFailed(QString message)
{
	DebugLog(dlInfo, "MapView::tileSaveFailed message: %d", qPrintable(message));
}

void MapView::panByPixels(int x, int y)
{
	double tileSize = _config.tileSizePx();
	double dx = (double)x;
	double dy = (double)y;
	dx /= tileSize;
	dy /= tileSize;
	_pos.moveBy(dx, dy);

}

bool MapView::isTileVisible(MapTile* mapTile) const
{
	for (QVector<VisibleMapTile>::const_iterator itr = _visibleTiles.begin();itr != _visibleTiles.end();++itr)
		if ( (*itr).mapTile == mapTile )
			return true;
	return false;
}

MapView::VisibleMapTile* MapView::findVisibleMapTile(MapTile* mapTile)
{
	for (QVector<VisibleMapTile>::iterator itr = _visibleTiles.begin();itr != _visibleTiles.end();++itr)
		if ( (*itr).mapTile == mapTile )
			return (&(*itr));
	return 0;
}
