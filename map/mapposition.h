#ifndef MapPosition_H
#define MapPosition_H

#include <QObject>

class MapView; // predeclared
class MapPosition : public QObject
{
	Q_OBJECT
public:
	const static int cMinZoom = 0;
	const static int cMaxZoom = 17;

	MapPosition(MapView* mapView);
	MapPosition(const MapPosition& cpy);
	MapPosition& operator=(const MapPosition& rhs);
	void copy(const MapPosition& cpy);

	bool isSet() const;
	bool set(double dx, double dy, int zoom = -1);
	bool setLonLat(double lon, double lat, int zoom = -1);
	bool setZoom(int zoom); // ensures x/y are correct and marks set

	void clear();

	int x() const;
	int y() const;
	int zoom() const;
	double dx() const;
	double dy() const;
	double xFraction() const;
	double yFraction() const;

	static QString buildKey(int x, int y, int zoom);
	void updateKey();
	const QString& key() const;

	int xyMax(int forZoom = -1) const; // depends on zoom

	double lon() const;
	double lat() const;

	static double lon(double x, int zoom);
	static double lat(double y, int zoom);

	void moveBy(double mdx, double mdy);

protected:
	MapView* _mapView;
	bool _set;
	double _dx;
	double _dy;
	int _x;
	int _y;
	int _zoom;
	QString _key;

signals:
	void changed();
public slots:

};

#endif // MapPosition_H
