#ifndef MAPTILE_H
#define MAPTILE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QImage>
#include "map/mapposition.h"
#include "map/mapconfig.h"

class MapView; // predeclared
class MapTile : public QObject
{
	Q_OBJECT
public:

	MapTile(MapView* pMapView);
	MapTile(const MapTile& cpy);
	MapTile& operator=(const MapTile& rhs);
	void copy(const MapTile& cpy);

	MapPosition& pos();
	const MapPosition& pos() const;

	bool isReady() const;
	bool isBusy() const;
	bool isCached() const;

	const QImage& image() const;

	QString getCacheFilename() const;
	QString getDownloadFilename() const;

	bool fileExists();
	bool loadFromFile();
	bool saveToFile(QString fileName = QString(""));
	bool download();

protected:
	MapView* _mapView;
	MapPosition _pos;
	bool _ready;
	bool _busy;
	bool _cached;
	MapTypes::MapTileSource _source;
	QImage _image;
	QString _lastErrorMessage;
	QNetworkAccessManager _netAccessManager;

signals:
	void changed();
	void ready();
	void loadFailed(QString message);
	void saveFailed(QString message);

public slots:
	void load();
	void loadFileOnly();
	void loadDownloadOnly();
	void saveCache();
	void networkRequestFinished(QNetworkReply * reply);
};
#endif // MAPTILE_H
