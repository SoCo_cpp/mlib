#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QWidget>
#include "map/mapview.h"

class MapWidget : public QWidget
{
	Q_OBJECT
public:
	explicit MapWidget(QWidget *parent = 0);

	MapView& mapView();
	const MapView& mapView() const;

	void prepTiles();
	void drawTiles(QPainter* painter);

protected:
	MapView _mapView;

	virtual void resizeEvent(QResizeEvent* event);
	virtual void paintEvent(QPaintEvent* event);
signals:
	void resized(int width, int height);

public slots:
	void viewChanged();
};

#endif // MAPWIDGET_H
