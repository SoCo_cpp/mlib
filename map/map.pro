
!defined(MLIB_SRC,var):error("MLIB map (mlib/map/map.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/map

message("MLIB including map...")

QT *= gui

SOURCES +=  $$MSRC/mapconfig.cpp \
            $$MSRC/mapposition.cpp \
            $$MSRC/maptile.cpp \
            $$MSRC/mapview.cpp \
            $$MSRC/mapwidget.cpp

HEADERS +=  $$MSRC/mapconfig.h \
            $$MSRC/mapposition.h \
            $$MSRC/maptile.h \
            $$MSRC/mapview.h \
            $$MSRC/mapwidget.h \
            $$MSRC/maptypes.h
