#ifndef TIMESTAMPEDLOG_H
#define TIMESTAMPEDLOG_H

#include <QObject>
#include <QString>
#include "logging/logbase.h"

class TimestampedLog : public LogBase
{
	Q_OBJECT
public:
	explicit TimestampedLog(QObject *parent = 0, const QString& sLogName = "TimestampedLog", const QString& sFileName = "", const QString& sPathDelimiter = "/", const QString& sLineDelimiter = "\r\n", const QString& sTimestampFormat = "");

	void setTimestampFormat(const QString& sFormat);
	const QString& timestampFormat() const;
	QString formattedTimestamp() const;

	virtual void log(const char* strFmt, ...);
	virtual void log(const char* strFmt, va_list arglist);
	virtual void log(const QString& sMsg);

protected:
	QString _timestampFormat;

signals:
	void timestampFormatChanged();
public slots:

};

#endif // TIMESTAMPEDLOG_H
