
!defined(MLIB_SRC,var):error("MLIB logging (mlib/logging/logging.pro) - MLIB_SRC not defined.")

MSRC = $$MLIB_SRC/logging

message("MLIB including logging...")

SOURCES += $$MSRC/logbase.cpp \
           $$MSRC/timestampedlog.cpp \
           $$MSRC/debuglogger.cpp

HEADERS += $$MSRC/logbase.h \
           $$MSRC/timestampedlog.h \
           $$MSRC/debuglogger.h
