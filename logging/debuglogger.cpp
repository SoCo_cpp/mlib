#include "logging/debuglogger.h"

DebugLogger& DebugLogInstance()
{
	return DebugLogger::instance();
}

void DebugLog(int level, const char* strFmt, ...)
{
	va_list arglist;
	va_start(arglist, strFmt);
	DebugLogInstance().log(level, strFmt, arglist);
	va_end(arglist);
}

void DebugLog(int level, const QString& sMsg)
{
	DebugLogInstance().log(level, sMsg);
}

char BoolYN(bool b)
{
	return (b ? 'Y' : 'N');
}

char IsNullYN(const void* p)
{
	return (p == NULL ? 'Y' : 'N');
}

const char* CStrOrNull(const char* str)
{
	return (str == NULL ? "<null>" : str);
}


DebugLogger::DebugLogger(QObject *parent /*= 0*/, const QString& sLogName /*= "DebugLog"*/, const QString& sFileName /*= ""*/, const QString& sPathDelimiter /*= "/"*/, const QString& sLineDelimiter /*= "\r\n"*/, const QString& sTimestampFormat /*= ""*/) :
	TimestampedLog(parent, sLogName, sFileName, sPathDelimiter, sLineDelimiter, sTimestampFormat),
	_maxLevel(DebugLogger::All)
{
}

/*static*/DebugLogger& DebugLogger::instance()
{
	static DebugLogger instance;
	return instance;
}

void DebugLogger::setMaxLevel(int level)
{
	_maxLevel = level;
	emit maxLevelChanged();
}

int DebugLogger::maxLevel() const
{
	return _maxLevel;
}

bool DebugLogger::isIncludedLevel(int level) const
{
	return (level <= maxLevel());
}

void DebugLogger::log(int level, const char* strFmt, ...)
{
    if (!isIncludedLevel(level))
		return;
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	emit outputRawLineSignal(stampMessage(level, sMsg));
}

void DebugLogger::log(int level, const char* strFmt, va_list arglist)
{
    if (!isIncludedLevel(level))
		return;
	emit outputRawLineSignal(stampMessage(level, QString().vsprintf(strFmt, arglist)));
}

void DebugLogger::log(int level, const QString& sMsg)
{
    if (!isIncludedLevel(level))
		return;
	emit outputRawLineSignal(stampMessage(level, sMsg));
}

QString DebugLogger::stampMessage(int level, const QString& sMsg)
{
	QString sStamped = formattedTimestamp();
	switch (level)
	{
		case None:	sStamped += "[None]> ";		break;
		case Error:	sStamped += "[Error]> ";	break;
		case Warn:	sStamped += "[Warn]> ";		break;
		case Info:	sStamped += "[Info]> ";		break;
		case Debug:	sStamped += "[Debug]> ";	break;
		case All:	sStamped += "[All]> ";		break;
		default:
			sStamped += "[Level ";
			sStamped += level;
			sStamped += "]> ";
			break;
	} // witch (level)
	sStamped += sMsg;
	return sStamped;
}

