#ifndef LOGBASE_H
#define LOGBASE_H

#include <QObject>

class LogBase : public QObject
{
    Q_OBJECT
public:
	explicit LogBase(QObject *parent = 0, const QString& sLogName = "Unnamed", const QString& sFileName = "", const QString& sPathDelimiter = "/", const QString& sLineDelimiter = "\r\n");

	void setLogName(const QString& sFileName);
	const QString& logName() const;

	void setPathDelimiter(const QString& sPathDelimiter);
	const QString& pathDelimiter() const;

	void setFileName(const QString& sFileName);
	bool isFileNameSet() const;
    const QString& fileName() const;
	QString filePath() const;

        bool fileExists() const;
	bool filePathExists() const;
	bool makePath() const;

	bool archiveEnabled() const;
	void setArchiveEnabled(bool enabled = true);

	qint64 archiveSize() const;
	void setArchiveSize(qint64 size);

	const QString& archiveDir() const;
	void setArchiveDir(const QString& dir, bool appendToFilePath = false);

	const QString& archiveNameFormat() const;
	void setArchiveNameFormat(const QString& format);

	void setArchiving(qint64 size, const QString& fileNameFormat, const QString& dir, bool appendToFilePath = false);

	QString archiveFileName() const;

	void setLineDelimiter(const QString& sLineDelimiter);
	const QString& lineDelimiter() const;

	bool outputRaw(const QString& sRaw) const;
	bool outputRawLine(const QString& sLine) const;

	void consoleWarn(const char* strFmt, ...) const;
	void consoleWarn(const QString& sMsg) const;

	virtual void log(const char* strFmt, ...);
	virtual void log(const char* strFmt, va_list arglist);
	virtual void log(const QString& sMsg);

protected:
	QString _logName;
	QString _fileName;
	QString _pathDelimiter;
	QString _lineDelimiter;
	bool _archiveEnabled;
	qint64 _archiveSize;
	QString _archiveDir;
	QString _archiveNameFormat;

signals:
	void logNameChanged();
    void fileNameChanged();
    void lineDelimiterChanged();
	void outputRawSignal(QString sMsg);
	void outputRawLineSignal(QString sMsg);

public slots:
	void outputRawSlot(QString sMsg);
	void outputRawLineSlot(QString sMsg);

};

#endif // LOGBASE_H
