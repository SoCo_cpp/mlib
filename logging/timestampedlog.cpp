#include "logging/timestampedlog.h"
#include <QDateTime>

TimestampedLog::TimestampedLog(QObject *parent /*= 0*/, const QString& sLogName /*= "TimestampedLog"*/, const QString& sFileName /*= ""*/, const QString& sPathDelimiter /*= "/"*/, const QString& sLineDelimiter /*= "\r\n"*/, const QString& sTimestampFormat /*= ""*/) :
	LogBase(parent, sLogName, sFileName, sPathDelimiter, sLineDelimiter),
	_timestampFormat(sTimestampFormat)
{
}

void TimestampedLog::setTimestampFormat(const QString& sFormat)
{
	_timestampFormat = sFormat;
	emit timestampFormatChanged();
}

const QString& TimestampedLog::timestampFormat() const
{
	return _timestampFormat;
}

QString TimestampedLog::formattedTimestamp() const
{
	if (timestampFormat().isEmpty())
		return QDateTime::currentDateTime().toString(Qt::ISODate) + " ";
	else
		return QDateTime::currentDateTime().toString(timestampFormat());
}

void TimestampedLog::log(const char* strFmt, ...)
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	emit outputRawLineSignal(formattedTimestamp() + sMsg);
}

void TimestampedLog::log(const char* strFmt, va_list arglist)
{
	emit outputRawLineSignal(formattedTimestamp() + QString().vsprintf(strFmt, arglist));
}

void TimestampedLog::log(const QString& sMsg)
{
	emit outputRawLineSignal(formattedTimestamp() + sMsg);
}
