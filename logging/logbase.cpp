#include "logging/logbase.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDateTime>

LogBase::LogBase(QObject *parent /*= 0*/, const QString& sLogName /*= "Unnamed"*/, const QString& sFileName /*= ""*/, const QString& sPathDelimiter /*= "/"*/, const QString& sLineDelimiter /*= "\r\n"*/) :
    QObject(parent),
	_logName(sLogName),
	_pathDelimiter(sPathDelimiter),
	_lineDelimiter(sLineDelimiter),
	_archiveEnabled(false),
	_archiveSize(2500000),
	_archiveNameFormat("_MM-dd-yy_hh-mm-ss")
{
	setFileName(sFileName); // use set function to clean up file name
	setArchiveDir("archive", true/*appendToFilePath*/);
	connect(this, SIGNAL(outputRawSignal(QString)), this, SLOT(outputRawSlot(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(outputRawLineSignal(QString)), this, SLOT(outputRawLineSlot(QString)), Qt::QueuedConnection);
}

void LogBase::setLogName(const QString& sFileName)
{
	_logName = sFileName;
	emit logNameChanged();
}

const QString& LogBase::logName() const
{
	return _logName;
}

void LogBase::setPathDelimiter(const QString& sPathDelimiter)
{
	_pathDelimiter = sPathDelimiter;
}

const QString& LogBase::pathDelimiter() const
{
	return _pathDelimiter;
}

const QString& LogBase::fileName() const
{
	return _fileName;
}

QString LogBase::filePath() const
{
	return QFileInfo(fileName()).absolutePath();
}

void LogBase::setFileName(const QString& sFileName)
{
	QString sTempFileName = sFileName; // make copy to accept const
	if (sTempFileName.startsWith ("~/"))
		   sTempFileName.replace (0, 1, QDir::homePath());
	QDir dirFileName(QDir::cleanPath(sTempFileName));
	_fileName = dirFileName.absolutePath();
    emit fileNameChanged();
}

bool LogBase::isFileNameSet() const
{
	return !_fileName.isEmpty();
}

bool LogBase::fileExists() const
{
	return isFileNameSet() && QFile::exists(fileName());
}

bool LogBase::filePathExists() const
{
	return isFileNameSet() && QDir("/").exists(filePath());
}

bool LogBase::makePath() const
{
	if (!QDir("/").mkpath(filePath())) // QDir::mkpath returns success if exists
		return false;
	if (_archiveEnabled && !QDir("/").mkpath(_archiveDir)) // QDir::mkpath returns success if exists
		return false;
	return true;
}

bool LogBase::archiveEnabled() const
{
	return _archiveEnabled;
}

void LogBase::setArchiveEnabled(bool enabled /*= true*/)
{
	_archiveEnabled = enabled;
}

qint64 LogBase::archiveSize() const
{
	return _archiveSize;
}

void LogBase::setArchiveSize(qint64 size)
{
	_archiveSize = size;
}

const QString& LogBase::archiveDir() const
{
	return _archiveDir;
}

void LogBase::setArchiveDir(const QString& dir, bool appendToFilePath /*= false*/)
{
	if (appendToFilePath)
		_archiveDir = filePath() + _pathDelimiter + dir;
	else
		_archiveDir = dir;
}

const QString& LogBase::archiveNameFormat() const
{
	return _archiveNameFormat;
}

void LogBase::setArchiveNameFormat(const QString& format)
{
	_archiveNameFormat = format;
}

void LogBase::setArchiving(qint64 size, const QString& fileNameFormat, const QString& dir, bool appendToFilePath /*= false*/)
{
	_archiveEnabled = true;
	_archiveSize = size;
	_archiveNameFormat = fileNameFormat;
	if (appendToFilePath)
		_archiveDir = filePath() + _pathDelimiter + dir;
	else
		_archiveDir = dir;
}

QString LogBase::archiveFileName() const
{
	QFileInfo fileInfo(fileName());
	QString suffix = fileInfo.completeSuffix();
	if (!suffix.isEmpty())
		suffix = "." + suffix;
	return _archiveDir + _pathDelimiter + fileInfo.completeBaseName() + QDateTime::currentDateTime().toString(_archiveNameFormat) + suffix;
}

void LogBase::setLineDelimiter(const QString& sLineDelimiter)
{
    _lineDelimiter = sLineDelimiter;
    emit lineDelimiterChanged();
}

const QString& LogBase::lineDelimiter() const
{
    return _lineDelimiter;
}

void LogBase::consoleWarn(const char* strFmt, ...) const
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = sMsg.vsprintf(strFmt, arglist);
	va_end(arglist);
	consoleWarn(sMsg);
}

void LogBase::consoleWarn(const QString& sMsg) const
{
	qWarning("%s: %s", qPrintable(logName()), qPrintable(sMsg)); // do this for security since there are no args
}

bool LogBase::outputRaw(const QString& sRaw) const
{
	if (!isFileNameSet())
	{
		consoleWarn("LogBase file name not set, fail");
		return false;
	}
	QFile file(fileName());
	if (!file.open(QIODevice::WriteOnly | QIODevice::Append)) // QIODevice::Text
	{
		consoleWarn("LogBase open failed: %s", qPrintable(fileName()));
		return false;
	}
	QTextStream outstream(&file);
    outstream << sRaw;
    if (outstream.status() != QTextStream::Ok)
	{
		consoleWarn("LogBase write failed");
        return false;
	}
	if (_archiveEnabled)
	{
		outstream.flush(); // make sure file size is up to date
		if (file.size() > _archiveSize)
			if (!file.rename(archiveFileName()))
			{
                consoleWarn("LogBase rename to archive file name failed: %s -> %s", qPrintable(fileName()), qPrintable(archiveFileName()));
				return false;
			}
	}
	return true;
}

bool LogBase::outputRawLine(const QString& sLine) const
{
    QString delimitedLine = sLine + lineDelimiter();
    return outputRaw(delimitedLine);
}

void LogBase::outputRawSlot(QString sMsg)
{
	outputRaw(sMsg);
}

void LogBase::outputRawLineSlot(QString sMsg)
{
	outputRawLine(sMsg);
}

void LogBase::log(const char* strFmt, ...)
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	emit outputRawLineSignal(sMsg);
}

void LogBase::log(const char* strFmt, va_list arglist)
{
	emit outputRawLineSignal(QString().vsprintf(strFmt, arglist));
}

void LogBase::log(const QString& sMsg)
{
	emit outputRawLineSignal(sMsg);
}
